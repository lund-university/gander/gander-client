@TestOn('chrome')

import 'dart:io';

import 'package:test/test.dart';
import 'package:gander/gander_package.dart';

void main() {
  // Server variables used in testing
  HttpServer server;
  Uri url;

  setUp(() async {
    server = await HttpServer.bind('localhost', 1234);
    url = Uri.parse('http://${server.address.host}:${server.port}');
  });

  tearDown(() async {
    await server.close(force: true);
    server = null;
    url = null;
  });

  // Test for basic Dart client
  test('Testing main function finding querySelector', () {
    var app = GanderController();
    expect(app, GanderController());
  });
}
