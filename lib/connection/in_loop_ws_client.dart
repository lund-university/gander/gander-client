import 'dart:convert';
import 'dart:html';
import '../service_models/gander_service_model.dart';

import '../service_models/gander_storage_model.dart';

class InLoopWsClient {
  StorageModel storage;

  Function callback;
  int sampleNmbr = 0;

  WebSocket webSocket;

  InLoopWsClient() {
    storage = StorageModel();
  }

  /// Initializes the connection with the eye-tracker.
  void init(Function coordinateConverter) {
    webSocket = WebSocket('ws://localhost:3005');
    storage.createNewFileInStorage();

    /// What to do when opneing?
    webSocket.onOpen.listen((event) async {
      webSocket.send('{"action": "connect"}');
    });

    /// All messaages from server will be on format {ts:'data', x:'data', y:'data'}
    webSocket.onMessage.listen((MessageEvent e) {
      //If this is the first message, we need to synchronize the time

      if (sampleNmbr == 0) {
        var data = jsonDecode(e.data.toString());
        //time data is sent in milliseconds, we need to convert it to microseconds
        var millis = double.parse(data['ts']);
        storage.sendTimeSynch(millis.toString(),
            DateTime.now().microsecondsSinceEpoch.toString());
      }
      //store every element that we get fixation data of from the server

      var elementString = coordinateConverter(e.data, isScaled: true);
      if (elementString != null) {
        storage.storeElements(elementString);
      }
      // Stores the coordinate string as-is.
      storage.storeCoordinates(e.data);

      ++sampleNmbr;
    });

    /// What the client should do on errors
    webSocket.onError.listen((Event e) {
      querySelector('.switch-div').style.background = 'red';
      print('Error connecting to the inLoopFixation service websocket: $e');
    });

    /// What should be done when closing the connection.
    webSocket.onClose.listen((CloseEvent e) {
      storage.sendAllDataOnEnd();
      storage.createNewFileInStorage();
      print('Closed connection to inLoopFixation service websocket');
      sampleNmbr = 0;
    });
  }

  /// Method for closing the connection.
  void close() async {
    webSocket.close();
  }
}
