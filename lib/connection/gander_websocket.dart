import 'dart:convert';
import 'dart:html';
import '../service_models/gander_storage_model.dart';

class GanderWebsocket {
  // Commands needed to connect to the websocket
  static String COMMAND_CONNECT = '{"action": "connect"}';
  static String COMMAND_SET_FPS =
      '{"action": "setSampleStreamFreq", "freq": 30}';
  static String COMMAND_START_LISTENING = '{"action": "startSampleStream"}';
  static String COMMAND_STOP_LISTENING = '{"action": "stopSampleStream"}';
  static String COMMAND_START_BUFFER = '{"action": "startSampleBuffer"}';
  static String COMMAND_STOP_BUFFER = '{"action": "stopSampleBuffer"}';
  //Apparently this is the max integer value when compiling to JS, which follows number format specified in IEEE 754
  static const int intMaxValue = 9007199254740991;
  //Sample Buffer is sent as a jsonArray, a single jsonObject surrounded in '[]'
  static String COMMAND_GET_SAMPLE_BUFFER =
      '{"action": "peekSamples", "nSamples":$intMaxValue}';
  static String COMMAND_CLEAR_SAMPLEBUFFER = '{"action":"clearSampleBuffer"}';
  //Goal: Test if this works fine.


  StorageModel storage;

  Function callback;
  int sampleNmbr = 0;

  WebSocket webSocket;

  GanderWebsocket() {
    storage = StorageModel();
  }

  /// Initializes the connection with the eye-tracker.
  void init(Function coordinateConverter, Function astCallback) {
    webSocket = WebSocket('ws://localhost:3003');
    storage.createNewFileInStorage();

    /// Sends the necessary actions to receive the streams of coordinates.
    webSocket.onOpen.listen((event) async {
      webSocket.sendString(COMMAND_CONNECT);
      webSocket.send(COMMAND_CLEAR_SAMPLEBUFFER);
      webSocket.sendString(COMMAND_START_BUFFER);
      webSocket.sendString(COMMAND_SET_FPS);
      webSocket.sendString(COMMAND_START_LISTENING);
    });

    /// What the client should to when a new coordinate has been received.
    webSocket.onMessage.listen((MessageEvent e) {
      if (e.data.toString().contains('"ts":')) {
        if (sampleNmbr == 0) {
          var data = jsonDecode(e.data.toString());
          storage.sendTimeSynch(data['ts'].toString(),
              DateTime.now().microsecondsSinceEpoch.toString());
        }
        if (e.data.toString().startsWith('[')) {
          //Indicates that this is data from the buffer, should be stored in buffer logs and sent with storeBufferData command
          storage.postBufferDataToStorage(e.data);
        } else {
          // Trying to find the matching element of the coordinates.
          var elementString = coordinateConverter(e.data);

          if (elementString != null) {
            storage.storeElements(elementString);
          }
          // Trying to find the AST representation of the coordinates.
          var astString = astCallback(e.data);
          if (astString != null) {
            storage.storeASTInformation(astString);
          }

          // Stores the coordinate string as-is.
          storage.storeCoordinates(e.data);

          ++sampleNmbr;
        }
      }
    });

    /// What the client should do on errors
    webSocket.onError.listen((Event e) {
      querySelector('.switch-div').style.background = 'red';
      print('Error connecting to the websocket: $e');
    });

    /// What should be done when closing the connection.
    webSocket.onClose.listen((CloseEvent e) {
      storage.sendAllDataOnEnd();
      print('Closed connection');
      sampleNmbr = 0;
    });
  }

  /// Method for closing the connection.
  void close() async {
    webSocket.sendString(COMMAND_STOP_LISTENING);
    webSocket.sendString(COMMAND_STOP_BUFFER);
    webSocket.sendString(COMMAND_GET_SAMPLE_BUFFER);
    await Future.delayed(Duration(seconds: 1), () {
      webSocket.send(COMMAND_CLEAR_SAMPLEBUFFER);
      webSocket.close();
    });
  }

  void sendMouseMoveEvent(Event e, Element foundElement, String data) {
    var targetElem = foundElement ?? DivElement();

    storage.storeInteraction(e.type, targetElem, data);
  }

  ///Sends information about client interactions to storage service, for logging.
  void sendInteractionEvent(Event e, [String data = '']) {
    Element target;
    var temp = e.target;

    if (temp is HtmlDocument || e.currentTarget == null) {
      /* 
        Typecheck due to problem with e.target when sending 
        Scrollevent. it evaluates as type HtmlDocument
        at runtime and this was a quick solution.
       */
      target = DivElement();
    } else if (e.currentTarget is HtmlDocument) {
      target = temp;
    } else { 
      //Hopefully returns the element with event connected to it.
      target = e.currentTarget;
    }

    storage.storeInteraction(e.type, target, data);
  }
}
