import 'dart:html';
import 'dart:math';
import '../gander_package.dart';
import 'aoi_base.dart';

class AOIHandler extends AOIHandlerBase {
  Map<String, List<Def>> map;
  List<String> colours = [
    'MediumSlateBlue',
    'MediumSeaGreen',
    'MediumTurquoise',
    'OliveDrab',
    'PaleVioletRed',
    'Silver'
  ];
  var color = 0;
  Set<String> showMoreList = {};

  List<DivElement> aoiList = [];
  // Function interactionCallBack;

  //Custom event to trigger AOIs when fixatino point detected
  Event fixationEvent = Event('fixation');

  //constants relating design of AOIs
  var colourDose = 0.01;
  var sizeDose = 0.2;
  var doseThreshold = 30;
  var doseThreshold2 = 200;
  var doseTime = 10000;
  var widthSize = 1.2;
  var heightSize = 1.1;

  AOIHandler(Function interactionCallBack, Map<String, List<Def>> useDefMap)
      : super(interactionCallBack) {
    map = useDefMap;
    // this.interactionCallBack = interactionCallBack;
  }

  @override
  List<DivElement> handleAOIs() {
    //Add divelements for AOIs, link them togetehr through eventlisteners
    if (map != null) {
      for (var name in map.keys) {
        for (var aoi in map[name]) {
          if (aoi != null) {
            createAOI(aoi, name);
          }
        }
      }
    }
    try {
      createNeighbourLinks(true);
    } catch (e) {
      print('Could not create neighbour links in handleAOIs');
    }
    return aoiList;
  }

  @override
  void moveAOIs(String buttonName) {
    if (map != null) {
      for (var name in map.keys) {
        for (var aoi in map[name]) {
          if (aoi != null) {
            handleShowMore(aoi, name);
          }
        }
      }
    }
    querySelectorAll('#' + Css.escape(buttonName + '-arrow'))
        .forEach((element) {
      element.remove();
    });
    showMoreList.remove(buttonName);
    createNeighbourLinks(false, buttonName);
    moveShowMoreIndicators();
  }

  void createAOI(Def linkedAOIs, String fileName) {
    var declLoc = linkedAOIs.loc;
    var start = aoiList.length;
    var elemStart = document.querySelector('#' +
        Css.escape('$fileName-l${declLoc.startLine}:${declLoc.startCol}'));
    var elemStop = document.querySelector(
        '#' + Css.escape('$fileName-l${declLoc.endLine}:${declLoc.endCol}'));

    if (elemStart != null && elemStop != null) {
      var colour =
          querySelector('#' + Css.escape('$fileName-l${declLoc.startLine}'))
              .className;
      var order = 0;
      var width;
      var height;
      if (elemStop.getBoundingClientRect().top ==
          elemStart.getBoundingClientRect().top) {
        // variables and fields

        if (elemStop.getBoundingClientRect().right -
                elemStart.getBoundingClientRect().left !=
            0) {
          width = ((elemStop.getBoundingClientRect().right -
                      elemStart.getBoundingClientRect().left) *
                  widthSize)
              .round();
        } else {
          width = 15;
        }
        if (elemStop.getBoundingClientRect().bottom -
                elemStart.getBoundingClientRect().top !=
            0) {
          height = ((elemStop.getBoundingClientRect().bottom -
                      elemStart.getBoundingClientRect().top) *
                  heightSize)
              .round();
        } else {
          height = 15;
        }
      } else {
        //methods are special since they include the whole block, we just use the decl
        width = ((elemStart.parent.getBoundingClientRect().right -
                    elemStart.parent.getBoundingClientRect().left) *
                0.75)
            .round();
        height = ((elemStart.getBoundingClientRect().bottom -
                    elemStart.getBoundingClientRect().top) *
                heightSize)
            .round();
      }
      var x =
          (elemStart.getBoundingClientRect().left - width / 2 + width / 2.4);
      var y =
          (elemStart.getBoundingClientRect().top - height / 2 + height / 2.2);
      var id = linkedAOIs.name + '-' + fileName + '-' + declLoc.toString();
      var def = createDiv(id, width, height, x, y, order, colour);
      aoiList.add(def);

      for (var use in linkedAOIs.useList) {
        var colour =
            querySelector('#' + Css.escape('$fileName-l${use.loc.startLine}'))
                .className;
        order++;
        var elemStartString =
            '$fileName-l${use.loc.startLine}:${use.loc.startCol}';
        var elemStopString = '$fileName-l${use.loc.endLine}:${use.loc.endCol}';

        elemStart = document.querySelector('#' +
            Css.escape('$fileName-l${use.loc.startLine}:${use.loc.startCol}'));
        elemStop = document.querySelector('#' +
            Css.escape('$fileName-l${use.loc.endLine}:${use.loc.endCol}'));
        if (elemStart != null) {
          width = ((elemStop.getBoundingClientRect().right -
                      elemStart.getBoundingClientRect().left) *
                  widthSize)
              .round();
          height = ((elemStop.getBoundingClientRect().bottom -
                      elemStart.getBoundingClientRect().top) *
                  heightSize)
              .round();
          var x = (elemStart.getBoundingClientRect().left -
              width / 2 +
              width / 2.4);
          var y = (elemStart.getBoundingClientRect().top -
              height / 2 +
              height / 2.2);

          def = createDiv(id, width, height, x, y, order, colour);
          aoiList.add(def);
        }
      }
      for (var i = start; i < aoiList.length; i++) {
        var colour = colours[color];
        //Trigger custom event when fixation is detected
        aoiList[i].addEventListener('fixation', (event) {
          var data = '${aoiList[i].dataset['colour']}';
          interactionCallBack(event, data);
          triggerCounter(aoiList[i], aoiList[start], colour);
        });
      }
      if (color + 1 == colours.length) {
        color = 0;
      } else {
        color++;
      }
    }
  }

  //another strategy for linking the AOIs centering around decls
  void createNeighbourLinks(bool create, [String buttonpressed]) {
    var list = [];
    var decl;

    for (var i = 0; i < aoiList.length - 1; i++) {
      if (!create) {
        if (!aoiList[i].id.contains('arrow') &&
            'show-more-' + aoiList[i].dataset['colour'] == buttonpressed) {
          aoiList[i].dataset['colour'] = '';
        }

        if (!aoiList[i + 1].id.contains('arrow') &&
            'show-more-' + aoiList[i + 1].dataset['colour'] == buttonpressed) {
          aoiList[i + 1].dataset['colour'] = '';
        }
      }
      var scroll = window.scrollY;

      if (aoiList[i].id.contains('-aoi-0')) {
        decl = aoiList[i];
      }
      if (decl.className == aoiList[i + 1].className) {
        var left;
        var top;
        var height;
        var theta;
        var order = aoiList[i + 1].id.split('-').last;
        if (aoiList[i + 1].dataset['colour'].contains('-button-') &&
            decl.dataset['colour'].contains('-button-')) {
          left = 0;
          height = 0;
          top = 0;
          theta = 0;
          var buttonTop = querySelector('.' +
                  Css.escape('show-more-' + aoiList[i + 1].dataset['colour']))
              .getBoundingClientRect()
              .top;
          var buttonMid = querySelector('.' +
                      Css.escape(
                          'show-more-' + aoiList[i + 1].dataset['colour']))
                  .getBoundingClientRect()
                  .left +
              querySelector('.' +
                          Css.escape(
                              'show-more-' + aoiList[i + 1].dataset['colour']))
                      .getBoundingClientRect()
                      .width *
                  0.5;
          var buttonBot = querySelector('.' +
                  Css.escape('show-more-' + aoiList[i + 1].dataset['colour']))
              .getBoundingClientRect()
              .bottom;
          var offsetX = 50;
          if (create) {
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid + offsetX,
                buttonTop - 10,
                5,
                15,
                pi / 6));
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid - offsetX,
                buttonTop - 10,
                5,
                15,
                5 * pi / 6));
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid - offsetX,
                buttonBot,
                5,
                15,
                7 * pi / 6));
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid + offsetX,
                buttonBot,
                5,
                15,
                11 * pi / 6));
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid,
                buttonTop - 15,
                5,
                15,
                0));
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid,
                buttonBot + 5,
                5,
                15,
                0));
            showMoreList.add('show-more-' + aoiList[i + 1].dataset['colour']);
          }
        } else if (aoiList[i + 1].dataset['colour'].contains('-button-')) {
          var y1 = double.parse(
                  decl.style.top.substring(0, decl.style.top.length - 2)) +
              double.parse(
                  decl.style.height.substring(0, decl.style.height.length - 2));

          var y2 = querySelector('.' +
                      Css.escape(
                          'show-more-' + aoiList[i + 1].dataset['colour']))
                  .getBoundingClientRect()
                  .top +
              scroll;

          var x1 = double.parse(
                  decl.style.left.substring(0, decl.style.left.length - 2)) +
              0.5 *
                  double.parse(decl.style.width
                      .substring(0, decl.style.width.length - 2));
          var x2 = querySelector('.' +
                      Css.escape(
                          'show-more-' + aoiList[i + 1].dataset['colour']))
                  .getBoundingClientRect()
                  .left +
              querySelector('.' +
                          Css.escape(
                              'show-more-' + aoiList[i + 1].dataset['colour']))
                      .getBoundingClientRect()
                      .width *
                  0.5;
          if (y2 < y1) {
            var tempy = y1;
            var tempx = x1;
            y1 = y2;
            y2 = tempy;
            x1 = x2;
            x2 = tempx;
          }
          height = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
          theta = atan((x1 - x2) / (y1 - y2));

          theta = -theta;

          if (x2 > x1) {
            left = x1 + 0.5 * height * sin(-theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          } else {
            left = x1 - 0.5 * height * sin(theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          }
          var buttonTop = querySelector('.' +
                  Css.escape('show-more-' + aoiList[i + 1].dataset['colour']))
              .getBoundingClientRect()
              .top;
          var buttonMid = querySelector('.' +
                      Css.escape(
                          'show-more-' + aoiList[i + 1].dataset['colour']))
                  .getBoundingClientRect()
                  .left +
              querySelector('.' +
                          Css.escape(
                              'show-more-' + aoiList[i + 1].dataset['colour']))
                      .getBoundingClientRect()
                      .width *
                  0.5;
          var buttonBot = querySelector('.' +
                  Css.escape('show-more-' + aoiList[i + 1].dataset['colour']))
              .getBoundingClientRect()
              .bottom;
          var offsetX = 50;

          if (create) {
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid + offsetX,
                buttonTop - 10,
                5,
                15,
                pi / 6));
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid - offsetX,
                buttonTop - 10,
                5,
                15,
                5 * pi / 6));
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid - offsetX,
                buttonBot,
                5,
                15,
                7 * pi / 6));
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid + offsetX,
                buttonBot,
                5,
                15,
                11 * pi / 6));
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid,
                buttonTop - 15,
                5,
                15,
                0));
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid,
                buttonBot + 5,
                5,
                15,
                0));
            showMoreList.add('show-more-' + aoiList[i + 1].dataset['colour']);
          }
        } else if (decl.dataset['colour'].contains('-button-')) {
          var y2 = double.parse(aoiList[i + 1]
              .style
              .top
              .substring(0, aoiList[i + 1].style.top.length - 2));
          var y1 = querySelector(
                      '.' + Css.escape('show-more-' + decl.dataset['colour']))
                  .getBoundingClientRect()
                  .bottom +
              scroll;

          var x2 = double.parse(aoiList[i + 1]
                  .style
                  .left
                  .substring(0, aoiList[i + 1].style.left.length - 2)) +
              0.5 *
                  double.parse(aoiList[i + 1]
                      .style
                      .width
                      .substring(0, aoiList[i + 1].style.width.length - 2));
          var x1 = querySelector(
                      '.' + Css.escape('show-more-' + decl.dataset['colour']))
                  .getBoundingClientRect()
                  .left +
              querySelector('.' +
                          Css.escape('show-more-' + decl.dataset['colour']))
                      .getBoundingClientRect()
                      .width *
                  0.5;
          if (y2 < y1) {
            var tempy = y1;
            var tempx = x1;
            y1 = y2;
            y2 = tempy;
            x1 = x2;
            x2 = tempx;
          }
          height = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
          theta = atan((x1 - x2) / (y1 - y2));

          theta = -theta;

          if (x2 > x1) {
            left = x1 + 0.5 * height * sin(-theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          } else {
            left = x1 - 0.5 * height * sin(theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          }
          var buttonTop = querySelector(
                  '.' + Css.escape('show-more-' + decl.dataset['colour']))
              .getBoundingClientRect()
              .top;
          var buttonMid = querySelector(
                      '.' + Css.escape('show-more-' + decl.dataset['colour']))
                  .getBoundingClientRect()
                  .left +
              querySelector('.' +
                          Css.escape('show-more-' + decl.dataset['colour']))
                      .getBoundingClientRect()
                      .width *
                  0.5;
          var buttonBot = querySelector(
                  '.' + Css.escape('show-more-' + decl.dataset['colour']))
              .getBoundingClientRect()
              .bottom;
          var offsetX = 50;

          if (create) {
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid + offsetX,
                buttonTop - 10,
                5,
                15,
                pi / 6));
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid - offsetX,
                buttonTop - 10,
                5,
                15,
                5 * pi / 6));
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid - offsetX,
                buttonBot,
                5,
                15,
                7 * pi / 6));
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid + offsetX,
                buttonBot,
                5,
                15,
                11 * pi / 6));
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid,
                buttonTop - 15,
                5,
                15,
                0));
            list.add(showArrows(
                decl.className,
                'show-more-' + decl.dataset['colour'],
                buttonMid,
                buttonBot + 5,
                5,
                15,
                0));
            showMoreList.add('show-more-' + decl.dataset['colour']);
          }
        } else {
          var aoiHeight = double.parse(
              decl.style.height.substring(0, decl.style.height.length - 2));
          var y1 = double.parse(
                  decl.style.top.substring(0, decl.style.top.length - 2)) +
              aoiHeight;
          var y2 = double.parse(aoiList[i + 1]
              .style
              .top
              .substring(0, aoiList[i + 1].style.top.length - 2));

          var x1 = double.parse(
                  decl.style.left.substring(0, decl.style.left.length - 2)) +
              0.5 *
                  double.parse(decl.style.width
                      .substring(0, decl.style.width.length - 2));
          var x2 = double.parse(aoiList[i + 1]
                  .style
                  .left
                  .substring(0, aoiList[i + 1].style.left.length - 2)) +
              0.5 *
                  double.parse(aoiList[i + 1]
                      .style
                      .width
                      .substring(0, aoiList[i + 1].style.width.length - 2));
          if (y2 < y1 - aoiHeight) {
            var tempy = y1;
            var tempx = x1;
            y1 = y2 + aoiHeight;
            y2 = tempy - aoiHeight;
            x1 = x2;
            x2 = tempx;
          }
          height = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

          if (y1 - y2 ==
              double.parse(decl.style.height
                  .substring(0, decl.style.height.length - 2))) {
            theta = pi / 2;
          } else {
            theta = atan((x1 - x2) / (y1 - y2));
          }
          theta = -theta;

          if (x2 > x1) {
            left = x1 + 0.5 * height * sin(-theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          } else {
            left = x1 - 0.5 * height * sin(theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          }
        }
        if (create) {
          list.add(showArrows(
              decl.className, decl.id + order, left, top, 5, height, theta));
        } else {
          var div = querySelector('#' + Css.escape(decl.id + order + '-arrow'));
          div.style.transform = 'none';
          div.style.top = top.toString() + 'px';
          div.style.left = left.toString() + 'px';
          div.style.height = height.toString() + 'px';
          div.style.transform = 'rotate($theta' 'rad)';
        }
      }
      if (aoiList[i + 1].id.contains('-arrow')) {
        i = aoiList.length;
      }
    }
    if (create) {
      for (var arrow in list) {
        aoiList.add(arrow);
      }
    }
  }

  void createLinks(bool create, [String buttonpressed]) {
    var list = [];

    for (var i = 0; i < aoiList.length - 1; i++) {
      if (!create) {
        if ('show-more-' + aoiList[i].dataset['colour'] == buttonpressed) {
          aoiList[i].dataset['colour'] = '';
        }
        if ('show-more-' + aoiList[i + 1].dataset['colour'] == buttonpressed) {
          aoiList[i + 1].dataset['colour'] = '';
        }
      }
      var scroll = window.scrollY;
      if (aoiList[i].className == aoiList[i + 1].className) {
        var left;
        var top;
        var height;
        var theta;
        if (aoiList[i + 1].dataset['colour'].contains('-button-') &&
            aoiList[i].dataset['colour'].contains('-button-')) {
          left = 0;
          height = 0;
          top = 0;
          theta = 0;
          var buttonTop = querySelector('.' +
                  Css.escape('show-more-' + aoiList[i + 1].dataset['colour']))
              .getBoundingClientRect()
              .top;
          var buttonMid = querySelector('.' +
                      Css.escape(
                          'show-more-' + aoiList[i + 1].dataset['colour']))
                  .getBoundingClientRect()
                  .left +
              querySelector('.' +
                          Css.escape(
                              'show-more-' + aoiList[i + 1].dataset['colour']))
                      .getBoundingClientRect()
                      .width *
                  0.5;
          var buttonBot = querySelector('.' +
                  Css.escape('show-more-' + aoiList[i + 1].dataset['colour']))
              .getBoundingClientRect()
              .bottom;
          var offsetX = 50;
          if (create) {
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid + offsetX,
                buttonTop - 10,
                5,
                15,
                pi / 6));
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid - offsetX,
                buttonTop - 10,
                5,
                15,
                5 * pi / 6));
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid - offsetX,
                buttonBot,
                5,
                15,
                7 * pi / 6));
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid + offsetX,
                buttonBot,
                5,
                15,
                11 * pi / 6));
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid,
                buttonTop - 15,
                5,
                15,
                0));
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid,
                buttonBot + 5,
                5,
                15,
                0));
            showMoreList.add('show-more-' + aoiList[i + 1].dataset['colour']);
          }
        } else if (aoiList[i + 1].dataset['colour'].contains('-button-')) {
          var y1 = double.parse(aoiList[i]
                  .style
                  .top
                  .substring(0, aoiList[i].style.top.length - 2)) +
              double.parse(aoiList[i]
                  .style
                  .height
                  .substring(0, aoiList[i].style.height.length - 2));

          var y2 = querySelector('.' +
                      Css.escape(
                          'show-more-' + aoiList[i + 1].dataset['colour']))
                  .getBoundingClientRect()
                  .top +
              scroll;

          var x1 = double.parse(aoiList[i]
                  .style
                  .left
                  .substring(0, aoiList[i].style.left.length - 2)) +
              0.5 *
                  double.parse(aoiList[i]
                      .style
                      .width
                      .substring(0, aoiList[i].style.width.length - 2));
          var x2 = querySelector('.' +
                      Css.escape(
                          'show-more-' + aoiList[i + 1].dataset['colour']))
                  .getBoundingClientRect()
                  .left +
              querySelector('.' +
                          Css.escape(
                              'show-more-' + aoiList[i + 1].dataset['colour']))
                      .getBoundingClientRect()
                      .width *
                  0.5;
          if (y2 < y1) {
            var tempy = y1;
            var tempx = x1;
            y1 = y2;
            y2 = tempy;
            x1 = x2;
            x2 = tempx;
          }
          height = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
          theta = atan((x1 - x2) / (y1 - y2));

          theta = -theta;

          if (x2 > x1) {
            left = x1 + 0.5 * height * sin(-theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          } else {
            left = x1 - 0.5 * height * sin(theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          }
          var buttonTop = querySelector('.' +
                  Css.escape('show-more-' + aoiList[i + 1].dataset['colour']))
              .getBoundingClientRect()
              .top;
          var buttonMid = querySelector('.' +
                      Css.escape(
                          'show-more-' + aoiList[i + 1].dataset['colour']))
                  .getBoundingClientRect()
                  .left +
              querySelector('.' +
                          Css.escape(
                              'show-more-' + aoiList[i + 1].dataset['colour']))
                      .getBoundingClientRect()
                      .width *
                  0.5;
          var buttonBot = querySelector('.' +
                  Css.escape('show-more-' + aoiList[i + 1].dataset['colour']))
              .getBoundingClientRect()
              .bottom;
          var offsetX = 50;

          if (create) {
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid + offsetX,
                buttonTop - 10,
                5,
                15,
                pi / 6));
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid - offsetX,
                buttonTop - 10,
                5,
                15,
                5 * pi / 6));
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid - offsetX,
                buttonBot,
                5,
                15,
                7 * pi / 6));
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid + offsetX,
                buttonBot,
                5,
                15,
                11 * pi / 6));
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid,
                buttonTop - 15,
                5,
                15,
                0));
            list.add(showArrows(
                aoiList[i + 1].className,
                'show-more-' + aoiList[i + 1].dataset['colour'],
                buttonMid,
                buttonBot + 5,
                5,
                15,
                0));
            showMoreList.add('show-more-' + aoiList[i + 1].dataset['colour']);
          }
        } else if (aoiList[i].dataset['colour'].contains('-button-')) {
          var y2 = double.parse(aoiList[i + 1]
              .style
              .top
              .substring(0, aoiList[i + 1].style.top.length - 2));
          var y1 = querySelector('.' +
                      Css.escape('show-more-' + aoiList[i].dataset['colour']))
                  .getBoundingClientRect()
                  .bottom +
              scroll;

          var x2 = double.parse(aoiList[i + 1]
                  .style
                  .left
                  .substring(0, aoiList[i + 1].style.left.length - 2)) +
              0.5 *
                  double.parse(aoiList[i + 1]
                      .style
                      .width
                      .substring(0, aoiList[i + 1].style.width.length - 2));
          var x1 = querySelector('.' +
                      Css.escape('show-more-' + aoiList[i].dataset['colour']))
                  .getBoundingClientRect()
                  .left +
              querySelector('.' +
                          Css.escape(
                              'show-more-' + aoiList[i].dataset['colour']))
                      .getBoundingClientRect()
                      .width *
                  0.5;
          if (y2 < y1) {
            var tempy = y1;
            var tempx = x1;
            y1 = y2;
            y2 = tempy;
            x1 = x2;
            x2 = tempx;
          }
          height = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
          theta = atan((x1 - x2) / (y1 - y2));

          theta = -theta;

          if (x2 > x1) {
            left = x1 + 0.5 * height * sin(-theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          } else {
            left = x1 - 0.5 * height * sin(theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          }
          var buttonTop = querySelector(
                  '.' + Css.escape('show-more-' + aoiList[i].dataset['colour']))
              .getBoundingClientRect()
              .top;
          var buttonMid = querySelector('.' +
                      Css.escape('show-more-' + aoiList[i].dataset['colour']))
                  .getBoundingClientRect()
                  .left +
              querySelector('.' +
                          Css.escape(
                              'show-more-' + aoiList[i].dataset['colour']))
                      .getBoundingClientRect()
                      .width *
                  0.5;
          var buttonBot = querySelector(
                  '.' + Css.escape('show-more-' + aoiList[i].dataset['colour']))
              .getBoundingClientRect()
              .bottom;
          var offsetX = 50;

          if (create) {
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid + offsetX,
                buttonTop - 10,
                5,
                15,
                pi / 6));
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid - offsetX,
                buttonTop - 10,
                5,
                15,
                5 * pi / 6));
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid - offsetX,
                buttonBot,
                5,
                15,
                7 * pi / 6));
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid + offsetX,
                buttonBot,
                5,
                15,
                11 * pi / 6));
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid,
                buttonTop - 15,
                5,
                15,
                0));
            list.add(showArrows(
                aoiList[i].className,
                'show-more-' + aoiList[i].dataset['colour'],
                buttonMid,
                buttonBot + 5,
                5,
                15,
                0));
            showMoreList.add('show-more-' + aoiList[i].dataset['colour']);
          }
        } else {
          var aoiHeight = double.parse(aoiList[i]
              .style
              .height
              .substring(0, aoiList[i].style.height.length - 2));
          var y1 = double.parse(aoiList[i]
                  .style
                  .top
                  .substring(0, aoiList[i].style.top.length - 2)) +
              aoiHeight;
          var y2 = double.parse(aoiList[i + 1]
              .style
              .top
              .substring(0, aoiList[i + 1].style.top.length - 2));

          var x1 = double.parse(aoiList[i]
                  .style
                  .left
                  .substring(0, aoiList[i].style.left.length - 2)) +
              0.5 *
                  double.parse(aoiList[i]
                      .style
                      .width
                      .substring(0, aoiList[i].style.width.length - 2));
          var x2 = double.parse(aoiList[i + 1]
                  .style
                  .left
                  .substring(0, aoiList[i + 1].style.left.length - 2)) +
              0.5 *
                  double.parse(aoiList[i + 1]
                      .style
                      .width
                      .substring(0, aoiList[i + 1].style.width.length - 2));
          if (y2 < y1 - aoiHeight) {
            var tempy = y1;
            var tempx = x1;
            y1 = y2 + aoiHeight;
            y2 = tempy - aoiHeight;
            x1 = x2;
            x2 = tempx;
          }
          height = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

          if (y1 - y2 ==
              double.parse(aoiList[i]
                  .style
                  .height
                  .substring(0, aoiList[i].style.height.length - 2))) {
            theta = pi / 2;
          } else {
            theta = atan((x1 - x2) / (y1 - y2));
          }
          theta = -theta;

          if (x2 > x1) {
            left = x1 + 0.5 * height * sin(-theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          } else {
            left = x1 - 0.5 * height * sin(theta);
            top = y1 - 0.5 * height * (1 - cos(theta));
          }
        }
        if (create) {
          list.add(showArrows(aoiList[i].className, aoiList[i].id, left, top, 5,
              height, theta));
        } else {
          var div = querySelector('#' + Css.escape(aoiList[i].id + '-arrow'));
          div.style.transform = 'none';
          div.style.top = top.toString() + 'px';
          div.style.left = left.toString() + 'px';
          div.style.height = height.toString() + 'px';
          div.style.transform = 'rotate($theta' 'rad)';
        }
      }
      if (aoiList[i + 1].id.contains('-arrow')) {
        i = aoiList.length;
      }
    }
    if (create) {
      for (var arrow in list) {
        aoiList.add(arrow);
      }
    }
  }

  void moveShowMoreIndicators() {
    var offsetX = 50;

    for (var buttonName in showMoreList) {
      var indicatorList =
          querySelectorAll('#' + Css.escape(buttonName + '-arrow'));
      var buttonTop = querySelector('.' + Css.escape(buttonName))
          .getBoundingClientRect()
          .top;
      var buttonMid = querySelector('.' + Css.escape(buttonName))
              .getBoundingClientRect()
              .left +
          querySelector('.' + Css.escape(buttonName))
                  .getBoundingClientRect()
                  .width *
              0.5;
      var buttonBot = querySelector('.' + Css.escape(buttonName))
          .getBoundingClientRect()
          .bottom;

      for (var i = 0; i < indicatorList.length; i += 6) {
        var scroll = window.scrollY;
        indicatorList[i].style.left = (buttonMid + offsetX).toString() + 'px';
        indicatorList[i].style.top =
            (buttonTop - 10 + scroll).toString() + 'px';
        indicatorList[i + 1].style.left =
            (buttonMid - offsetX).toString() + 'px';
        indicatorList[i + 1].style.top =
            (buttonTop - 10 + scroll).toString() + 'px';
        indicatorList[i + 2].style.left =
            (buttonMid - offsetX).toString() + 'px';
        indicatorList[i + 2].style.top = (buttonBot + scroll).toString() + 'px';
        indicatorList[i + 3].style.left =
            (buttonMid + offsetX).toString() + 'px';
        indicatorList[i + 3].style.top = (buttonBot + scroll).toString() + 'px';
        indicatorList[i + 4].style.left = (buttonMid).toString() + 'px';
        indicatorList[i + 4].style.top =
            (buttonTop - 15 + scroll).toString() + 'px';
        indicatorList[i + 5].style.left = (buttonMid).toString() + 'px';
        indicatorList[i + 5].style.top =
            (buttonBot + 5 + scroll).toString() + 'px';
      }
    }
  }

  void triggerCounter(DivElement div, DivElement decl, String colour) async {
    div.dataset.update('counter', (value) => (int.parse(value) + 1).toString());
    GanderUtils.setTimeout(() => decrementCounter(div, colour),
        doseTime); // timeout before decrementing the opacity of AOI
    if (int.parse(div.dataset['counter']) > doseThreshold) {
      await displayRelations(div, colour);
    }
    if (int.parse(div.dataset['counter']) > doseThreshold2) {
      decl.dataset
          .update('counter', (value) => (int.parse(value) + 1).toString());
      await displayRelations(decl, colour);
      GanderUtils.setTimeout(() => decrementCounter(decl, colour),
          doseTime); // timeout before decrementing the opacity of AOI
    }
  }

  void decrementCounter(DivElement div, String colour) async {
    div.dataset.update('counter', (value) => (int.parse(value) - 1).toString());
    await displayRelations(div, colour);
  }

  Future<void> displayRelations(DivElement div, String colour) async {
    var order = div.id.split('-').last;
    if (order == '0') {
      var list = querySelectorAll('.${div.className}');
      for (var el in list) {
        el.style.backgroundColor = colour;
        el.style.opacity =
            '${2 * atan(int.parse(div.dataset['counter']) * colourDose) / pi - 0.4}'; //approximation of sigmoid fxn, how a change in counter should change opacity
        if (!el.id.contains('-arrow')) {
          el.style.transform =
              'scale(${3 * atan((int.parse(div.dataset['counter']) - doseThreshold) * sizeDose) / pi + 1})'; //scaling the AOIs (not the links) according to counters
        }
      }
    } else {
      var list = [];
      list.add(div);
      var temp = div.id;

      var declString = temp.replaceRange(temp.length - 1, temp.length, '0');
      var decl = querySelector('#' + Css.escape(declString));
      list.add(decl);
      list.add(querySelector('#' + Css.escape(declString + order + '-arrow')));
      if (decl.dataset['colour'].contains('-button-')) {
        list.addAll(querySelectorAll('#' +
            Css.escape('show-more-' + decl.dataset['colour'] + '-arrow')));
      }

      for (var el in list) {
        el.style.backgroundColor = colour;
        el.style.opacity =
            '${2 * atan(int.parse(div.dataset['counter']) * colourDose) / pi - 0.4}'; //approximation of sigmoid fxn, how a change in counter should change opacity
        if (!el.id.contains('-arrow')) {
          el.style.transform =
              'scale(${3 * atan(int.parse(div.dataset['counter']) * sizeDose) / pi + 1})'; //scaling the AOIs (not the links) according to counters
        }
      }
    }
  }

  DivElement createDiv(String id, num width, num height, num x, num y,
      num order, String colour) {
    var div;
    div = DivElement()
      ..className = '$id-aoi'
      ..id = '$id-aoi-$order'
      ..style.position = 'absolute'
      ..style.zIndex = '1'
      ..style.width = '$width' 'px'
      ..style.height = '$height' 'px'
      ..style.top = '$y' 'px'
      ..style.left = '$x' 'px'
      ..style.borderRadius = '10px'
      ..style.backgroundColor = 'red' //debug
      ..dataset = {'counter': '0', 'colour': colour};

    return div;
  }

  DivElement showArrows(String aoiClass, String id, num x, num y, num width,
      num height, num deg) {
    var div = DivElement()
      ..style.position = 'absolute'
      ..style.top = '$y' 'px'
      ..style.left = '$x' 'px'
      ..style.width = '$width' 'px'
      ..style.height = '$height' 'px'
      ..className = aoiClass
      ..id = id + '-arrow';
    div.style.transform = 'rotate($deg' 'rad)';
    return div;
  }

//to handle moving existing AOis and rescaling the hidden ones
  void handleShowMore(Def linkedAOIs, String fileName) {
    var declLoc = linkedAOIs.loc;

    var elemStart = document.querySelector('#' +
        Css.escape('$fileName-l${declLoc.startLine}:${declLoc.startCol}'));
    var elemStop = document.querySelector(
        '#' + Css.escape('$fileName-l${declLoc.endLine}:${declLoc.endCol}'));
    var width;
    var height;
    if (declLoc.startLine == declLoc.endLine) {
      // variables and fields
      if (elemStop.getBoundingClientRect().right -
              elemStart.getBoundingClientRect().left !=
          0) {
        width = ((elemStop.getBoundingClientRect().right -
                    elemStart.getBoundingClientRect().left) *
                widthSize)
            .round();
      } else {
        width = 15;
      }
      if (elemStop.getBoundingClientRect().bottom -
              elemStart.getBoundingClientRect().top !=
          0) {
        height = ((elemStop.getBoundingClientRect().bottom -
                    elemStart.getBoundingClientRect().top) *
                heightSize)
            .round();
      } else {
        height = 15;
      }
    } else {
      //methods
      width = ((elemStart.parent.getBoundingClientRect().right -
                  elemStart.parent.getBoundingClientRect().left) *
              0.75)
          .round();
      height = ((elemStart.getBoundingClientRect().bottom -
                  elemStart.getBoundingClientRect().top) *
              heightSize)
          .round();
    }
    var x = (elemStart.getBoundingClientRect().left - width / 2 + width / 2.4);
    var y = (elemStart.getBoundingClientRect().top - height / 2 + height / 2.2);
    var id = linkedAOIs.name + '-' + fileName + '-' + declLoc.toString();

    var order = 0;

    var div = querySelector('#' + Css.escape('$id-aoi-$order'));
    div.style.left = '${x}px';
    div.style.top = '${y + window.scrollY}' 'px';
    div.style.width = '$width' 'px';
    div.style.height = '$height' 'px';

    //Handles the individual arrows and uses
    if (linkedAOIs.useList.isNotEmpty) {
      for (var use in linkedAOIs.useList) {
        order++;
        elemStart = document.querySelector('#' +
            Css.escape('$fileName-l${use.loc.startLine}:${use.loc.startCol}'));
        elemStop = document.querySelector('#' +
            Css.escape('$fileName-l${use.loc.endLine}:${use.loc.endCol}'));
        if (elemStart != null) {
          width = ((elemStop.getBoundingClientRect().right -
                      elemStart.getBoundingClientRect().left) *
                  widthSize)
              .round();
          height = ((elemStop.getBoundingClientRect().bottom -
                      elemStart.getBoundingClientRect().top) *
                  heightSize)
              .round();
          x = (elemStart.getBoundingClientRect().left -
              width / 2 +
              width / 2.4);
          y = (elemStart.getBoundingClientRect().top -
              height / 2 +
              height / 2.2);

          var div = querySelector('#' + Css.escape('$id-aoi-$order'));
          div.style.left = '${x}px';
          div.style.top = '${y + window.scrollY}' 'px';
          div.style.width = '$width' 'px';
          div.style.height = '$height' 'px';
        }
      }
    }
  }
}

class AOI {
  var fileName;
  var def;

  AOI(this.fileName, this.def);

  Def getDef() {
    return def;
  }

  String getFile() {
    return fileName;
  }
}
