import 'dart:convert';
import 'dart:html';
import 'dart:math';
import 'package:gander/aoi/aoi_handler.dart';

import '../gander_package.dart';
import 'aoi_base.dart';
import 'ast_converter.dart';

class AOIASTHandler extends AOIHandlerBase {
  Map<String, Map<String, dynamic>> map;
  List<String> colours = [
    'MediumSlateBlue',
    'MediumSeaGreen',
    'MediumTurquoise',
    'OliveDrab',
    'PaleVioletRed',
    'Silver'
  ];
  var color = 0;
  Set<String> showMoreList = {};

  List<DivElement> aoiList = [];
  // Function interactionCallBack;

  //Custom event to trigger AOIs when fixatino point detected
  Event fixationEvent = Event('fixation');

  //constants relating design of AOIs
  var colourDose = 0.01;
  var sizeDose = 0.2;
  var doseThreshold = 30;
  var doseThreshold2 = 200;
  var doseTime = 10000;
  var widthSize = 1.2;
  var heightSize = 1.1;

  AOIASTHandler(
      Function interactionCallBack, Map<String, Map<String, dynamic>> map)
      : super(interactionCallBack) {
    this.map = map;
    // this.interactionCallBack = interactionCallBack;
  }

  @override
  List<DivElement> handleAOIs() {
    //Add divelements for AOIs, link them togetehr through eventlisteners
    //To try in temp:
    //Find first span and add a div to this span thats 10% bigger than the surrounding span.
    //See how this turns up in client and if we can shift to generating aois in this way.

    for (var file in map.keys) {
      AOIcreation(file, map[file], 'root');
    }
    addAoiTrigger();
    return aoiList;
  }

  @override
  void moveAOIs(String buttonName) {
    // for (var aoi in aoiList) {
    //   if (aoi.id.contains('string_literal')) {
    //     var rowId = aoi.id.split('-')[4].split(':')[1] +
    //         '-' +
    //         aoi.id.split('-')[5] +
    //         '-l' +
    //         aoi.id.split('-')[3].split(':')[0];
    //     print(rowId);

    //     if (querySelector('#' + Css.escape(rowId))
    //             .getBoundingClientRect()
    //             .height >
    //         40) {
    //       aoi.style.height = '100%';
    //       aoi.style.top = '0px';
    //       var lineDiv = aoi.parent.parent.parent;
    //       var oldParent = aoi.parent;
    //       lineDiv.children.add(aoi);
    //       oldParent.children.remove(aoi);
    //     }
    //   }
    // }
  }

  void AOIcreation(String file, Map<String, dynamic> map, String parentType) {
    var node = map;

    var startCol = int.parse(node['start'].split(':')[1]);
    var endCol = int.parse(node['end'].split(':')[1]);
    var lineNbr = int.parse(node['start'].split(':')[0]);
    var lineNbrEnd = int.parse(node['end'].split(':')[0]);

    //TODO: Fix this, we should differentiate between block and like for statements, even thouggh both span several lines
    if (lineNbrEnd != lineNbr &&
        !node['type'].contains('statement') &&
        !node['type'].contains('lambda') &&
        !node['type'].contains('block_comment')) {
      // print(lineNbr);
      // print(node['end'].split(':')[0]);
      // print('ERROR: AOI spans multiple lines: \n $node \n');
    } else {
      genAOI(node['type'], file, startCol, endCol, lineNbr, lineNbrEnd,
          parentType);
    }
    if (node.containsKey('children')) {
      for (var child in node['children']) {
        // print(child);
        if (node['type'].toString() == 'block') {
          AOIcreation(file, child, parentType);
        } else {
          AOIcreation(file, child, node['type']);
        }
      }
    }
  }

  void genAOI(String nodeType, String file, int startCol, int endCol,
      int lineNbr, int lineEnd, String parentType) {
    Element elemStart;
    var elemList;
    var line;

    try {
      if (nodeType == 'block_comment') {
        line = lineNbr;
        elemList = [];
        for (var i = lineNbr; i <= lineEnd; i++) {
          var elemStartPre = document
              .querySelector('#' + Css.escape(file) + '-' + Css.escape('l$i'));
          elemStart = elemStartPre.querySelector('.lineCode');

          for (var span in elemStart.children) {
            if (elemStart == null || span.className.contains('lineNbr')) {
              //print('#' + Css.escape(file) + '-' + Css.escape('l$line:$i'));
              //print('erroer ${span.id}');
              continue;
            }
            elemList.add(span);
          }
        }
      } else {
        line = lineNbr;
        elemList = [];
        for (var i = startCol; i <= endCol; i++) {
          elemStart = document.querySelector(
              '#' + Css.escape(file) + '-' + Css.escape('l$line:$i'));

          if (elemStart == null) {
            //print('#' + Css.escape(file) + '-' + Css.escape('l$line:$i'));

            continue;
          }
          elemList.add(elemStart);
        }
      }

      //elemStart.style.position = 'relative';
    } catch (e) {
      print(nodeType);
      //print(node.id);
      //print(node.startPosition);
      //print('#' + Css.escape(file) + '-l' + Css.escape(node.startPosition));
      //print('#' + file + '-l' + node.startPosition);
      //print('_________________');
      //print(e);
      // print(fileName);
      // print('#' +
      //     Css.escape('$fileName-l${declLoc.startLine}:${declLoc.startCol}'));
      // print(Css.escape('8'));
    }

    var random = Random();

    if (elemList != null && elemList.length > 0) {
      var colour;

      colour = querySelector('#' + Css.escape('$file-l$lineNbr')).className;

      var aoiColor = colours[random.nextInt(colours.length)];
//&& int.parse(querySelector('#' + Css.escape('$file-l$line')).style.height) >40
      bool isLargeString = false;
      if (nodeType == 'string_literal' &&
          querySelector('#' + Css.escape('$file-l$line'))
                  .getBoundingClientRect()
                  .height >
              40) {
        // print(querySelector('#' + Css.escape('$file-l$line'))
        //     .getBoundingClientRect()
        //     .height);
        // print(elemList[0].innerHtml);
        // // print(elemList[0].getBoundingClientRect());
        // print('_________________');
        //print(elemList[0].parent.id);
        //print(elemList[0].parent.parent.id);
        elemList[0] = elemList[0].parent.parent;
        isLargeString = true;
        // print(elemList[0]);
        // print('aoi-$nodeType-parent:$parentType-$line:$startCol-file:$file');
        // print(elemList[0].parent.parent.parent.id);
        // print(elemList[0].parent.parent.parent.parent.id);
        //TODO figure out how to handle large string literals, atm the AOI barely covers it since it's left to right
      }
      for (var i = 0; i < elemList.length; i++) {
        var tokenDiv = DivElement();
        tokenDiv
          ..style.position = 'absolute'
          ..style.zIndex = '1'
          //..style.width = '${width}px'
          //..style.width = '100%'
          ..style.width = '100%'
          ..style.height = '150%'
          ..style.top = '-25%'
          //..style.left = '${left}px'
          ..style.left = '0px'
          ..id = 'aoi-$nodeType-parent:$parentType-$line:$startCol-file:$file'
          //..style.opacity = '0.5'
          //..style.backgroundColor = aoiColor
          ..dataset = {'counter': '0', 'colour': '$colour'};
        aoiList.add(tokenDiv);
        if (elemList[i].children.isEmpty ||
            elemList[i].className.contains('hljs')) {
          //Denna funkar inte som den ska, nu blir allt med highlight hljs dubbel-AOI
          elemList[i].style.position = 'relative';
          elemList[i].children.add(tokenDiv);
        } else if (isLargeString) {
          elemList[i].style.position = 'relative';
          tokenDiv.style.height = '100%';
          tokenDiv.style.top = '0px';
          elemList[i].children.add(tokenDiv);
          //print('added');
        }
      }
      //print(elemList[0].innerHtml);
    }
  }

  void addAoiTrigger() {
    for (var aoiElem in aoiList) {
      aoiElem.addEventListener('fixation', (event) {
        //debug
        var data = int.parse('${aoiElem.dataset['counter']}');
        var size = aoiElem.getBoundingClientRect();
        var width = size.width;
        var height = size.height;
        interactionCallBack(
            event,
            aoiElem.dataset['colour'] +
                '-' +
                width.toString() +
                '-' +
                height.toString());
        // if (data > 5) {
        //   aoiElem.style.backgroundColor =
        //       colours[Random().nextInt(colours.length)];
        // }
        aoiElem.dataset['counter'] = '${data + 1}';
      });
    }
  }

  /// Old code for generating AOI based on the AST that comes from the ExtendJ compiler
  // void recursiveAOIcreation(String file, ASTLocation node,
  //     {int start, int end, int line}) {
  //   if (node == null) {
  //     return;
  //   }
  //   if (node.astNode.contains('Stmt') || node.astNode.contains('Catch')) {
  //     //print('______________');
  //     if (node.astNode.contains('For')) {
  //       genAOI(node, node.astNode, file);
  //       for (var i = 0; i < node.children.length; i++) {
  //         recursiveAOIcreation(file, node.children[i]);
  //       }
  //     } else if (node.astNode.contains('While')) {
  //       genAOI(node, node.astNode, file);
  //       for (var i = 0; i < node.children.length; i++) {
  //         recursiveAOIcreation(file, node.children[i]);
  //       }
  //     } else if (node.astNode.contains('If')) {
  //       genAOI(node, node.astNode, file);
  //       for (var i = 0; i < node.children.length; i++) {
  //         if (node.children[i].astNode == 'Opt') {
  //           if (node.children[i].children.isNotEmpty) {
  //             genAOI(node.children[i].children[0], 'ElseBlock', file);
  //           }
  //         }
  //         recursiveAOIcreation(file, node.children[i]);
  //       }
  //     } else if (node.astNode.contains('Try')) {
  //       genAOI(node, node.astNode, file);
  //       for (var i = 0; i < node.children.length; i++) {
  //         recursiveAOIcreation(file, node.children[i]);
  //       }
  //     } else if (node.astNode.contains('Catch')) {
  //       genAOI(node, node.astNode, file);
  //       for (var i = 0; i < node.children.length; i++) {
  //         recursiveAOIcreation(file, node.children[i]);
  //       }
  //     } else {
  //       var type = 'None';
  //       if (node.children.isNotEmpty) {
  //         type = node.children[0].astNode;
  //       }
  //       genAOI(node, type, file, startCol: start, endCol: end, lineNbr: line);
  //     }
  //     //print('=================');
  //   } else if (node.astNode.contains('Decl')) {
  //     genAOI(node, node.astNode, file);
  //     if (node.astNode == 'MethodDecl' ||
  //         node.astNode == 'ConstructorDecl' ||
  //         node.astNode == 'ClassDecl') {
  //       for (var i = 0; i < node.children.length; i++) {
  //         recursiveAOIcreation(file, node.children[i]);
  //       }
  //     }
  //   } else {
  //     for (var i = 0; i < node.children.length; i++) {
  //       recursiveAOIcreation(file, node.children[i]);
  //     }
  //   }
  //   //recursively call genAOI for this and childrens node if id exists
  // }
  //  void commentAOIs(String file) {
  //   if (file.contains('target')) {
  //     return;
  //   }
  //   var fileName = file.split('-');
  //   fileName.removeLast();
  //   var fileDiv = fileName.join('-');
  //   var viewDiv = querySelector('#' + Css.escape('file-object-$fileDiv'));
  //   print('#' + Css.escape('file-object-$fileDiv'));
  //   print(viewDiv);
  //   var commenSpanList =
  //       viewDiv.querySelectorAll('.' + Css.escape('hljs-comment'));
  //   var random = Random();
  //   for (var comment in commenSpanList) {
  //     var line = int.parse(comment.children[0].id
  //         .split('-')[2]
  //         .split(':')[0]
  //         .replaceAll('l', ''));
  //     var colour = querySelector('#' + Css.escape('$file-l$line'));
  //     var aoiColor = colours[random.nextInt(colours.length)];
  //     var id;
  //     if (comment.children[0].id.contains('target')) {
  //       id = 'target-${comment.children[0].innerHtml.replaceAll('//', '')}';
  //     } else {
  //       id = 'src-${comment.children[0].innerHtml.replaceAll('//', '')}';
  //     }
  //     var tokenDiv = DivElement();
  //     tokenDiv
  //       ..style.position = 'absolute'
  //       ..style.zIndex = '1'
  //       //..style.width = '${width}px'
  //       //..style.width = '100%'
  //       ..style.width = '100%'
  //       ..style.height = '120%'
  //       ..style.top = '0px'
  //       //..style.left = '${left}px'
  //       ..style.left = '0px'
  //       ..id = 'aoi-comment-$id'
  //       ..style.opacity = '0.5'
  //       ..style.backgroundColor = aoiColor
  //       ..dataset = {'counter': '0', 'colour': '$colour'};
  //     aoiList.add(tokenDiv);
  //     comment.style.position = 'relative';
  //     comment.children.add(tokenDiv);
  //   }
  // }

  // void genAOI(ASTLocation node, String type, String file,
  //     {int startCol, int endCol, int lineNbr}) {
  //   var linePre;
  //   Element elemStart;
  //   var elemList = null;
  //   var line;

  //   try {
  //     //elemStart = document.querySelector(
  //     //'#' + Css.escape(file) + '-l' + Css.escape(node.startPosition));
  //     var start;
  //     var end;
  //     if (node.astNode == 'MethodDecl' || node.astNode == 'ConstructorDecl') {
  //       start = int.parse(node.startPosition.split(':')[1]);
  //       //end = int.parse(node.endPosition.split(':')[1]);
  //       line = int.parse(node.startPosition.split(':')[0].replaceAll('l', ''));
  //       var lineElem = document
  //           .querySelector('#' + Css.escape(file) + '-' + Css.escape('l$line'));
  //       //elemList = lineElem.children;
  //       elemList = [];
  //       var b = true;
  //       for (var i = 0; i < lineElem.children.length; i++) {
  //         if (!b) {
  //           if (lineElem.children[i].className.contains('hljs')) {
  //             elemList.add(lineElem.children[i].children[0]);
  //           } else {
  //             elemList.add(lineElem.children[i]);
  //           }
  //         } else if (b && lineElem.children[i].className.contains('hljs')) {
  //           b = false;
  //           i -= 1;
  //         }
  //       }
  //     } else {
  //       if (startCol != null && endCol != null) {
  //         start = startCol;

  //         end = endCol;
  //         line = lineNbr;
  //         elemList = [];
  //         for (var i = start; i <= end; i++) {
  //           elemStart = document.querySelector(
  //               '#' + Css.escape(file) + '-' + Css.escape('l$line:$i'));

  //           if (elemStart == null) {
  //             //print('#' + Css.escape(file) + '-' + Css.escape('l$line:$i'));

  //             continue;
  //           }
  //           elemList.add(elemStart);
  //           //print(elemStart.innerHtml);
  //           //print('line: $line col: $i');
  //           //print(node.id);
  //         }
  //       } else {
  //         start = int.parse(node.startPosition.split(':')[1]);
  //         //end = int.parse(node.endPosition.split(':')[1]);
  //         line =
  //             int.parse(node.startPosition.split(':')[0].replaceAll('l', ''));
  //         var lineElem = document.querySelector(
  //             '#' + Css.escape(file) + '-' + Css.escape('l$line'));
  //         //elemList = lineElem.children;
  //         elemList = [];
  //         var b = true;
  //         for (var i = 0; i < lineElem.children.length; i++) {
  //           if (!b) {
  //             if (lineElem.children[i].className.contains('hljs')) {
  //               elemList.add(lineElem.children[i].children[0]);
  //             } else {
  //               elemList.add(lineElem.children[i]);
  //             }
  //           } else if (b &&
  //               lineElem.children[i].className != 'lineNbr' &&
  //               lineElem.children[i].innerHtml != ' ') {
  //             b = false;
  //             i -= 1;
  //           }
  //         }
  //       }

  //       //print('line: $line, start: $start, end: $end');
  //     }

  //     // elemStart.style.position = 'relative';
  //     //print(elemStart.id);
  //     // print('#' +
  //     //     Css.escape('$fileName-l${declLoc.startLine}:${declLoc.startCol}'));
  //     //linePre = elemStart.parent;

  //     //elemStart.style.position = 'relative';
  //   } catch (e) {
  //     print(type);
  //     //print(node.id);
  //     //print(node.startPosition);
  //     //print('#' + Css.escape(file) + '-l' + Css.escape(node.startPosition));
  //     //print('#' + file + '-l' + node.startPosition);
  //     //print('_________________');
  //     //print(e);
  //     // print(fileName);
  //     // print('#' +
  //     //     Css.escape('$fileName-l${declLoc.startLine}:${declLoc.startCol}'));
  //     // print(Css.escape('8'));
  //   }

  //   var random = Random();

  //   if (elemList != null && elemList.length > 0) {
  //     var colour;
  //     if (startCol == null) {
  //       colour = querySelector('#' +
  //               Css.escape(
  //                   '$file-l${int.parse(node.startPosition.split(':')[0])}'))
  //           .className;
  //     } else {
  //       colour = querySelector('#' + Css.escape('$file-l$line'));
  //     }

  //     // var width = elemList[elemList.length - 1].getBoundingClientRect().right -
  //     //     elemList[0].getBoundingClientRect().left;
  //     // var height = elemList[0].getBoundingClientRect().top -
  //     //     elemList[0].getBoundingClientRect().bottom;
  //     // height = height * -1;
  //     // var left = elemList[0].getBoundingClientRect().left;
  //     // //print(left);
  //     // //print(node.id);
  //     // var top = elemList[0].getBoundingClientRect().top;
  //     var aoiColor = colours[random.nextInt(colours.length)];
  //     for (var i = 0; i < elemList.length; i++) {
  //       var tokenDiv = DivElement();
  //       tokenDiv
  //         ..style.position = 'absolute'
  //         ..style.zIndex = '1'
  //         //..style.width = '${width}px'
  //         //..style.width = '100%'
  //         ..style.width = '100%'
  //         ..style.height = '120%'
  //         ..style.top = '0px'
  //         //..style.left = '${left}px'
  //         ..style.left = '0px'
  //         ..id = 'aoi-${node.astNode}-$type-$line:$startCol'
  //         ..style.opacity = '0.5'
  //         ..style.backgroundColor = aoiColor
  //         ..dataset = {'counter': '0', 'colour': '$colour'};
  //       aoiList.add(tokenDiv);
  //       if (elemList[i].children.isEmpty ||
  //           elemList[i].className.contains('hljs')) {
  //         //Denna funkar inte som den ska, nu blir allt med highlight hljs dubbel-AOI
  //         elemList[i].style.position = 'relative';
  //         elemList[i].children.add(tokenDiv);
  //       }
  //     }
  //     //print(elemList[0].innerHtml);
  //   }
  // }
}
