import 'dart:html';

abstract class AOIHandlerBase {
  Function interactionCallBack;

  AOIHandlerBase(this.interactionCallBack);

  List<DivElement> handleAOIs();

  void moveAOIs(String s);
}
