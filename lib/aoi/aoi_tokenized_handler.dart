import 'package:highlighting/languages/r.dart';

import 'aoi_base.dart';
import 'dart:html';
import 'tokenizer.dart';
import 'dart:math';

class TokenizedAOI extends AOIHandlerBase {
//Map<String, List<Def>> map;
  List<String> tokens = [];
  // List<String> colours = [
  //   'MediumSlateBlue',
  //   'MediumSeaGreen',
  //   'MediumTurquoise',
  //   'OliveDrab',
  //   'PaleVioletRed',
  //   'Silver'
  // ];
  //copilot generated a great color palette
  List<String> colours = [
    '#e0ffcd',
    '#fdffcd',
    '#ffcdcd',
    '#cdfffd',
    '#cdcdff',
    '#ffcdff',
    '#cdffff',
    '#ffffcd'
  ];
  var color = 0;
  Set<String> showMoreList = {};

  List<DivElement> aoiList = [];
  // Function interactionCallBack;

  //Custom event to trigger AOIs when fixatino point detected
  Event fixationEvent = Event('fixation');

  //constants relating design of AOIs
  var colourDose = 0.01;
  var sizeDose = 0.2;
  var doseThreshold = 30;
  var doseThreshold2 = 200;
  var doseTime = 10000;
  var widthSize = 1.2;
  var heightSize = 1.1;
  var fileName;

  TokenizedAOI(Function interactionCallBack, String fileName)
      : super(interactionCallBack) {
    //map = useDefMap;
    this.fileName = fileName;
    // this.interactionCallBack = interactionCallBack;
  }
  Map<Element, List<Element>> declAOIs = {};

  @override
  List<DivElement> handleAOIs() {
    //Add divelements for AOIs, link them togetehr through eventlisteners
    //To try in temp:
    //Find first span and add a div to this span thats 10% bigger than the surrounding span.
    //See how this turns up in client and if we can shift to generating aois in this way.
    //var tokens = Tokenizer.tokens[fileName] ?? {};
    var i = 1;
    // print('here');
    // print(tokens.length);
    // print(fileName);
    // print(Tokenizer.tokens.keys);
    var file = fileName + '-target';

    while (genLineAOI(i, file)) {
      //print(aoi);

      i++;
    }
    i = 1;
    // print('here');
    // print(tokens.length);
    // print(fileName);
    // print(Tokenizer.tokens.keys);
    file = fileName + '-src';

    while (genLineAOI(i, file)) {
      //print(aoi);

      i++;
    }
    addAoiTrigger();
    return aoiList;
  }

  // ///A method to generate AOIs of a single file
  // @override
  // List<DivElement> handleAOIsSingleFile(String file) {
  //   //Add divelements for AOIs, link them togetehr through eventlisteners

  //   //To try in temp:
  //   //Find first span and add a div to this span thats 10% bigger than the surrounding span.
  //   //See how this turns up in client and if we can shift to generating aois in this way.

  //   var defList = map[file];

  //   for (var aoi in defList) {
  //     if (aoi != null) {
  //       // createAOI(aoi, file);
  //     }
  //   }
  //   return aoiList;
  // }

  @override
  void moveAOIs(String buttonName) {
    //  if (map != null) {
    //    for (var name in map.keys) {
    //      for (var aoi in map[name]) {
    //        if (aoi != null) {
    //          //handleShowMore(aoi, name);
    //        }
    //      }
    //    }
    //  }
    //  querySelectorAll('#' + Css.escape(buttonName + '-arrow'))
    //      .forEach((element) {
    //    element.remove();
    //  });
    //  showMoreList.remove(buttonName);
    //createNeighbourLinks(false, buttonName);
    // moveShowMoreIndicators();
    print('moveAOIs');
  }

  bool genLineAOI(var line, var file) {
    var linePre;
    Element elemStart;
    try {
      var side;
      if (file.contains('target')) {
        side = 'right';
      } else {
        side = 'left';
      }
      //print(file);
      elemStart =
          document.querySelector('#' + Css.escape('$file-l$line-col-$side'));
      if (elemStart == null) {
        elemStart =
            document.querySelector('#' + Css.escape('$file-l$line-col-$side'));
      }
      elemStart.style.position = 'relative';
      //print(elemStart.id);
      // print('#' +
      //     Css.escape('$fileName-l${declLoc.startLine}:${declLoc.startCol}'));
      linePre = elemStart.parent;
      //elemStart.style.position = 'relative';
    } catch (e) {
      // print(e);
      // print(fileName);
      // print('#' +
      //     Css.escape('$fileName-l${declLoc.startLine}:${declLoc.startCol}'));
      // print(Css.escape('8'));
      return false;
    }

    var random = Random();
    var tokenDiv = DivElement();
    elemStart.children.add(tokenDiv);
    var colour = querySelector('#' + Css.escape('$file-l$line')).className;
    aoiList.add(tokenDiv);
    tokenDiv
      ..style.position = 'absolute'
      ..style.zIndex = '1'
      ..style.width = '100%'
      ..style.height = '100%'
      ..style.top = '0px'
      ..style.left = '0px'
      ..id = 'aoi-nonParsable-parent:nonParsable-$line:0-file:$file'
      //..style.opacity = '0.5'
      //..style.backgroundColor = colours[random.nextInt(colours.length)]
      ..dataset = {'counter': '0', 'colour': '$colour'};

    return true;
  }

  void genAOI(String aoi, int order) {
    var linePre;
    Element elemStart;
    try {
      elemStart = document.querySelector('#' + Css.escape(aoi));
      elemStart.style.position = 'relative';
      //print(elemStart.id);
      // print('#' +
      //     Css.escape('$fileName-l${declLoc.startLine}:${declLoc.startCol}'));
      linePre = elemStart.parent;
      //elemStart.style.position = 'relative';
    } catch (e) {
      // print(e);
      // print(fileName);
      // print('#' +
      //     Css.escape('$fileName-l${declLoc.startLine}:${declLoc.startCol}'));
      // print(Css.escape('8'));
      return;
    }

    var random = Random();
    var tokenDiv = DivElement();
    elemStart.children.add(tokenDiv);

    aoiList.add(tokenDiv);
    tokenDiv
      ..style.position = 'absolute'
      ..style.zIndex = '1'
      ..style.width = '100%'
      ..style.height = '100%'
      ..style.top = '0px'
      ..style.left = '0px'
      ..id = 'aoi-$order-$fileName'
      ..style.opacity = '0.5'
      ..style.backgroundColor = colours[random.nextInt(colours.length)]
      ..dataset = {'counter': '0', 'colour': 'red'};

    // var tokenSpan = SpanElement()
    //   ..style.position = 'relative'
    //   ..id = '$fileName-l${declLoc.startLine}:$token-aoi'
    //   ..style.opacity = '0.5'
    //   ..style.backgroundColor = 'MediumSlateBlue'
    //   ..dataset = {'counter': '0', 'colour': 'red'};

    //elemStart.style.backgroundColor = colours[random.nextInt(colours.length)];

    //linePre.insertBefore(tokenSpan, elemStart);

    // var stopCol;
    // if (linkedAOIs.isVar) {
    //   stopCol = token
    //       .length; //If it's a variable we only care for the length of the token
    // } else {
    //   stopCol = linePre
    //       .children.length; //If it's a function we care for the whole line
    // }
    // for (var col = 0; col < stopCol; col++) {
    //   var charSpan = document.querySelector('#' +
    //       Css.escape(
    //           '$fileName-l${declLoc.startLine}:${declLoc.startCol + col}'));
    //   if (charSpan != null) {
    //     tokenSpan.append(charSpan);
    //   }
    // }
//
    // declAOIs[tokenSpan] = [];
//
    // for (var use in linkedAOIs.useList) {
    //   var useDiv = genTokenSpan(use, fileName, token, linkedAOIs.isVar);
    //   declAOIs[tokenSpan].add(useDiv);
    // }
  }

  //Element genTokenSpan(
  //    InstanceUse use, String fileName, String token, bool isVar) {
  //  var loc = use.loc;

  //  var linePre = document.querySelector('#$fileName-l${loc.startLine}');
  //  linePre.style.position = 'aboslute';

  //  var tokenSpan = SpanElement()
  //    ..style.position = 'relative'
  //    ..id = '$fileName-l${loc.startLine}:$token-aoi'
  //    ..style.opacity = '0.5'
  //    ..style.backgroundColor = 'MediumSlateBlue'
  //    ..dataset = {'counter': '0', 'colour': 'red'};

  //  var elemStart = document.querySelector(
  //      '#' + Css.escape('$fileName-l${loc.startLine}:${loc.startCol}'));

  //  linePre.insertBefore(tokenSpan, elemStart);

  //  var stopCol;
  //  if (isVar) {
  //    stopCol = token
  //        .length; //If it's a variable we only care for the length of the token
  //  } else {
  //    stopCol = linePre
  //        .children.length; //If it's a function we care for the whole line
  //  }

  //  for (var col = 0; col < stopCol; col++) {
  //    var charSpan = document.querySelector('#' +
  //        Css.escape('$fileName-l${loc.startLine}:${loc.startCol + col}'));
  //    if (charSpan != null) {
  //      tokenSpan.append(charSpan);
  //    }
  //  }
  //  return tokenSpan;
  //}

  void addAoiTrigger() {
    for (var aoiElem in aoiList) {
      aoiElem.addEventListener('fixation', (event) {
        //debug
        var data = int.parse('${aoiElem.dataset['counter']}');
        interactionCallBack(event, data.toString());
        /*if (data > 5) {
          aoiElem.style.backgroundColor =
              colours[Random().nextInt(colours.length)];
        }
        */
        aoiElem.dataset['counter'] = '${data + 1}';
      });
    }
  }
}
