import 'dart:convert';
import 'dart:html';

/// One AST Converter for each pull request.
/// Creates a new one for each time a new change view is loaded.
class ASTConverter {
  List<ASTFile> files = [];

  ASTConverter();

  void addFile(ASTFile file) {
    files.add(file);
  }

  ASTLocation getASTNode(ASTFile file, String line, bool isSrc) {
    var information = isSrc ? file.src : file.target;
    var location = information.findASTPosition(line);
    return location;
  }
}

/// A AST File contains the ASTInformation for both the source and target version
/// of the file.
class ASTFile {
  ASTInformation src;
  ASTInformation target;
  String fileName;

  ASTFile(this.src, this.target, this.fileName);
}

/// Contains the content and the collection of AST Nodes in a file.
class ASTInformation {
  StringBuffer content = StringBuffer();
  ASTCollection collection;

  /// Finds the ASTLocation location on [line] in the code file.
  ASTLocation findASTPosition(String line) {
    var found = collection.findPosition(line);
    //unnecessary null check
    //return (found != null) ? found : null;
    if (found == null) {
      print('ASTLocation is null');
      return null;
    }
    return found;
  }
}

/// Keeps track of the AST representation of the file.
class ASTCollection {
  ASTLocation location;

  /// Adds the AST tree as a ASTLocation by a recursive manner.
  void addLocations(dynamic loc) {
    location = addRecursively(loc['CompilationUnit']);
  }

  /// Goes down to the bottom of the tree and adds the AST Nodes as ASTLocation with its children.
  ASTLocation addRecursively(dynamic loc) {
    var children = <ASTLocation>[];
    if (loc['children'] != null) {
      for (var childLoc in loc['children']) {
        children.add(addRecursively(childLoc));
      }
    }
    var id = 'NaN';
    if (loc['id'] != null) {
      id = loc['id'];
    }
    return ASTLocation(
        loc['node'], id, loc['startposition'], loc['endposition'], children);
  }

  /// Loops over the AST Tree to find the AST Node with the closest position to [line].
  ///
  /// For example, given [line]="2:5" and we have a parent AST with start pos "1:0" and end pos "5:10" with a child with start "2:3" and end "2:7", we should return the child.
  ASTLocation findPosition(String line) {
    var pos;
    if (!line.contains(':') || !line.contains('-')) {
      return null;
    } else if (line.contains(':')) {
      pos = line.split(':');
    } else {
      pos = line.substring(1).split('-');
    }
    if (pos.length < 2) {
      // Its a line and not a row, col - pair.
      return findPositionRecursively(location, int.parse(pos[0]), 0);
    }
    return findPositionRecursively(
        location, int.parse(pos[0]), int.parse(pos[1]));
  }

  /// Recursive method for finding the closest position of the children of [current] ASTLocation at [line] : [col].
  ASTLocation findPositionRecursively(ASTLocation current, int line, int col) {
    ASTLocation res = null;
    if (current.isInPosition(line, col)) {
      res = current;
      var childrenList = <ASTLocation>[];
      for (var child in current.children) {
        //Checking the childs position, if it is in the position then continue the search
        if (child.isInPosition(line, col)) {
          //Saves child to list
          childrenList.add(child);
          var found = findPositionRecursively(child, line, col);
          if (found != null) {
            //Found a child to traverse through, returning it
            childrenList.add(found);
          }
        }
        // Find best child and set it to res
      }
      var bestChild = findClosestChild(childrenList, line, col);
      if (bestChild != null && bestChild.closerThan(current, line, col)) {
        res = bestChild;
      }
    }
    // No node could be found. Returning null or itself to symbolize this;
    return res;
  }

  /// Evaluates which of the children is closest to [line] : [col]
  ASTLocation findClosestChild(List<ASTLocation> list, int line, int col) {
    if (list.isNotEmpty) {
      var res = list[0];
      for (var child in list) {
        if (child.closerThan(res, line, col)) {
          res = child;
        }
      }
      return res;
    } else {
      return null;
    }
  }

  void astString(ASTLocation location) {
    if (location == null) {
      return;
    }
    if (location.astNode != null) {
      print(location.astNode);
    } else {
      print("null");
    }
    if (location.id != null) {
      print(location.id);
    } else {
      print("null");
    }

    print('\n');
    for (var child in location.children) {
      astString(child);
    }
  }
}

/// The front end version of a AST Node with its name, position, and children.
class ASTLocation {
  String astNode;
  String id;
  String startPosition;
  String endPosition;
  List<ASTLocation> children;

  ASTLocation(this.astNode, this.id, this.startPosition, this.endPosition,
      this.children);

  /// Returns the JSON representation of the ASTLocation with the timestamp [ts] and file name [fileName].
  String getStorageRepresentation(String ts, String fileName) {
    var jsonstring = jsonEncode({
      'node': '$astNode',
      'startPos': '$startPosition',
      'endPos': '$endPosition',
      'ts': ts,
      'file': fileName
    });
    return jsonstring;
  }

  /// Retrieves the start postition as a list: [row, col]
  List<int> getStartPos() {
    return startPosition.split(':').map((e) => int.parse(e)).toList();
  }

  /// Retrieves the end postition as a list: [row, col]
  List<int> getEndPos() {
    return endPosition.split(':').map((e) => int.parse(e)).toList();
  }

  /// Checks if the ASTLocation have the location [line]:[col] inside itself.
  bool isInPosition(int line, int col) {
    var startPos = getStartPos();
    var endPos = getEndPos();
    var res = false;

    // If the line numbers for start and end are lower than the input line.
    if (startPos[0] < line && endPos[0] > line) {
      res = true;
    } else if (startPos[0] == 0 &&
        endPos[0] == 0 &&
        startPos[1] == 0 &&
        endPos[1] == 0) {
      res = true;
    } else if (endPos[0] == line && startPos[0] == line) {
      res = (startPos[1] <= col && endPos[1] >= col) ? true : false;
    } else if (startPos[0] == line) {
      res = (startPos[1] <= col) ? true : false;
    } else if (endPos[0] == line) {
      res = (endPos[1] >= col) ? true : false;
    }
    return res;
  }

  /// Evaluates if this node is closer to [line]:[col] position than the other.
  /// Currently only checks if one of the ASTLocations have the position "0:0", since this means it is an imported AST Node that isn't really a part
  /// of the AST Tree.
  bool closerThan(ASTLocation other, int line, int col) {
    if (isZeroZero()) {
      return false;
    } else if (other.isZeroZero()) {
      return true;
    }

    return true;
  }

  /// Checks if the ASTLocations have the position "0:0", since this means it is an imported AST Node that isn't really a part
  /// of the AST Tree.
  bool isZeroZero() {
    var thisStart = getStartPos();
    var thisEnd = getEndPos();
    return (thisStart[0] == 0 &&
        thisEnd[0] == 0 &&
        thisStart[1] == 0 &&
        thisEnd[1] == 0);
  }
}
