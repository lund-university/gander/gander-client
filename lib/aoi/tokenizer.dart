import 'package:highlighting/highlighting.dart';
import 'package:path/path.dart' as p;
import 'dart:html';

class Tokenizer {
  static Map<String, Set<String>> tokens = {};

  static final Map<String, String> extensionToLanguageId = {
    '.java': 'java',
    '.gradle': 'groovy',
    '.groovy': 'groovy',
    '.gvy': 'groovy',
    '.gy': 'groovy',
    '.gsh': 'groovy',
    '.scala': 'scala',
    '.md': 'markdown',
    '.yml': 'yaml',
    '.yaml': 'yaml',
    '.go': 'go',
    '.js': 'javascript',
    '.cjs': 'javascript',
    '.mjs': 'javascript',
    '.ts': 'typescript',
    '.tsx': 'typescript',
    '.cts': 'typescript',
    '.mts': 'typescript',
    '.cpp': 'cpp',
    '.cxx': 'cpp',
    '.cc': 'cpp',
    '.h': 'cpp',
    '.hpp': 'cpp',
    '.c': 'c',
    '.py': 'python'
};

  static String generateSpan(
      String code, String fileName, String sanitizedName, String type, int lineNmbr) {

    var languageId = getLanguageId(fileName);
    var res = highlight.parse(code, languageId: languageId);

    var str = StringBuffer();
    var col = 1;
    if (!tokens.containsKey(sanitizedName)) {
      tokens[sanitizedName] = {};
    }
    void traverseNodes(Node node) {
      final shouldAddSpan = node.className != null &&
          ((node.value != null && node.value.isNotEmpty) ||
              (node.children != null && node.children.isNotEmpty));
      if (shouldAddSpan) {
        //print('Added span for: ${node.className}');
        var prefix = node.noPrefix ? '' : 'hljs-';
        str.write('<span class="${prefix + node.className}" ');
      } else {
        str.write('<span ');
      }

      if (node.value != null) {
        var codeToken = node.value;
        var codeTokenLength = codeToken.length;
        str.write('id=$sanitizedName-$type-l$lineNmbr:$col>');

        tokens[sanitizedName].add('$sanitizedName-$type-l$lineNmbr:$col');
        col += codeTokenLength;
        str.write(_escape(node.value));
      } else if (node.children != null) {
        str.write('>');
        node.children.forEach((el) => traverseNodes(el));
      }
      //'$sanitizedName-$type-l$lineNbr:$colNbr' is id
      //  if (shouldAddSpan) {
      //    str += '</span>';
      //  }
      str.write('</span>');
    }

    for (var node in res.nodes) {
      traverseNodes(node);
    }
    //print(tokens);
    return str.toString();
  }

  static String getLanguageId(String fileName) {
    var extension = p.extension(fileName);
    return extensionToLanguageId[extension] ?? 'plaintext';
  }

  static String traverseNodes(Node node, String str) {
    final shouldAddSpan = node.className != null &&
        ((node.value != null && node.value.isNotEmpty) ||
            (node.children != null && node.children.isNotEmpty));
    if (shouldAddSpan) {
      var prefix = node.noPrefix ? '' : 'hljs-';
      str += '<span class="${prefix + node.className}">';
    } else {
      str += '<span>';
    }

    if (node.value != null) {
      str += _escape(node.value);
      print('hej');
    } else if (node.children != null) {
      node.children.forEach((el) => traverseNodes(el, str));
    }

    //  if (shouldAddSpan) {
    //    str += '</span>';
    //  }
    str += '</span>';
    return str;
  }

  static String _escape(String value) {
    return value
        .replaceAll(RegExp(r'&'), '&amp;')
        .replaceAll(RegExp(r'<'), '&lt;')
        .replaceAll(RegExp(r'>'), '&gt;');
  }
}
