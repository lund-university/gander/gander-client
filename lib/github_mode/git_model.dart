/// TODO DOCUMENTATION
import 'dart:convert';

import '../file/compare/compare_model.dart';
import '../gander_package.dart';
import '../service_models/gander_service_model.dart';

class GitModel {
  static const _HOST = 'http://localhost';
  static const _GITPATH = '/pur/git';
  static const _PULLPATH = '/pulls';
  static const _DIFFPATH = '/diff';
  static const _FILEPATH = '/files';
  var beenHere = [];

  static const int _PORT = 8001;

  GitModel();

  Future<PullRequestDetails> getPullRequestFromNbr(int pullNumber) async {
    try {
      var result = await GanderServiceModel.getRequest(
          '$_HOST:$_PORT$_GITPATH$_PULLPATH/$pullNumber');

      return _processPullRequestFromNbrResponse(result);
    } catch (e) {
      print("Couldn't open $_HOST$_PORT$_GITPATH$_PULLPATH");
      rethrow;
    }
  }

  Future<List<PullRequestFile>> getPullRequestFiles(int pullNumber) async {
    try {
      var result = await GanderServiceModel.getRequest(
          '$_HOST:$_PORT$_DIFFPATH$_PULLPATH/$pullNumber$_FILEPATH');

      return _processFilesResponse(result);
    } catch (e) {
      print(
          'Couldn\'t open $_HOST:$_PORT$_DIFFPATH$_PULLPATH/$pullNumber$_FILEPATH');
      rethrow;
    }
  }

  List<PullRequestFile> _processFilesResponse(String jsonString) {
    var fileList = <PullRequestFile>[];
    List<dynamic> jsonFileList = json.decode(jsonString);
    for (var file in jsonFileList) {
      //To skip files with no changes and images (which are not supported)
      if (file['additions'] == 0 && file['deletions'] == 0) {
        continue;
      }
      //In case of renaming, the file content is null, which causes Ganderutils.decodeContent to crash.
      var encodedContent = file['content'] ?? '';

      var content = GanderUtils.decodeContent(encodedContent);
      var jsonPatch = file['patch'];

      var filename = file['filename'];
      var patchList;
      if (jsonPatch != null) {
        patchList = parsePatchFromMap(filename, jsonPatch);
      }
      var pullRequestFile = PullRequestFile(
          filename,
          file['status'],
          file['additions'],
          file['deletions'],
          patchList,
          content,
          file['path']);

      fileList.add(pullRequestFile);
    }
    return fileList;
  }

  PullRequestDetails _processPullRequestFromNbrResponse(String jsonString) {
    Map<String, dynamic> jsonPullRequest = json.decode(jsonString);
    List<dynamic> jsonLabels = jsonPullRequest['labels'];
    var labels = getLabels(jsonLabels);
    List<dynamic> jsonCommits = jsonPullRequest['commits'];
    var commits = getCommits(jsonCommits);
    var pullRequestDetails = PullRequestDetails(
        jsonPullRequest['title'],
        jsonPullRequest['id'],
        jsonPullRequest['number'],
        jsonPullRequest['body'],
        jsonPullRequest['state'],
        jsonPullRequest['created_at'],
        jsonPullRequest['merged_at'],
        labels,
        jsonPullRequest['milestone'],
        commits);
    return pullRequestDetails;
  }

  List<Label> getLabels(List<dynamic> jsonLabels) {
    var labelList = <Label>[];
    for (var current in jsonLabels) {
      var label =
          Label(current['name'], current['description'], current['color']);
      labelList.add(label);
    }
    return labelList;
  }

  List<PullRequestCommit> getCommits(List<dynamic> jsonCommits) {
    var commitList = <PullRequestCommit>[];
    for (var current in jsonCommits) {
      var commit = PullRequestCommit(current['sha'], current['message']);
      commitList.add(commit);
    }
    return commitList;
  }

  Future<List<PullRequest>> getPullRequests() async {
    try {
      var result = await GanderServiceModel.getRequest(
          '$_HOST:$_PORT$_GITPATH$_PULLPATH');

      return _processPullRequestResponse(result);
    } catch (e) {
      print("Couldn't open $_HOST$_PORT$_GITPATH$_PULLPATH");
      rethrow;
    }
  }

  List<PullRequest> _processPullRequestResponse(String jsonString) {
    var pullRequestList = <PullRequest>[];

    List<dynamic> pullRequests = json.decode(jsonString);
    for (var jsonPullRequest in pullRequests) {
      var rev = '';
      if ((jsonPullRequest['number'] == 571 ||
              jsonPullRequest['number'] == 613 ||
              jsonPullRequest['number'] == 610) &&
          !beenHere.contains(jsonPullRequest['number'])) {
        rev = 'fix';
      } else if (jsonPullRequest['number'] == 50 &&
          !beenHere.contains(jsonPullRequest['number'])) {
        rev = 'review';
      }
      var pullRequest = PullRequest(
          jsonPullRequest['id'],
          jsonPullRequest['number'],
          jsonPullRequest['title'],
          jsonPullRequest['body'],
          jsonPullRequest['state'],
          jsonPullRequest['created_at'],
          jsonPullRequest['merged_at'],
          rev);
      pullRequestList.add(pullRequest);
    }
    return pullRequestList;
  }

  Future<PullRequestDetails> getPullRequestFromUrl(String url) async {
    try {
      var formattedUrl = getFormattedUrl(url);
    
      var result = await GanderServiceModel.getRequest(
          '$_HOST:$_PORT$_GITPATH$_PULLPATH/$formattedUrl');

      return _processPullRequestFromNbrResponse(result);
    } catch (e) {
      print("Couldn't open $_HOST$_PORT$_GITPATH$_PULLPATH");
      rethrow;
    }
  }

  Future<List<PullRequestFile>> getPullRequestFilesFromUrl(String url) async {
    try {
      var formattedUrl = getFormattedUrl(url);

      var result = await GanderServiceModel.getRequest(
          '$_HOST:$_PORT$_DIFFPATH$_PULLPATH/$formattedUrl$_FILEPATH');

      return _processFilesResponse(result);
    } catch (e) {
      print(
          'Couldn\'t open $_HOST:$_PORT$_DIFFPATH$_PULLPATH/$_FILEPATH');
      rethrow;
    }
  }

  String getFormattedUrl(String url) {
    var uri = Uri.parse(url);
    var segments = uri.pathSegments;

    if (segments.length >= 4 && segments[2] == 'pull') {
      return '${segments[0]}/${segments[1]}/${segments[3]}';
    } else {
      throw FormatException('URL does not match expected structure');
    }
  }
}

class PullRequestFile extends Comparison {
  String path;
  String content;

  PullRequestFile(String filename, String status, int additions, int deletions,
      List<Patch> patchList, this.content, this.path)
      : super(filename, status, additions, deletions, patchList);
}

class PullRequest {
  final int id;
  final int number;
  final String title;
  final String body;
  final String state;
  final String createdAt;
  final String mergedAt;
  String review;
  PullRequest(this.id, this.number, this.title, this.body, this.state,
      this.createdAt, this.mergedAt, this.review);
}

class PullRequestDetails {
  final String title;
  final int id;
  final int number;
  final String body;
  final String state;
  final String createdAt;
  final String mergedAt;
  final List<Label> labels;
  final String milestoneTitle;
  final List<PullRequestCommit> commits;
  const PullRequestDetails(
      this.title,
      this.id,
      this.number,
      this.body,
      this.state,
      this.createdAt,
      this.mergedAt,
      this.labels,
      this.milestoneTitle,
      this.commits);
}

class PullRequestCommit {
  final String sha;
  final String message;
  const PullRequestCommit(this.sha, this.message);
}

class Label {
  final String name;
  final String description;
  final String color;
  const Label(this.name, this.description, this.color);
}
