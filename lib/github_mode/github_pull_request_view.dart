/// TODO DOCUMENTATION
import 'dart:html';

import 'package:gander/connection/in_loop_ws_client.dart';
import 'package:intl/intl.dart';
import '../aoi/ast_converter.dart';
import '../file/compare/compare_model.dart';
import '../comments/annotation_model.dart';
import 'git_model.dart';
import '../file/file_model.dart';
import 'github_pull_request_details_view.dart';
import 'package:collection/collection.dart';

class PullRequestView {
  GitModel gitModel;
  GanderCompare compareModel;
  AnnotationModel annModel;
  PullRequestDetailsView pullRequestDetailsView;
  CommentCollection commentColl;
  Function interactionCallback;
  InLoopWsClient socket;
  var source = './favicon.ico';
  PullRequestView(this.gitModel, this.compareModel, this.annModel,
      this.interactionCallback, this.socket);

  Future<void> generatePullRequestList() async {
    querySelector('.container-body').innerHtml = '';
    await displayPullRequestList();
    await FileCollectionModel.fetchSupportedExtensions();
  }

  void displayPullRequestList() async {
    var pullRequestList = await gitModel.getPullRequests();

    var cardDiv = DivElement()
      ..className = 'card'
      ..id = 'pull-request-view-card';
    var labelSrc = HeadingElement.h1()
      ..text = 'Pull Requests'
      ..id = 'pull-list-label';

    cardDiv.append(labelSrc);

    cardDiv.append(generateUrlForm());

    var list = UListElement()..id = 'pull-list';

    for (var current in pullRequestList) {
      var pullRequestId = current.id;
      var changeObjList = UListElement()
        ..id = 'commit-list-$pullRequestId'
        ..className = 'pr-info-list';
      var smallCardDiv = DivElement()
        ..className = 'card pull-request-item'
        ..id = 'commit-div-$pullRequestId';

      var title = HeadingElement.h5()
        ..id = 'commit-list-item-title-$pullRequestId'
        ..className = 'list-item pr-title'
        ..text = current.title;

      var createdAt = DateTime.parse(current.createdAt);
      var formatter = DateFormat('yyyy-MM-dd');
      var createdAtOnlyDate = formatter.format(createdAt);

      var infoString = LIElement()
        ..text =
            '#${current.number.toString()} · created at: ${createdAtOnlyDate.toString()}'
        ..className = 'pr-info';
      var assignedBox;
      if (current.review != '') {
        assignedBox = DivElement()
          ..id = 'assignedBox-${current.id}'
          ..hidden = false
          ..className = 'assignedBox';

        var image = ImageElement()
          ..id = 'rewievPic'
          ..width = 48
          ..height = 48
          ..src = source;

        assignedBox.appendText((current.review == 'review')
            ? 'Assigned to review'
            : 'Feedback Available');
        assignedBox.append(image);
      }
      changeObjList.append(title);
      changeObjList.append(infoString);
      if (current.review != '') {
        changeObjList.append(assignedBox);
      }
      smallCardDiv.append(changeObjList);
      //smallCardDiv.append(assignedBox);
      list.append(smallCardDiv);

      //      smallCardDiv.onClick.listen((e) => {displayPullRequest(e, current)});
      smallCardDiv.addEventListener('click', (event) {
        try {
          socket.webSocket.send(
              '{"action": "enterPR", "data": "${current.number}-${current.title}"}');
        } catch (e) {
          print('couldnt access fixation service');
        }
        displayPullRequest(event, current);
        gitModel.beenHere.add(current.number);
      }, true);
      //smallCardDiv.addEventListener('mouseenter', (event) => {smallCardDiv.style.backgroundColor = 'black'}, true);
      //smallCardDiv.onFocus.listen((e) {
      //  e.preventDefault();
      //  changeObjList.style.backgroundColor = 'blue';
      //  });
      changeObjList.addEventListener('mouseenter',
          (event) => {changeObjList.style.backgroundColor = mouseHovers(true)});

      changeObjList.addEventListener('mouseout', (event) {
        changeObjList.style.backgroundColor = mouseHovers(false);
      });
    }
    cardDiv.append(list);

    querySelector('.container-body').append(cardDiv);
  }

  String mouseHovers(bool isOver) {
    return isOver ? '#f1f1f1' : 'white';
  }

  DivElement getLoader() {
    return DivElement()..className = 'loader';
  }

  void displayPullRequest(MouseEvent e, PullRequest current) async {
    interactionCallback(e);

    //Change the cursor to a loader
    querySelector('body').className = 'wait';

    var pullNumber = current.number;
    //KOLLAHÄR
    await annModel.getComments('github', pullNumber.toString());

    commentColl = annModel.commentCollections.firstWhere(
        (c) => c.number == current.number.toString(),
        orElse: () => null);
    
    var pullRequestToDisplay = await gitModel.getPullRequestFromNbr(pullNumber);
    pullRequestDetailsView = PullRequestDetailsView(
        pullRequestToDisplay,
        null,
        returnToPullRequestList,
        gitModel,
        commentColl,
        interactionCallback,
        annModel.postComment);
    await pullRequestDetailsView.generatePullRequestDetailsView();
  }

  void returnToPullRequestList(MouseEvent e) {
    interactionCallback(e);
    generatePullRequestList();
  }

  void displayComments(int pullNumber) {
    var comments = annModel.commentCollections;
    var req = comments.firstWhere((c) => c.number == pullNumber.toString(),
        orElse: () => null);
    commentColl = req;
    pullRequestDetailsView.generateOverviewComments(req);
  }

  // ASTFile getFileInChangeView(String sanitizedName) {
  //   //Could be refactored into simpler format.
  //   //var file = pullRequestDetailsView?.getFileInChangeView(sanitizedName);
  //   //return file != null ? file : null;
  //   if (pullRequestDetailsView != null) {
  //     var file = pullRequestDetailsView.getFileInChangeView(sanitizedName);
  //     if (file != null) {
  //       return file;
  //     }
  //   }
  //   return null;
  // }

  void setSrc(String src) {
    source = src;
  }

  DivElement generateUrlForm() {
    var button = ButtonElement()
      ..text = 'Submit'
      ..id = 'loginButton';

    button.disabled = true;

    var flexBox = DivElement()
      ..className = 'flex-container';
    var label = Element.p()
      ..text = 'Url:'
      ..id = 'url-label';

    var urlBox = TextInputElement()..id = 'urlBox';
    urlBox.onInput.listen((event) {
      interactionCallback(event, urlBox.value.toString());
      button.disabled = urlBox.value.isEmpty;
    });

    button.addEventListener('click', (event) => {
      loadPullRequestByUrl(event, urlBox.value.toString())
    }, true);

    flexBox.append(label);
    flexBox.append(urlBox);
    flexBox.append(button);

    return flexBox;
  }

  void loadPullRequestByUrl(Event event, String url) async {
    interactionCallback(event);

    var pullRequestToDisplay = await gitModel.getPullRequestFromUrl(url);

    try {
      socket.webSocket.send(
        '{"action": "enterPR", "data": "99999-${pullRequestToDisplay.title}"}');
    } catch (e) {
      print('couldnt access fixation service');
    }

    //Change the cursor to a loader
    querySelector('body').className = 'wait';

    commentColl = CommentCollection('99999');

    pullRequestDetailsView = PullRequestDetailsView(
        pullRequestToDisplay,
        url,
        returnToPullRequestList,
        gitModel,
        commentColl,
        interactionCallback,
        annModel.postComment);
    await pullRequestDetailsView.generatePullRequestDetailsView();
  }
}
