/// TODO DOCUMENTATION

import 'package:markdown/markdown.dart' show markdownToHtml;
import 'dart:html';
//import 'aoi_handler.dart';
import '../comments/annotation_model.dart';
import '../aoi/ast_converter.dart';
import '../file/compare/compare_model.dart';
import 'git_model.dart';
import '../gander_package.dart';
import '../file/content_area_handler.dart';

// import 'package:redux/redux.dart';

class PullRequestDetailsView {
  PullRequestDetails pullRequestToDisplay;
  String url;
  //FileCollectionModel fileCollModel;
  Function returnToPullRequestList;
  Function interactionCallback;
  Function postCommentCallback;
  GitModel gitModel;
  List<ChangeView> changeViews = [];
  CommentCollection commentColl;
  PullRequestPatch pullRequestPatch;
  List<PullRequestFile> fileList;
  DivElement cardDiv;

  static const String currentTab = 'current-tab';

  PullRequestDetailsView(
      this.pullRequestToDisplay,
      this.url,
      this.returnToPullRequestList,
      this.gitModel,
      this.commentColl,
      this.interactionCallback,
      this.postCommentCallback);

  Future<void> generatePullRequestDetailsView() async {
    if (url == null) {
      fileList = await gitModel.getPullRequestFiles(pullRequestToDisplay.number);
    } else {
      fileList = await gitModel.getPullRequestFilesFromUrl(url);
    }

    //var navigation = getNavigationBar();
    querySelector('.container-body').innerHtml = '';

    //Ensures that the scroll is reset when replaying the session
    window.scrollTo(0, 0);

    //Stop the cursor from being a loader
    querySelector('body').className = '';
    var backButton = ButtonElement()
      ..text = 'Back to list'
      ..id = 'back-to-pull-request-overview-button'
      ..onClick.listen(returnToPullRequestList);

    cardDiv = DivElement()
      ..id = 'pull-request-view-card'
      ..className = 'card';
    var labelSrc = HeadingElement.h1()..text = pullRequestToDisplay.title;

    //The div that will contain the body text of the PR or the cange view
    var currentlyShowingDiv = DivElement()..id = 'currently-showing-div';
    cardDiv.append(getTopPanel(labelSrc, backButton));
    //cardDiv.append(navigation);
    cardDiv.append(currentlyShowingDiv);

    querySelector('.container-body').append(cardDiv);

    //Needed to solve the problem with comments duplicating during replay
    //Should be revised if problems occur again
    removeDuplicateComments();

    //displayOverview();
    await displayContentView();
  }

  void removeDuplicateComments() {
    var tempColl = commentColl;

    //Check for duplicates in commentColl. If a comment with same body and on
    //the same line exists, just add one of them
    for (var com in commentColl.comments) {
          if (tempColl.comments
                  .where((c) => c.body == com.body && c.line == com.line)
                  .length >
              1) {
            tempColl.comments.remove(com);
          }
        }
  }

  DivElement getNavigationBar() {
    var div = DivElement()..className = 'w3-light-grey pull-request-bar';
    var overview = Element.a()
      ..text = 'Overview'
      ..id = 'pull-request-overview-button'
      ..className = 'w3-bar-item w3-button ' + currentTab;
    div.append(overview);
    var changes = Element.a()
      ..text = 'Changes'
      ..id = 'pull-request-change-view-button'
      ..className = 'w3-bar-item w3-button ';
    div.append(changes);
    changes.onClick.listen((event) async {
      interactionCallback(event);
      //We have to calculate the Y-pos of where the change view start and also remove the hegiht of the page header as to not scroll to far.
      var diff = document
          .querySelector('.page-header-container')
          .getBoundingClientRect()
          .height;
      var scrollValue = document
              .querySelector('#pullRequestConentDiv')
              .getBoundingClientRect()
              .top -
          diff;
      //Make the window scroll into screen when pressing changes-btn
      window.scroll({'left': 0, 'top': scrollValue, 'behavior': 'smooth'});
    });
    overview.onClick.listen((event) {
      interactionCallback(event);
      changeCurrentButton(overview);
      displayOverview();
    });

    return div;
  }

  Future<void> displayContentView() async {
    var prViewCard = querySelector('#pull-request-view-card');

    // fileCollModel.useDefMap = {};
    // aoi = AOIHandler(interactionCallback, fileCollModel.getUseDefMap());

    //var contentDiv = await getContentView(fileList);
    //contentDiv.id = 'pullRequestContentDiv';
    ////Sets the same style as the overview, for consistency
    //contentDiv.className = 'card';
    List<ContentAreaHandler> contentAreaList = [];
    for (var file in fileList) {
      if (file.patchList != null) {
        var view = ContentAreaHandler(file, commentColl, file.content,
            interactionCallback, postCommentCallback,
            commentsAreEnabled: true);

        contentAreaList.add(view);
      }
    }

    var contentDiv = await getPrDetailsViewDiv(contentAreaList);

    prViewCard.append(contentDiv);

    //For all views in this pull request, generate AOI elements
    //Currently has to be done after the view has been painted
    //look into letting the view handle this.
    for (var a in contentAreaList) {
      a.generateAOI();
    }
  }

  Future<DivElement> getPrDetailsViewDiv(
      List<ContentAreaHandler> contentAreaList) async {
    var prViewCard = querySelector('#pull-request-view-card');
    prViewCard.innerHtml = '';
    var contentDiv = await getContentView(contentAreaList);
    contentDiv.id = 'pullRequestContentDiv';
    //Sets the same style as the overview, for consistency
    contentDiv.className = 'card';

    return contentDiv;
  }

  ///Creates the content view, with the files and the comments.
  Future<DivElement> getContentView(
      List<ContentAreaHandler> contentAreaList) async {
    var contentAreaDiv = DivElement();
    for (var contentArea in contentAreaList) {
      var viewDiv = await contentArea.getContentView();

      // var store = Store<int>(displayReducer, initialState: 0);
      // store.onChange.listen((state) async {
      //   contentArea.switchView(state);
      //   var parent = div.parent;
      //   div.remove();
      //   parent.append(await getPrDetailsViewDiv(contentAreaList));
      // });

      // var switchButton = SwitchViewToggleButton();
      //viewDiv.insertBefore(switchButton, viewDiv.firstChild);
      contentAreaDiv.append(viewDiv);

      // contentDiv.querySelector('#switch-view-button').onChange.listen((e) {
      //     store.dispatch(contentArea.currentDisplay);
      //   });
    }
    return contentAreaDiv;
  }

  // int displayReducer(int state, action) {
  //   if (action == Display.changeView) {
  //     return Display.fileView.index;
  //   } else if(action == Display.fileView) {
  //     return Display.changeView.index;
  //   }
  //   return state;
  // }

  ///Creates the overview view, with comments and pullRequest infomration
  void displayOverview() {
    var body = DivElement()
      ..innerHtml = markdownToHtml(pullRequestToDisplay.body)
      ..className = 'pull-request-body'
      ..id = 'pull-request-body-div';
    var currentlyShowingDiv = querySelector('#currently-showing-div');
    currentlyShowingDiv.innerHtml = '';
    var approvePR = FormElement()
      ..id = 'pull-request-${pullRequestToDisplay.number}-approve'
      ..className = 'approveform';

    var approvePRCheckBox = CheckboxInputElement()
      ..id = 'pull-request-${pullRequestToDisplay.number}-approve-checkbox'
      ..className = 'approvePRcheckbox';

    var checkBoxLabel = DivElement()
      ..id = 'pull-request-${pullRequestToDisplay.number}-approve-checkbox-text'
      ..className = 'checkboxlabel'
      ..text = 'Approve entire Pull Request';
    var approvePRButton = ButtonElement()
      ..id = 'pull-request-${pullRequestToDisplay.number}-approve-button'
      ..text = 'Approve'
      ..onClick.listen((event) => {interactionCallback(event)});

    approvePR.append(approvePRCheckBox);
    approvePR.append(checkBoxLabel);
    approvePR.append(approvePRButton);
    currentlyShowingDiv.append(body);
    currentlyShowingDiv.append(approvePR);
    generateOverviewComments(commentColl);

    displayContentView();
  }

  ///Generates the comments of the Overview view.
  void generateOverviewComments(CommentCollection comColl) {
    commentColl = comColl;
    var commentContainer = createCommentSection(comColl.comments.length);

    for (var comment in comColl.comments) {
      commentContainer.append(createCommentContainer(comment));
    }
    querySelector('#pull-request-view-card').append(commentContainer);
  }

  /// Creates the comment section shown on the overview page.
  DivElement createCommentSection(int length) {
    var commSectionDiv = querySelector('#comment-section-div-card');
    if (commSectionDiv != null) {
      commSectionDiv.remove();
    }

    commSectionDiv = DivElement()
      ..className = 'card'
      ..id = 'comment-section-div-card';
    commSectionDiv.append(createCommentHeader(length));
    return commSectionDiv;
  }

  /// Creates the header for the comment section.
  DivElement createCommentHeader(int length) {
    return DivElement()
      ..text = 'Comments: ($length)'
      ..id = 'comment-section-header';
  }

  /// Creates the body of the comment section, who has commented, in what file, on what line, and the content of the comment.
  DivElement createCommentContainer(Comment comment) {
    var commentDiv = DivElement();
    var row = DivElement()..className = 'row';
    var fileCol = DivElement()
      ..className = 'col'
      ..text = 'In file ${comment.filePath}'
      ..id = 'comment-container-file-column';
    var lineCol = DivElement()
      ..className = 'col'
      ..text = 'Line ${comment.line}'
      ..id = 'comment-container-line-column';

    //Create a link to the section where this comment can be seen.
    var contentCol = AnchorElement()
      ..className = 'col'
      ..innerHtml = markdownToHtml(comment.body)
      ..id = 'comment-container-content-column';
    var sanFile = GanderUtils.santizeFileName(comment.filePath);
    contentCol.href = '#$sanFile-comment-div-${comment.line}-posted-comments';

    row.append(fileCol);
    row.append(lineCol);
    row.append(contentCol);

    commentDiv.append(row);
    return commentDiv;
  }

  void changeCurrentButton(Element currentButton) {
    var previouslyButton = querySelector('.$currentTab');
    previouslyButton.classes.remove(currentTab);
    currentButton.classes.add(currentTab);
  }

  UListElement getTopPanel(Element label, ButtonElement btn) {
    var panelList = UListElement()..id = 'pull-request-panel';

    //The state is not added to not confuse the during the pilot
    /*var state = LIElement()
      ..text = pullRequestToDisplay.state
      ..className = 'pull-request-state ${pullRequestToDisplay.state} panelLi';
    panelList.append(state);*/

    var createdAt = LIElement()
      ..text = 'Created at: ${pullRequestToDisplay.createdAt.toString()}'
      ..className = 'panelLi';
    panelList.append(btn);
    panelList.append(label);
    panelList.append(createdAt);

    //The merge date is not added to not confuse the during the pilot
    /*var mergeDate = pullRequestToDisplay.mergedAt;
    if (mergeDate != null) {
      var mergedAt = LIElement()
        ..text = 'Merged at: ${mergeDate.toString()}'
        ..className = 'panelLi';
      panelList.append(mergedAt);
    }*/

    return panelList;
  }

  // ASTFile getFileInChangeView(String sanitizedName) {
  //   //print(fileCollModel.fileModels.toString());
  //   //Could be refactored
  //   for (var changeView in changeViews) {
  //     var file = changeView.astFile;
  //     if (file == null) {
  //       continue;
  //     }
  //     var fileSanitized = GanderUtils.santizeFileName(file.fileName);
  //     if (fileSanitized == sanitizedName) {
  //       return file;
  //     }
  //   }
  //   return null;
  // }
}
