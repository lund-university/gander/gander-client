import 'dart:html';

class GanderReplayWebsocket {
  // Commands needed to connect to the websocket
  static String CONNECT_COMMAND = '{"action": "connect"}';
  static String START_SENDING_LOGS = '{"action": "ready"}';
  static String START_SENDING_FIXATED_LOGS = '{"fixation":"true"}';
  static String STOP_USING_FIXATION = '{"fixation":"false"}';
  var fixation;

  var log_list = [];
  bool logDone = false;

  GanderReplayWebsocket();

  WebSocket webSocket;

  /// Initializes the connection with the eye-tracker.
  void init(bool fixate) {
    fixation = fixate;
    webSocket = WebSocket('ws://localhost:3004');

    /// Sends the necessary actions to receive the streams of coordinates.
    webSocket.onOpen.listen((event) {
      webSocket.sendString(CONNECT_COMMAND);
       if (fixation) {
         webSocket.sendString(START_SENDING_FIXATED_LOGS);
       }
       webSocket.sendString(START_SENDING_LOGS);
    });
    webSocket.onMessage.listen((MessageEvent e) {
      //if (replayReady) {
      //  var data = e.data.toString();
      //  log_list.add(data);
      //} else {
      //  if (e.data.toString() == 'ready') {
      //    replayReady = true;
      //  }
      //}
      var data = e.data.toString();

      if (data == 'done') {
        logDone = true;
      } else {
        log_list.add(data);
      }
      // write something about saving the inputs in log
    });

    /// What the client should do on errors
    webSocket.onError.listen((Event e) {
      print('Error connecting to the websocket: $e');
    });

    /// What should be done when closing the connection.
    webSocket.onClose.listen((CloseEvent e) {
      webSocket.sendString(STOP_USING_FIXATION);
      print('Closed connection');
    });
  }

  void close() {
    webSocket.sendString(STOP_USING_FIXATION);
  }

  Future<bool> fetchLogs() async {
    //Wait for all logs to be fetched
    while (!logDone) {
      await Future.delayed(Duration(milliseconds: 100));
    }
    return true;
  }

  List getLogs() {
    return log_list;
  }

  void testSort(String s) {
    webSocket.sendString(s);
  }
}
