import 'dart:html';

import 'replay_model.dart';

class ReplayView {
  Function visibleElementsCallback;
  ReplayModel replay;
  String path = '';
  List<bool> options = [false, false, false];

  ReplayView(this.replay);

  Future<bool> generateStartReplayPage() async {
    querySelector('.container-body').innerHtml = '';
    querySelector('.container-navigation').innerHtml = '';
    var res = await displayForm();
    return res;
  }

  /// Adds the start replay page to container
  Future<bool> displayForm() async {
    
    var button = ButtonElement()
      ..text = 'Set Session'
      ..id = 'checkPathButton'
      ..style.margin = '10px'
      ..onClick.listen((e) {
        //if there already is a button, remove it so that a replay can't start without all the data.
        if(querySelector('#StartReplayButton') != null) {
          querySelector('#StartReplayButton').remove();
        }

        var chosenOption = 'Experiment';

        if (chosenOption == 'Analysis') {
          displayAnalysisForm();
          replay.setUseFixation();
        }
        replay.setReplayType(chosenOption == 'Experiment');
        var elem2 = querySelector('#choosePath') as SelectElement;
        var chosenFile = elem2.selectedOptions[0].value;

        postPathforFile(e, chosenFile);

      });
    //button.disabled = true;

    var cardDiv = DivElement();
    var header = HeadingElement.h1()
      ..text = 'Replay'
      ..id = 'replay-heading';
    var label = Element.p()
      ..text = 'Choose the date and time of your session:'
      ..id = 'path-label';

    var nameBox = TextInputElement()
      ..style.margin = '3px'
      ..id = 'pathBox';
    nameBox.onInput.listen((event) {
      path = nameBox.value;
      if (path.isNotEmpty) {
        button.disabled = false;
      } else {
        button.disabled = true;
      }
    });

    var choose_replay_type = SelectElement()
      ..style.margin = '5px'
      ..id = 'replayType';

    var option1 = OptionElement()
      ..text = 'Experiment'
      ..selected = true;
    option1.onSelect.listen((event) => {option1.selected = !option1.selected});

    var option2 = OptionElement()
      ..selected = false
      ..text = 'Analysis';
    option2.onSelect.listen((event) => {option2.selected = !option2.selected});
    choose_replay_type.children.add(option1);
    choose_replay_type.children.add(option2);

    var choose_file = SelectElement()
      ..style.margin = '5px'
      ..id = 'choosePath';
    var i = 0;
    await replay.getPaths().then((fileList) {
      try {
        fileList.sort();
        for (var file in fileList.reversed) {
          var text;
          try {
            text = file.substring(4, 6) +
                '-' +
                file.substring(6, 8) +
                ' ' +
                ((int.parse(file.substring(8, 10)) + 1) % 24).toString() +
                ':' +
                file.substring(10, 12);
          } catch (e) {
            text = file;
          }
          var option = OptionElement()
            ..text = text
            ..value = file
            ..selected = (i == 0);
          option.onSelect
              .listen((event) => {option.selected = !option.selected});
          i++;
          choose_file.children.add(option);
        }
      } catch (e) {
        displayErrorMessage('Problem retrieving files');
      }
    });

    cardDiv.append(header);
    cardDiv.append(label);
    cardDiv.append(choose_file);

    ///cardDiv.append(nameBox);

    cardDiv.append(button);

    querySelector('.container-body').append(cardDiv);
    return true;
  }

  void postPathforFile(MouseEvent event, String path) async {
    print(path);
    var success = await replay.postPathforLogs(path);
    if (!success) {
      displayErrorMessage('Path did not work, try another:');
    } else if (querySelector('#StartReplayButton') == null) {
      displayStartbutton();
    }
  }

  void displayAnalysisForm() {
    var carDiv = DivElement();
    var info = Element.p()
      ..text = 'Choose the features for analysis:'
      ..id = 'analysis-info';
    var Analysischecks = FormElement()..id = 'analysis-checkbox-form';

    var analysisCheck1 = CheckboxInputElement()
      ..id = 'checkbox-semantic'
      ..className = 'checkboxAnalysis'
      ..onClick.listen((event) => {options[0] = !options[0]});

    var checkBoxLabel1 = DivElement()
      ..id = 'checkboxtext-semantic'
      ..text = 'Display semantic relationship';
    var div1 = DivElement()..className = 'replay-checkDiv';
    div1.append(analysisCheck1);
    div1.append(checkBoxLabel1);

    var analysisCheck2 = CheckboxInputElement()
      ..id = 'checkbox-highlighting'
      ..className = 'checkboxAnalysis'
      ..onClick.listen((event) => {options[1] = !options[1]});

    var checkBoxLabel2 = DivElement()
      ..id = 'checkboxtext-highlighting'
      ..text = 'Highlight code looked at';

    var div2 = DivElement()..className = 'replay-checkDiv';
    div2.append(analysisCheck2);
    div2.append(checkBoxLabel2);

    var analysisCheck3 = CheckboxInputElement()
      ..id = 'checkbox-fixation'
      ..className = 'checkboxAnalysis'
      ..onClick.listen((event) {
        options[2] = !options[2];
      });

    var checkBoxLabel3 = DivElement()
      ..id = 'checkboxtext-fixation'
      ..text = 'Display fixation points';

    var div3 = DivElement()..className = 'replay-checkDiv';
    div3.append(analysisCheck3);
    div3.append(checkBoxLabel3);

    Analysischecks.append(div1);
    Analysischecks.append(div2);
    Analysischecks.append(div3);
    carDiv.append(info);
    carDiv.append(Analysischecks);

    querySelector('.container-body').append(carDiv);
  }

  void displayStartbutton() async {
    //before showing this button, make sure all logs are sent
    
    await replay.logsFetched();

    var startbutton = ButtonElement()
      ..text = 'Enter Replay'
      ..id = 'StartReplayButton'
      ..onClick.listen((e) {
        var elem1 = querySelector('#replayType') as SelectElement;

        //var chosenOption; //= elem1.selectedOptions[elem1.selectedIndex];
        replay.setReplayType(true);
        replay.setAnalysisOption(options);
        replay.startReplay(e);
      });
    startbutton.disabled = false;

    var cardDiv = DivElement();
    cardDiv.append(startbutton);

    querySelector('.container-body').append(cardDiv);
  }

  void displayErrorMessage(String err) {
    var label = Element.p()
      ..text = err
      ..id = 'path-label';
    var cardDiv = DivElement();
    cardDiv.append(label);

    querySelector('.container-body').append(cardDiv);
  }

  ///Creates the DivElement eyeblob used to represent eyetracker-data on screen during replay-mode
  static void displayEyetrackerBlob() {
    var eyeblob = ImageElement();

    eyeblob.className = 'eyeBlob';
    eyeblob.src = './circle.png';
    eyeblob.style.width = '100px';
    eyeblob.style.height = '100px';
    eyeblob.style.top = '100px';
    eyeblob.style.left = '100px';
    eyeblob.style.position = 'absolute';
    eyeblob.style.zIndex = '4';

    document.querySelector('body').append(eyeblob);
  }

  ///Creates the DivElement cursor used to visualize cursormovements.
  static void displayCustomCursor() {
    var cursor = ImageElement()
      ..src = './custom-cursor-arrow.png'
      ..style.width = '50px'
      ..style.height = '50px'
      ..style.top = '10px'
      ..style.left = '10px'
      ..style.position = 'absolute'
      ..style.zIndex = '10'
      ..id = 'customCursor';

    document.querySelector('body').append(cursor);
  }

  static void removeCustomCursor() {
    document.querySelector('#customCursor').remove();
  }

  static void removeEyeBlob() {
    var eyeBlob = querySelector('.eyeBlob');
    eyeBlob.remove();
  }

  ///Method for tracking visibility, need to fix
  void toggleEyetrackerVisibility() {
    try {
      var eyeBlob = querySelector('.eyeBlob');
      if (eyeBlob.style.display == 'flex') {
        eyeBlob.style.display = 'none !important';
        eyeBlob.hidden = true;
      } else {
        eyeBlob.hidden = false;
        eyeBlob.style.display = 'flex';
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
