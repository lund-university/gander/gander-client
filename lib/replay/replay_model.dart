import 'dart:async';
import 'dart:html';
import 'dart:convert';

import '../gander_login.dart';
import '../file/file_model.dart';
import 'replay_websocket.dart';
import 'start_replay_view.dart';
import '../service_models/gander_service_model.dart';

class ReplayModel {
  GanderLogin login;

  static const int _PORT = 8001;
  static const String _HOST = 'http://localhost';
  static const String _SETPATH = '/path';
  static const String _GETPATHS = '/getpaths';
  static const String _FIXATE_FILE = '/fixatedfile';

  static const String _SUCCESS_MESSAGE = 'SUCCESS';
  static String storageLogPath = '';

  var replayType;
  var scrollYValue = 0;
  var log_list = [];
  var workList = [];
  var reqResList = [];
  var workListInd = 0;
  var inReplay = false;
  var eT = false;
  var customCursorVisable = false;
  var textIsHighlighted = false;
  var prev_id;
  var ogColour;
  var paused = false;
  var offsetY = 0;
  var semantic = false;
  var highlight = false;
  var fixation = false;

  var useDefMap = {};
  var beenHere = 0;
  GanderReplayWebsocket socket;
  FileCollectionModel model;

  ReplayModel(this.login, this.model);

  void setUseFixation() {
    fixation = true;
  }

  ///posts the desired path to replay service for the log of the session we want to replay
  ///atm we don't support fixation in analysis replay, but might be implemented later

  Future<bool> postPathforLogs(String path) async {
    if (fixation) {
      try {
        var result = await GanderServiceModel.sendRequestwithRespons(
            '$_HOST:$_PORT$_FIXATE_FILE/$path',
            method: 'GET',
            sendData: '',
            requestHeaders: {
              'Content-Type': 'application/json; charset=UTF-8'
            });
        // print('RES: ' + result);
        if (result.contains(_SUCCESS_MESSAGE)) {
          print('Start websocket to receive parsed fixation data to replay');
          startWebsocket();
        }
        return result.contains(_SUCCESS_MESSAGE);
      } catch (e) {
        print("Couldn't open $_HOST:$_PORT$_FIXATE_FILE: " + e.toString());
        rethrow;
      }
    } else {
      try {
        var result = await GanderServiceModel.sendRequestwithRespons(
            '$_HOST:$_PORT$_SETPATH',
            method: 'POST',
            sendData: '"$storageLogPath$path"',
            requestHeaders: {
              'Content-Type': 'application/json; charset=UTF-8'
            });
        if (result.contains(_SUCCESS_MESSAGE)) {
          print('Start websocket to receive data to replay');
          startWebsocket();
        }
        return result.contains(_SUCCESS_MESSAGE);
      } catch (e) {
        print("Couldn't open $_HOST:$_PORT$_SETPATH: " + e.toString());
        rethrow;
      }
    }
  }

  Future<bool> logsFetched() async {
    print('Fetching logs');
    return await socket.fetchLogs();
  }

  Future<List<String>> getPaths() async {
    var result = await GanderServiceModel.getRequest('$_HOST:$_PORT$_GETPATHS');
    var res = jsonDecode(result);
    var list = [''];

    //Get the absolute path to the log folder on the local machine.
    storageLogPath = res['path'];
    if (!storageLogPath.endsWith('\\') || !storageLogPath.endsWith('/')) {
      if (storageLogPath.contains('/')) {
        storageLogPath = '$storageLogPath/';
      } else if (storageLogPath.contains('\\')) {
        storageLogPath = '$storageLogPath\\';
      }
    }

    var fileArr = res['files'];

    for (var i = 0; i < fileArr.length; i++) {
      list.add(fileArr[i]);
    }
    list.remove('');

    return list;
  }

  ///Changes the necessary attributes and styles to exit replay, the locally saved lists are emptied.
  void exitReplay() {
    GanderServiceModel.replay = false;
    inReplay = false;
    ReplayView.removeCustomCursor();
    if (eT) {
      ReplayView.removeEyeBlob();
    }
    socket.close();
    workList = [];
    reqResList = [];
    document.body.style.opacity = '1.0';
    document.body.style.backgroundColor = 'White';
    document.getElementById('topLeft').hidden = false;
    document.getElementById('exitReplay').hidden = true;
    workListInd = 0;
    customCursorVisable = false;
    eT = false;
    fixation = false;
    highlight = false;
    semantic = false;
  }

  void startWebsocket() {
    socket = GanderReplayWebsocket();
    socket.init(fixation);
  }

  ///Changes attributes to indicate to necessary services that client is in replay.
  ///Gets the loglist from websocket and sends the parsed mockMap to service model.
  ///Initiates the parsing and then the execution of the worklist.
  void startReplay(MouseEvent e) {
    GanderServiceModel.replay = true;
    inReplay = true;
    offsetY = window.outerHeight - window.innerHeight;
    document.body.style.backgroundColor = 'Lavender';
    document.body.style.opacity = '0.7';

    document.getElementById('topLeft').hidden = true;
    document.getElementById('exitReplay').hidden = false;
    log_list = socket.getLogs();
    sortLogList(log_list);
    GanderServiceModel.getMockService(parseReqResList());
    if (workList.isNotEmpty && workList[0].runtimeType == String) {
      parseworklist(workList);
      // print(workList.length);
    }

    login.generateLogin();
    document.onKeyPress.listen((event) {
      pause(event);
    });
    executeWorkList(0);
  }

  ///Iterates through the worklist async by updating the [WLindex] and sends the items to their respective methods to execute.
  ///The delay is calculated from the difference between the timestamps of the next element and current, can be tuned by multiplication.
  void executeWorkList(int delay) async {
    await Future.delayed(Duration(microseconds: delay), () {
      if (workList[workListInd].runtimeType == EyeTrackerLog) {
        if (!eT) {
          ReplayView.displayEyetrackerBlob();
          eT = !eT;
        }

        if ((workList[workListInd].element == 'span' ||
                workList[workListInd].element == 'pre') &&
            !replayType) {
          if (highlight) {
            showElement(workList[workListInd].el_id);
          }
        }

        moveEyetrackerBlob(
            workList[workListInd].x.round(), workList[workListInd].y.round());
        // print(workList[workListInd]);
      } else {
        if (workList[workListInd].action == 'scroll') {
          triggerScroll(workList[workListInd].data);
        } else if (workList[workListInd].action == 'mousemove') {
          if (!customCursorVisable) {
            ReplayView.displayCustomCursor();
            customCursorVisable = !customCursorVisable;
          }
          moveCustomCursor(
              workList[workListInd].data['x'], workList[workListInd].data['y']);
        } else {
          var elem = document.querySelector('#${workList[workListInd].id}');
          if (workList[workListInd].action == 'click' &&
              workList[workListInd].element == 'button') {
            if (elem.style != null) {
              highlightElement(elem, elem.style.background);
            } else {
              highlightElement(elem, '');
            }
          }
          triggerEvent(
              workList[workListInd].action, elem, workList[workListInd].data);
        }
      }
      workListInd++;
      if ((workListInd < workList.length - 1) && !paused && inReplay) {
        executeWorkList(((int.parse(workList[workListInd].ts) -
                    int.parse(workList[workListInd - 1].ts)) *
                1.5)
            .round());
      }
    });
  }

  ///If eyetracker is looking at code in changeview we check if it exists in [useDefMap]
  ///At the moment only methods and variable declaration are detected.
  void showUseDef(String id, String el) {
    try {
      var line;
      var idList = id.split('-');
      if (el == 'span') {
        line = int.parse(idList[2].substring(1, idList[2].indexOf(':')));
      } else {
        line = int.parse(idList[2].substring(1));
      }
      if (useDefMap.isEmpty) {
        //needs to be changed if we enter another PR view
        useDefMap = model.useDefMap;
      }
      var lookingAt;
      if (useDefMap.containsKey('${idList[0]}-${idList[1]}')) {
        for (var def in useDefMap['${idList[0]}-${idList[1]}']) {
          if (def.isVar) {
            if (line == def.location.startLine) {
              lookingAt = def;
              break;
            }
          } else {
            if (def.location.startLine <= line &&
                def.location.endLine >= line) {
              lookingAt = def;
              break;
            }
          }
        }
      }
      if (lookingAt != null) {
        if (lookingAt.isVar) {
          showElement(id,
              -1); //add something about displaying all the lines it's used in.
          for (var use in lookingAt.uses) {
            //hard to see if this works since we might not have support for var i compiler-service?
            var i = use['start'].split(':')[0];
            showElement('${idList[0]}-${idList[1]}-l$i', -1);
          }
        } else {
          //indicates the surrounding method the eyeBlob is looking at.
          var i = lookingAt.location.startLine;
          while (i < lookingAt.location.endLine) {
            showElement('${idList[0]}-${idList[1]}-l$i',
                i - lookingAt.location.startLine + 1);
            i++;
          }
          showElement('${idList[0]}-${idList[1]}-l$i', -2);
        }
      }
    } catch (e) {
      print(id + ' ' + el + ' ' + e.toString());
      rethrow;
    }
  }

  ///Scrolls the window according to the logged scrollData, it's recorded as negative usally, but we want it to be positive
  void triggerScroll(String data) {
    var scrollData = int.parse(data);
    var scrollValue = scrollData < 0
        ? -scrollData
        : scrollData; //Apparently abs() isn't available?
    window.scroll({'left': 0, 'top': scrollValue, 'behavior': 'smooth'});
    scrollYValue =
        scrollValue; //Update value of scrollYValue to make eyeBlob follow scroll.
  }

  ///Triggers the event [action] on element [elem],
  ///[data] is meant to be discarded for all events that are not keyEvents on TextInputElements or scrollEvents
  void triggerEvent(String action, Element elem, [String data = '']) {
    try {
      if (action == 'input') {
        //Only for textInputElement
        var inputElem = elem as TextInputElement;

        inputElem.value = data;

        var event = Event(action);
        elem.dispatchEvent(event);
      } else if (action == 'change' && elem is SelectElement) {
        elem.value = data;

        var event = Event(action);
        elem.dispatchEvent(event);
      } else if (action == 'click') {
        var event = MouseEvent(action);
        elem.dispatchEvent(event);
      } else if (action == 'mouseup') {
      } else if (action == 'mouseenter') {
        var event = MouseEvent(action);
        elem.dispatchEvent(event);
      } else if (action == 'keypress') {
        var event = CustomEvent('customKeypress', detail: data);
        elem.dispatchEvent(event);
      } else {
        var event = Event(action);
        elem.dispatchEvent(event);
      }
    } catch (exception) {
      print('$action\n$elem\n$data' + exception.toString());
      rethrow;
    }
  }

  // highlights the element the blob is on for emphasis
  void showElement(String id, [int lines = 0]) {
    if (lines == 0) {
      try {
        if (prev_id != id) {
          var elem = querySelector('#${Css.escape(id)}');
          if (elem.style != null) {
            ogColour = elem.style.backgroundColor;
            elem.style.backgroundColor = 'MediumSlateBlue';
          } else {
            ogColour = 'White';
            elem.style.backgroundColor = 'MediumSlateBlue';
            print('style is null\n$id');
          }
          hideElement(elem, ogColour);
          prev_id = id;
        }
      } catch (e) {
        print(id + '\n' + ogColour.toString() + '\n' + e.toString());
      }
    } else {
      try {
        var elem = querySelector('#$id');
        if (lines == -1) {
          elem.style.borderStyle = 'dotted';
        } else if (lines == 1) {
          elem.style.borderLeftStyle = 'dotted';
          elem.style.borderRightStyle = 'dotted';
          elem.style.borderTopStyle = 'dotted';
        } else if (lines == -2) {
          elem.style.borderLeftStyle = 'dotted';
          elem.style.borderRightStyle = 'dotted';
          elem.style.borderBottomStyle = 'dotted';
        } else {
          elem.style.borderLeftStyle = 'dotted';
          elem.style.borderRightStyle = 'dotted';
        }
      } catch (e) {
        print(id + '\n' + ogColour.toString() + 'here');
      }
    }
  }

  void hideElement(Element el, String colour) async {
    await Future.delayed(Duration(milliseconds: 100), () {
      try {
        el.style.backgroundColor = colour;
      } catch (e) {
        print('${el.id}\n$colour\n problem hiding');
      }
    });
  }

  ///Sorts the loglist from the websocket into reqresList, which will be parsed and sent to Gander service model, and
  ///worklist which is parsed and kept in replay model.
  void sortLogList(List l) {
    for (String s in l) {
      if (s.contains('"TimeRes"')) {
        reqResList.add(s);
      } else {
        workList.add(s);
      }
    }
  }

  ///Used to move the Eyetracking-blob element [eyeblob] by [x] and [y] coordinates
  void moveEyetrackerBlob(int x, int y) {
    try {
      if (x >= 0) {
        var eyeblob = querySelector('.eyeBlob');
        eyeblob.hidden = false;
        eyeblob.style.left = '${x - 50}px';
        eyeblob.style.top = '${(y - 50) + scrollYValue - offsetY}px';
      } else {
        var eyeblob = querySelector('.eyeBlob');
        eyeblob.hidden = true;
      }
    } catch (e) {
      print(e.toString());
      rethrow;
    }
  }

  ///Used to move the custom cursor element [customCursor] by [x] and [y] coordinates
  void moveCustomCursor(num posX, num posY) {
    try {
      var x = (posX * window.outerWidth).round();
      var y = (posY * window.outerHeight).round();
      var cursor = querySelector('#customCursor');
      cursor.style.left = '${x}px';
      cursor.style.top = '${y}px';
    } catch (e) {
      print(e.toString());
      rethrow;
    }
  }

// Parses the eyetracking log files into a list of EyeTrackerLog objects, the coords are
// also scaled to the current window size. We assume the list is sorted by replay service before
// sending through websocket.
  void parseworklist(List l) {
    var list = [];

    for (var log in l) {
      print(log);
      var entry = jsonDecode(log);

      if (entry.toString().contains("action")) {
        var id = entry['element-id'].toString().replaceAll(':', '\\:');
        list.add(InteractionLog(entry['msSinceEpoch'], entry['action'],
            entry['data'], entry['element'], id));
      } else {
        if (entry['x'] != null) {
          var tempX = double.parse(entry['x']);
          var tempY = double.parse(entry['y']);
          list.add(EyeTrackerLog(tempX, tempY, entry['msSinceEpoch'],
              entry['element-id'], entry['element']));
        } else {
          list.add(EyeTrackerLog(-1, -1, entry['msSinceEpoch'], '', ''));
        }
      }
    }

    workList = list;
  }

  ///Parses the list of request Response pairs into a map which is used by Gander service model
  ///to mock out back end.
  Map<String, List<ReqResLog>> parseReqResList() {
    var map = <String, List<ReqResLog>>{};
    for (var i in reqResList) {
      i.replaceAll('\n', '\\n');
      i.replaceAll('\r', '\\r');
    }
    for (var log in reqResList) {
      var reqresData = jsonDecode(log);
      //If the map doesn't have an entry with the url key, it's added as a new entry.
      if (!map.containsKey(reqresData['Request'])) {
        map.addAll({
          reqresData['Request']: [
            ReqResLog(
                reqresData['msSinceEpoch']['TimeReq'],
                reqresData['msSinceEpoch']['TimeRes'],
                reqresData['Response'],
                false)
          ]
        });
      }
      //Else, the list of response objects is updated and a new reqreslog object is added to the list
      else {
        map.update(
            reqresData['Request'],
            (v) =>
                v +
                [
                  ReqResLog(
                      reqresData['msSinceEpoch']['TimeReq'],
                      reqresData['msSinceEpoch']['TimeRes'],
                      reqresData['Response'],
                      false)
                ]);
      }
    }
    for (var entry in map.entries) {
      //if the value, list<ReqReslog>, has more than one entry, they are sorted by the timestamps
      if (entry.value.length > 1) {
        // to have states in check.
        entry.value.sort((a, b) => int.parse(a.tReq) - int.parse(b.tReq));
      }
    }
    return map;
  }

  void highlightElement(Element elem, String ogColour) {
    elem.style.backgroundColor = 'rgb(252,212,233)';
    removeHighlights(elem, ogColour);
  }

  ///Used to remove the highlighting of pressed element during replay session
  void removeHighlights(Element elem, String originalcolor) async {
    try {
      await Future.delayed(const Duration(seconds: 1),
          () => elem.style.backgroundColor = originalcolor);
    } catch (e) {
      print(e.toString());
    }
  }

  ///Sets the type of replay
  ///if [type]==[true] it's participant replay
  ///else it's analysis replay
  void setReplayType(bool type) {
    replayType = type;
  }

  void setAnalysisOption(List<bool> l) {
    semantic = l[0];
    highlight = l[1];
    fixation = l[2];
  }

  void pause(KeyboardEvent e) {
    if (e.keyCode == KeyCode.SPACE && inReplay) {
      e.preventDefault();
      if (paused) {
        executeWorkList(0);
        paused = !paused;
      } else {
        paused = !paused;
      }
    }
  }
}

class EyeTrackerLog {
  final double x;
  final double y;
  final String ts;
  final String el_id;
  final String element;
  EyeTrackerLog(this.x, this.y, this.ts, [this.el_id = '', this.element = '']);
}

class InteractionLog {
  final String ts;
  final String action;
  final dynamic data;
  final String element;
  final String id;
  const InteractionLog(this.ts, this.action, this.data, this.element, this.id);
}

class ReqResLog {
  final String tReq;
  final String tRes;
  final dynamic response;
  bool used;

  ReqResLog(this.tReq, this.tRes, this.response, this.used);
}
