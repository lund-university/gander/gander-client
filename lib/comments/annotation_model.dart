import 'dart:convert';

import '../service_models/gander_service_model.dart';
import 'package:collection/collection.dart';

/// Used for retrieving and posting comments to the Annotation microservice.
class AnnotationModel {
  static const int _PORT = 8001;
  static const String _HOST = 'http://localhost';
  static const String _ANNOTATIONPATH = '/annotation';
  static const String _COMMENTPATH = '/comments';
  static const String STATUS_OK = 'OK';
  //Stores all of the pull requests and their collection of comments.
  List<CommentCollection> commentCollections = <CommentCollection>[];
  

  AnnotationModel();

  /// Retrieves all of the comments currently stored for the pull request [number].
  Future<void> getComments(String mode, String number) async {
    try {
      var result = await GanderServiceModel.sendRequestwithRespons(
          '$_HOST:$_PORT$_ANNOTATIONPATH$_COMMENTPATH/$mode/$number',
          method: 'GET',
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
      _processComments(result);
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_ANNOTATIONPATH$_COMMENTPATH/$mode/$number");
      rethrow;
    }
  }

  /// Takes the response from the server [req] and adds the comments to the list of comments.
  void _processComments(String req) {
    var incomingData = jsonDecode(req);
    var incomingComments = incomingData['comments'] ?? [];
    CommentCollection coll;
    // If it exists in our current commentCollections.

    coll = commentCollections.firstWhere(
        (element) => element.number == incomingData['number'],
        orElse: () => CommentCollection(incomingData['number']));

    for (var comment in incomingComments) {
      coll.addComment(comment);
    }
    if (!commentCollections.contains(coll)) {
      commentCollections.add(coll);
    }
  }

  /// Posts a new comment to the pull request [number], containing [data].
  Future<String> postComment(String number, dynamic data) async {
    try {
      var result = await GanderServiceModel.sendRequestwithRespons(
          '$_HOST:$_PORT$_ANNOTATIONPATH$_COMMENTPATH/$number',
          method: 'POST',
          sendData: data,
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
      return _processPostedComment(result, number, data);
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_ANNOTATIONPATH$_COMMENTPATH/$number");
      rethrow;
    }
  }

  /// Checks if the comment could be added in the server. If it could, then add it to our commentCollections as well.
  String _processPostedComment(String req, String number, dynamic data) {
    if (!req.contains('ERROR')) {
      var coll = commentCollections.firstWhere((c) => c.number == number,
          orElse: () => CommentCollection(number));

      commentCollections.add(coll);
      data = jsonDecode(data);
      coll.addComment(data);

      return STATUS_OK;
    } else {
      return '$req: Could not add comment to annotation service';
    }
  }

  void clearStorage() {
    commentCollections = [];
  }
}

/// A Class in charge of a pull request number and its comments.
class CommentCollection {
  String number;
  List<Comment> comments;

  CommentCollection(this.number) {
    comments = <Comment>[];
  }

  /// Adds a comment with content [comment] to its list of comments.
  void addComment(dynamic comment) {
    var id = comment['id'];
    // Checks if it is a new comment. If it is not, we should update an existing comment's content.

    var com = comments.firstWhere((element) => element.id == id,
        orElse: () => Comment(comment['body'], comment['file']));

    com.user ??= comment['user'];

    com.id ??= id.toString();

    com.number = int.parse(number);

    com.is_a_reply =
        comment['is_a_reply'] == null ? false : comment['is_a_reply'] == 'true';

    com.reply_to_id = comment['reply_to_id'].toString();

    //comment['line'] can be sent as both String and int. Maybe we could solve this in the backend.
    if (comment['line'].runtimeType == int) {
      com.line = comment['line'];
    } else {
      com.line = int.parse(comment['line']);
    }

    if (comments.map((e) => e.id).contains(id)) {
      comments[comments.indexOf(comments.firstWhere(
          (element) => element.id == id,
          orElse: () => null))] = com;
    } else {
      comments.add(com);
    }
  }
}

class Comment {
  String body;
  String filePath;
  String user;
  bool is_a_reply;
  DateTime createdAt;
  DateTime updatedAt;
  int number;
  String id;
  String reply_to_id;
  int line;

  Comment(this.body, this.filePath, [this.line]);

  /// Method for getting a json representation of the comment. Used for storing them on the service.
  String toJson() {
    line ??= 0;
    reply_to_id ??= '0';
    var jsonstring = jsonEncode({
      'number': '$number',
      'body': '$body',
      'file': '$filePath',
      'line': '$line',
      'updated_at': '$updatedAt',
      'created_at': '$createdAt',
      'id': '$id',
      'is_a_reply': '$is_a_reply',
      'user': '$user',
      'reply_to_id': '$reply_to_id'
    });
    return jsonstring;
  }
}
