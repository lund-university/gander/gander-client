import 'dart:html';
import 'dart:math';
import 'dart:convert';
import 'package:markdown/markdown.dart' show markdownToHtml;
import '../aoi/ast_converter.dart';
import '../file/compare/compare_model.dart';
import 'annotation_model.dart';
import '../utils/user_info.dart';

class CommentModel {
  CommentCollection commentColl;
  Comparison comp;
  String name;

  Map<String, dynamic> config;

  ///The file name without characters that are not allowed in a html id or html class.
  String sanitizedName;
  DivElement fileObject;
  ASTFile astFile;
  Function interactionCallback;
  Function postCommentCallback;

  CommentModel(this.comp, this.commentColl, this.interactionCallback,
      this.postCommentCallback, this.sanitizedName) {
    getConfigInfo();
  }

  /// Returns a version of [name] without any characters that are not allowed in a html id or html class.
  String sanitizeFileName(String name) {
    return name
        .replaceAll('.', '')
        .replaceAll('/', '')
        .replaceAll('-', '')
        .replaceAll('_', '');
  }

  /// Adds all of the comments for the file in the content area [fileObject].
  void addCommentForFile(DivElement fileObject) {
    //getConfigInfo();

    var comments = commentColl.comments.where((element) =>
        sanitizeFileName(element.filePath) ==
        sanitizeFileName(comp.filename)); //fileNames does not always match

    var lines = comments.map((e) => e.line).toSet();
    for (var commentLine in lines) {
      var line = fileObject
          .querySelector('#$sanitizedName-target-l${(commentLine).toString()}');
      var commentsInLine =
          comments.where((element) => element.line == commentLine);
      var parent = line?.parent;
      var next = line?.nextElementSibling;
      var includeCancelButton = false;

      parent?.insertBefore(
          (generateCommentSection(commentLine, commentColl.number,
              comp.filename, includeCancelButton, commentsInLine)),
          next);
    }
  }

  Future<void> getConfigInfo() async {
    var configContent = await HttpRequest.getString('config.json');
    Map<String, dynamic> conf = jsonDecode(configContent);
    config = conf;
  }

  ///Generates the comments found in commentColl.comments for the file.
  void generateCommentCollectionComments(DivElement fileObjectDiv) {
    for (var comment in commentColl.comments) {
      //if comment is on the same file
      if (sanitizeFileName(comment.filePath) ==
          sanitizeFileName(comp.filename)) {
        //if comment is not a reply, add it as a new comment thread.
        if (!comment.is_a_reply) {
          var commentLine = comment.line;
          var codeLinePre = fileObjectDiv.querySelector(
              '#${sanitizeFileName(comp.filename)}-target-l$commentLine');
          //If comment is assigned a line we want to show it on this line
          //If commentline = -1 it is a comment on the file or PR as a whole
          if (commentLine != -1) {
            var codeLine;
            if (codeLinePre.children.length > 1) {
              codeLine = codeLinePre.children[1].className == 'lineCode'
                  ? codeLinePre.children[1]
                  : codeLinePre;
            } else {
              codeLine = codeLinePre;
            }
            codeLine.style.borderBottomStyle = 'solid';
            codeLine.style.borderLeftStyle = 'solid';
            codeLine.style.borderRightStyle = 'solid';
            var commentPostLocation;
            var click = MouseEvent('click');
            if (codeLinePre.className.contains('button-')) {
              var ind = codeLine.className.lastIndexOf('button-');
              commentPostLocation = codeLine.className.substring(ind + 7);
            }

            fileObjectDiv
                .querySelector(
                    '#show-more-btn-${sanitizeFileName(comp.filename)}-$commentPostLocation')
                ?.dispatchEvent(click);
            //Old version of triggering show more buttons, keep until new version is tested.
            // if (codeLine.className.contains('button-top')) {
            //   var click = MouseEvent('click');
            //   fileObjectDiv
            //       .querySelector(
            //           '#show-more-btn-${sanitizeFileName(comp.filename)}-top')
            //       .dispatchEvent(click);
            // } else if (codeLine.className.contains('button-bottom')) {
            //   var click = MouseEvent('click');
            //   fileObjectDiv
            //       .querySelector(
            //           '#show-more-btn-${sanitizeFileName(comp.filename)}-bottom')
            //       .dispatchEvent(click);
            // }
          }
        }
      } //#show-more-btn-srcBirdjava-button-bottom-18
    }
  }

  /// Creates the div with the found comments [comments], if included. Otherwise, it will create a new comment dialogue-div for a user to insert new comments on the line [lineNo].
  DivElement generateCommentSection(int lineNo, String pullReqNumber,
      String fileName, bool includeCancelButton,
      [Iterable<Comment> comments]) {
    var name;
    if (fileName == 'PR') {
      name = fileName;
    } else {
      name = sanitizedName;
    }

    var commentDiv = DivElement()..id = '$name-comment-div-$lineNo';

    if (config != null && config['mode'] == 'github') {
      commentDiv.className = 'comment-div';
    } else {
      commentDiv.className = 'gerrit-comment-div';
    }
    if (querySelector('#$sanitizedName-comment-div-$lineNo-posted-comments') !=
        null) {
      return querySelector(
          '#$sanitizedName-comment-div-$lineNo-posted-comments');
    }

    if (comments != null) {
      commentDiv.append(generatePostedCommentsArea(lineNo, comments));
    } else {
      commentDiv.append(generatePostedCommentsArea(lineNo));
    }
    commentDiv.append(generateAddContentArea(
        lineNo, pullReqNumber, fileName, includeCancelButton));
    //  commentDiv.removeEventListener('onClick', ((event) {
    //    Element target = event.target;
    //    var line = target.id.split('-l').last;
    //    astFile.src.findASTPosition(line);
    //    var testdata = {
    //      'x': target.getBoundingClientRect().top / window.outerWidth,
    //      'y': target.getBoundingClientRect().left / window.outerWidth,
    //      'ts': 3463391524625
    //    };
    //    testAST(jsonEncode(testdata));
    //  }));
    return commentDiv;
  }

  DivElement generatePostedCommentsArea(int lineNo,
      [Iterable<Comment> comments]) {
    var postedCommentsSection = DivElement()
      ..id = '$sanitizedName-comment-div-$lineNo-posted-comments'
      ..className = 'comment-div-posted-section';

    if (comments != null) {
      var nbrOfComments = comments.length;
      var minimizeIcon = ImageElement()
        ..src = './collapse-icon.png'
        ..className = 'change-comment-size-icon'
        ..id = '$sanitizedName-change-comment-size-icon-$lineNo';

      var minimizeButton = ButtonElement()
        ..append(minimizeIcon)
        ..id = '$sanitizedName-minimize-button-$lineNo'
        ..className = 'change-comment-size-button minimize'
        ..onClick.listen((event) {
          interactionCallback(event);
          var parentDiv = postedCommentsSection.parent;
          parentDiv.style.display = 'none';
          var grandparent = parentDiv.parent;
          grandparent.insertBefore(
              getMaximizeElement(parentDiv, lineNo, nbrOfComments), parentDiv);
        });

      postedCommentsSection.append(minimizeButton);
      for (var comment in comments) {
        var userinfo = DivElement()
          ..className = 'posted-comment-user-info'
          ..id = '$sanitizedName-comment-userinfo-${comment.id}';
        var name = comment.user;

        var userIcon = ImageElement()
          ..src = getImagePathForUser(name)
          ..width = 50
          ..height = 50
          ..style.margin = '5px'
          ..className = 'profile-picture'
          ..id = '$sanitizedName-comment-usericon-${comment.id}';
        var userName = DivElement()
          ..text = '${comment.user}'
          ..id = '$sanitizedName-comment-username-${comment.id}';
        userinfo
          ..append(userIcon)
          ..append(userName);
        var image = ImageElement();
        var containsImage = false;

        if (markdownToHtml(comment.body).contains('<img src="')) {
          //checks if the comment has image ref
          var link =
              comment.body.split('(').firstWhere((s) => s.contains('https:'));
          var ref = link.split(')')[0];
          image.src =
              ref; //since dart removes the image with markdown to html we need to manually do this :/
          containsImage = true;
        }
        var commentContentDiv = DivElement()
          ..className = 'posted-comment-content'
          ..id = '$sanitizedName-comment-content-div-${comment.id}'
          ..appendHtml(markdownToHtml(comment.body));
        if (containsImage) {
          commentContentDiv.children[0].children
              .removeWhere((element) => element.tagName == 'IMG');
          commentContentDiv.append(image);
        }
        var commentDiv = DivElement()
          ..className = 'posted-comment-div'
          ..id = '$sanitizedName-comment-div-${comment.id}'
          ..append(userinfo)
          ..append(commentContentDiv);
        postedCommentsSection.append(commentDiv);
      }
    }

    return postedCommentsSection;
  }

  DivElement generateAddContentArea(int lineNo, String pullReqNumber,
      String fileName, bool includeCancelButton) {
    var addCommentsSection = DivElement()
      ..id = '$sanitizedName-comment-div-$lineNo-add-comments'
      ..className = 'comment-div-add-section';

    var writeCommentsSection = DivElement()
      ..id = '$sanitizedName-comment-div-$lineNo-write-comments'
      ..className = 'comment-div-write-section';

    var textArea = TextInputElement()
      ..id = '$sanitizedName-comment-div-$lineNo-text-add-comments'
      ..className = 'text-area-write-comment';
    textArea.onInput.listen((event) {
      interactionCallback(event, textArea.value);
    });

    var btnArea = DivElement()
      ..id = '$sanitizedName-comment-div-$lineNo-btn-div'
      ..className = 'comment-div-btn-section';
    var submitBtn = ButtonElement()
      ..id = '$sanitizedName-comment-div-$lineNo-submit-button'
      ..className = 'button submit-comment'
      ..text = 'Submit'
      ..onClick.listen((e) {
        sendPostComment(e, textArea, pullReqNumber, lineNo, fileName);
        querySelector('#$sanitizedName-comment-div-$lineNo-cancel-button')
            ?.remove();
      });

    var currentPre = querySelector('#$sanitizedName-target-l$lineNo');
    var profileImage = querySelector('.profile-picture');
    var src = UserInfo.imagePath;
    if (profileImage != null) {
      src = (profileImage as ImageElement).src;
    }

    var userProfile = ImageElement()
      ..src = src
      ..height = 50
      ..width = 50
      ..className = 'profile-picture'
      ..id = '$sanitizedName-${UserInfo.username}-comment-$lineNo';
    writeCommentsSection.append(userProfile);
    writeCommentsSection.append(textArea);
    addCommentsSection.append(writeCommentsSection);
    btnArea.append(submitBtn);

    if (includeCancelButton) {
      var cancelBtn = ButtonElement()
        ..id = '$sanitizedName-comment-div-$lineNo-cancel-button'
        ..className = 'cancel-button cancel-comment'
        ..text = 'Cancel'
        ..onClick.listen((event) {
          interactionCallback(event);
          textArea.value = '';
          toggleCommentSection(event, currentPre);
        });
      btnArea.append(cancelBtn);
    }

    addCommentsSection.append(btnArea);

    return addCommentsSection;
  }

  ///Updates the comment area in the Overview view. Appends a DivElement containing [newComment]
  void updateOverviewCommentContent(Comment newComment) {
    var commentDiv = DivElement();
    var row = DivElement()..className = 'row';
    var fileCol = DivElement()
      ..className = 'col'
      ..text = 'In file ${newComment.filePath}'
      ..id = 'comment-container-file-column';
    var lineCol = DivElement()
      ..className = 'col'
      ..text = 'Line ${newComment.line}'
      ..id = 'comment-container-line-column';
    var contentCol = AnchorElement()
      ..className = 'col'
      ..id = 'comment-container-content-column'
      ..href = '#$sanitizedName-comment-div-${newComment.line}-posted-comments';

    contentCol.setInnerHtml(markdownToHtml(newComment.body));

    row.append(fileCol);
    row.append(lineCol);
    row.append(contentCol);

    commentDiv.append(row);
    //querySelector('#comment-section-div-card').append(commentDiv);
  }

  /// Sends the posted comment to Annotation Service, to be stored.
  void sendPostComment(Event e, TextInputElement commentBox,
      String pullReqNumber, int lineNo, String fileName) async {
    var comment = commentBox.value;

    if (comment.trim() == '') {
      return;
    }

    var newComment = Comment(comment, fileName, lineNo)
      ..createdAt = DateTime.now()
      ..updatedAt = DateTime.now()
      ..user = UserInfo.username
      ..id = Random().nextInt(1000000000).toString();

    updateOverviewCommentContent(newComment);

    var comments =
        commentColl.comments.where((element) => element.filePath == fileName);
    var commentsInLine = comments.where((element) => element.line == lineNo);
    if (commentsInLine.isNotEmpty) {
      newComment
        ..is_a_reply = true
        ..reply_to_id = commentsInLine.last.id;
    } else {
      newComment.is_a_reply = false;
    }

    //Send interaction and content of comment field
    interactionCallback(e, newComment.body);

    var res = await postCommentCallback(pullReqNumber, newComment.toJson());
    if (res == 'OK') {
      commentBox.value = '';
      createNewShownComment(newComment, lineNo);
    }
  }

  /// Adds the newly posted comment to the comment dialogue, that shows that the comment has been added.
  void createNewShownComment(Comment comment, int lineNo) {
    var postedCommentsSection =
        querySelector('#$sanitizedName-comment-div-$lineNo-posted-comments');

    var profileImage = querySelector('.profile-picture');
    var src = UserInfo.imagePath;
    if (profileImage != null) {
      src = (profileImage as ImageElement).src;
    }

    var userinfo = DivElement()
      ..className = 'posted-comment-user-info'
      ..id =
          '$sanitizedName-${UserInfo.username}-comment-posted-userinfo-$lineNo';
    var userIcon = ImageElement()
      ..src = src
      ..width = 50
      ..height = 50
      ..className = 'profile-picture'
      ..id =
          '$sanitizedName-${UserInfo.username}-comment-posted-usericon-$lineNo'
      ..style.margin = '5px';
    var userName = DivElement()..text = '${comment.user}';
    userinfo
      ..append(userIcon)
      ..append(userName);
    var commentContentDiv = DivElement()
      ..className = 'posted-comment-content'
      ..id =
          '$sanitizedName-${UserInfo.username}-comment-posted-content-div-$lineNo'
      ..appendHtml(markdownToHtml(comment.body));
    var commentDiv = DivElement()
      ..className = 'posted-comment-div'
      ..id =
          '$sanitizedName-${UserInfo.username}-comment-posted-comment-div-$lineNo'
      ..append(userinfo)
      ..append(commentContentDiv);
    postedCommentsSection.append(commentDiv);
    if (lineNo != -1) {
      var codeLine = querySelector('#$sanitizedName-target-l$lineNo');
      codeLine.style.borderBottomStyle = 'solid';
      codeLine.style.borderLeftStyle = 'solid';
      codeLine.style.borderRightStyle = 'solid';
    }
  }

  /// Used to toggle the comment section when pressing the icon next to the line. It shows or hides the comment dialogue.
  void toggleCommentSection(Event event, Element target) {
    interactionCallback(event);
    var parent = target.parent;
    var fileName = parent.parent.parent.parent.id.split('-')[2];
    var santitized = sanitizedName;

    var commentLineID = target.id.split('-');
    if (commentLineID.contains('whitespace')) {
      return;
    }
    var id = int.parse(commentLineID[2].substring(1));

    //Checks if there exists a comment from before
    var el = querySelector('#$santitized-comment-div-$id');
    if (el == null) {
      var includeCancelButton = true;
      el = generateCommentSection(
          id, commentColl.number, fileName, includeCancelButton);
      parent.append(el);
      event.preventDefault();
      querySelector('#$fileName-comment-div-$id-text-add-comments').focus();
    } else {
      el = querySelector('#$santitized-comment-div-$id');

      if (el.style.display == 'none') {
        el.style.display = '';
        /**<input type="text" id="srcBirdjava-comment-div-35-text-add-comments" class="text-area-write-comment"> */
        event.preventDefault();
        querySelector('#$fileName-comment-div-$id-text-add-comments').focus();
      } else {
        el.style.display = 'none';
      }
    }
  }

  ///Returns the div containing the button that the user can press to maximize a comment section.
  DivElement getMaximizeElement(
      DivElement commentDiv, int lineNo, int nbrOfComments) {
    var maximizeDiv = DivElement()
      ..className = 'comment-div maximize'
      ..id = '$sanitizedName-comment-div-$lineNo-posted-comments';

    var maximizeIcon = ImageElement()
      ..src = './expand-icon.png'
      ..className = 'change-comment-size-icon'
      ..id = '$sanitizedName-change-comment-size-icon-$lineNo';

    var maximizeButton = ButtonElement()
      ..append(maximizeIcon)
      ..id = '$sanitizedName-maximize-button-$lineNo'
      ..className = 'change-comment-size-button'
      ..onClick.listen((event) {
        interactionCallback(event);
        maximizeDiv.style.display = 'none';
        commentDiv.style.display = 'block';
      });
    maximizeDiv.append(maximizeButton);
    var showCommentElement = Element.p()..className = 'show-all-comments';

    //if statement to determine if "comment" of "comments" should be used
    showCommentElement.text = (nbrOfComments == 1)
        ? 'Show $nbrOfComments comment'
        : 'Show $nbrOfComments comments';

    maximizeDiv.append(showCommentElement);
    return maximizeDiv;
  }

  /// Returns an image path for the username [name]. If the user already has an image,
  /// the path to that image is returned. Otherwise, the user is given an available image.
  String getImagePathForUser(String name) {
    var userMap = UserInfo.usernameToImagePath;
    if (userMap.containsKey(name)) {
      return userMap[name];
    }
    var imagePathList = UserInfo.imagePathList;
    if (imagePathList.isNotEmpty) {
      var imagePath = imagePathList.removeAt(0);
      userMap[name] = imagePath;
      UserInfo.imagePathList = imagePathList;
      return imagePath;
    }
    // If there is no available images, the dart logo is used as default.
    return './favicon.ico';
  }
}
