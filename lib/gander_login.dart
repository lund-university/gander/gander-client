import 'dart:html';
import 'dart:convert';

import 'gerrit_mode/gerrit_changes_view.dart';
import 'github_mode/github_pull_request_view.dart';
import 'utils/user_info.dart';

/// Front page of the Gander client.
class GanderLogin {
  String username = '';
  String pathToBirdImage;

  Function interactionCallback;
  Function visibleElementsCallback;
  PullRequestView PRView;
  ChangesView changesView;

  GanderLogin(this.interactionCallback, this.PRView, this.changesView);

  /// Adds the login page to the container body.
  void generateLogin() {
    querySelector('.container-body').innerHtml = '';
    querySelector('.container-navigation').innerHtml = '';
    displayForm();
  }

  /// Adds the login page to the container body.
  void displayForm() {
    var button = ButtonElement()
      ..text = 'Log in'
      ..id = 'loginButton';
    //..onClick.listen((e) => registerUserChoices(e, username, pathToBirdImage));
    button.addEventListener('click', (event) {
      registerUserChoices(event, username, pathToBirdImage);
    });
    button.disabled = true;
    var cardDiv = DivElement();

    var header = HeadingElement.h1()
      ..text = 'Log in'
      ..id = 'log-in-title';
    var label = Element.p()
      ..text = 'Name:'
      ..id = 'name-label';

    var nameBox = TextInputElement()..id = 'nameBox';
    nameBox.onInput.listen((event) {
      //Sends interactionevent for every entered character in the nameBox
      interactionCallback(event, nameBox.value.toString());
      username = nameBox.value;
      if (username.isNotEmpty && pathToBirdImage != null) {
        button.disabled = false;
      } else {
        button.disabled = true;
      }
    });

    cardDiv.append(header);
    cardDiv.append(label);
    cardDiv.append(nameBox);

    var imageLabel = Element.p()
      ..text = 'Choose a profile picture:'
      ..id = 'image-label';
    cardDiv.append(imageLabel);
    var form = FormElement();
    var i = 1;
    for (var birdPath in UserInfo.imagePathList.take(5)) {
      //Only display the first five images to prevent the login page to look to crowded
      var bird = LabelElement();
      var radioBird = RadioButtonInputElement()
        ..value = 'bird'
        ..name = 'profile'
        ..id = 'bird$i'
        ..onClick.listen((event) {
          //Sends interactionEvent where data is the birdPath
          interactionCallback(event, birdPath.toString());
          pathToBirdImage = birdPath;
          if (username.isNotEmpty && pathToBirdImage != null) {
            button.disabled = false;
          }
        });
      bird.append(radioBird);
      var birdImage = ImageElement()
        ..src = birdPath
        ..style.margin = '5px'
        ..className = 'profile-picture'
        ..width = 150
        ..height = 150;
      bird.append(birdImage);
      form.append(bird);
      i++;
    }
    cardDiv.append(form);

    cardDiv.append(button);

    querySelector('.container-body').append(cardDiv);
  }

  /// When the button is pressed, the information is used to set the username [name] and the chosen profile picture path [imagePath].
  void registerUserChoices(MouseEvent event, String name, String imagePath) async {
    //When button pressed => send interactionEvent for button containing full username.
    interactionCallback(event, name);

    window.localStorage['currentUser'] = name;

    querySelector('.container-body').innerHtml = '';
    var profileInformation = querySelector('#profile-information');
    if (!UserInfo.imagePathList.contains(imagePath)) {
      UserInfo.imagePathList.add(imagePath);
    }
    var birdImage = ImageElement()
      ..src = imagePath
      ..style.margin = '2px'
      ..className = 'profile-picture'
      ..width = 48
      ..height = 48;
    profileInformation.append(birdImage);

    profileInformation.append(Element.p()
      ..text = name
      ..id = 'username');
    querySelector('#logOutButton').style.position = 'right';
    querySelector('#logOutButton').hidden = false;
    PRView.setSrc(imagePath);
    UserInfo.username = name;
    UserInfo.imagePath = imagePath;
    UserInfo.usernameToImagePath[name] = imagePath;

    // Remove the chosen imagePath from the list of possible choices for the oter generated profiles that will make
    // comments in the commits.
    UserInfo.imagePathList.remove(imagePath);

    var configContent = await HttpRequest.getString('config.json?ts=${DateTime.now().millisecondsSinceEpoch}');
    Map<String, dynamic> config = jsonDecode(configContent);

    if (config['mode'] == 'github') {
      await PRView.generatePullRequestList();
    } else if (config['mode'] == 'gerrit') {
      await changesView.generateChanges();
    } else {
      displayWelcome(name, imagePath);
    }
  }

  /// Adds a welcome-page will be added to the container body.
  void displayWelcome(String username, String imagePath) {
    var cardDiv = DivElement()..className = 'welcome';
    var welcome = HeadingElement.h1()
      ..text = 'Welcome'
      ..id = 'welcome-text';
    cardDiv.append(welcome);

    var birdImage = ImageElement()
      ..src = imagePath
      ..style.margin = '5px'
      ..className = 'profile-picture'
      ..width = 150
      ..height = 150;
    cardDiv.append(birdImage);
    var name = HeadingElement.h1()..text = username;
    cardDiv.append(name);
    querySelector('.container-body').append(cardDiv);
  }
}
