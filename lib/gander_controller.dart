import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:gander/gerrit_mode/gerrit_changes_view.dart';
import 'package:gander/gerrit_mode/gerrit_model.dart';

import 'aoi/ast_converter.dart';
import 'gander_login.dart';
import 'gander_package.dart';
import 'github_mode/github_pull_request_view.dart';
import 'replay/replay_model.dart';
import 'service_models/gander_service_model.dart';
import 'utils/user_info.dart';
import 'file/compare/compare_model.dart';
import 'utils/coordinate_converter.dart';
import 'github_mode/git_model.dart';
import 'connection/gander_websocket.dart';
import 'comments/annotation_model.dart';
import 'replay/start_replay_view.dart';
import 'connection/in_loop_ws_client.dart';

/// Controller of the Gander client
class GanderController {
  Timer scrolling;
  Timer mouseMove;
  GanderWebsocket socket;
  InLoopWsClient inLoopSocket;
  PurView fileView;
  DiffView diffView;
  ChangelogView changelogView;
  PullRequestView pullRequestView;
  ChangesView changesView;
  FileCollection fileCollection;
  FileCollectionModel model;
  AnnotationModel annModel;
  DivElement textArea = querySelector('#code-line');
  GanderDiff diff;
  GanderCompare compare;
  DivElement contentDisplay;
  GanderConverter coordinateConverter;
  ASTConverter astConverter;
  bool isTracking;
  GitModel gitModel;
  GerritModel gerritModel;
  GanderLogin login;
  ReplayView replayView;
  ReplayModel replay;

  /// Initializes all of the components of the front end.
  GanderController() {
    socket = GanderWebsocket();
    inLoopSocket = InLoopWsClient();
    coordinateConverter = GanderConverter();

    gitModel = GitModel();
    gerritModel = GerritModel();
    annModel = AnnotationModel();
    astConverter = ASTConverter();
    diff = GanderDiff(model);

    compare = GanderCompare();
    pullRequestView = PullRequestView(
        gitModel, compare, annModel, socket.sendInteractionEvent, inLoopSocket);
    changesView = ChangesView(
        gerritModel, annModel, socket.sendInteractionEvent, inLoopSocket);
    login =
        GanderLogin(socket.sendInteractionEvent, pullRequestView, changesView);
    isTracking = false;
    fileView = PurView(model, socket.sendInteractionEvent);
    changelogView = ChangelogView(diff, socket.sendInteractionEvent);

    contentDisplay = querySelector('.container-body');
    replay = ReplayModel(login, model);
    replayView = ReplayView(replay);

    /* Commented out for pilot!

    These methods and models are not currently used or needed for the pilot.
    This includes the components called 'DiffView', 'ChangeLog' and 'ProgressView'. 
    For more details on them, please see respective class.

    updateFilesList();
    querySelector('#getProgress').onClick.listen(showProgressView);
    querySelector('#getDiffView').onClick.listen(showDiffView);
    querySelector('#getChangeLog').onClick.listen(showChangeLog);
    querySelector('#getProgress').onClick.listen(showProgressView);
    */

    setPlatformMode();

    querySelector('#toggleTracking').onChange.listen(toggleTracking);
    querySelector('#getPullRequestView').onClick.listen(showPullRequestView);
    querySelector('#getChangesView').onClick.listen(showChangesView);
    querySelector('#enterReplay').onClick.listen(showReplayView);
    querySelector('#exitReplay').onClick.listen(escapeReplayView);
    querySelector('#logOutButton').onClick.listen((event) {
      querySelector('#profile-information').innerHtml = '';
      UserInfo.restoreImagelist();
      login.generateLogin();
      querySelector('#logOutButton').hidden = true;
    });

    document.getElementById('exitReplay').hidden = true;
    login.generateLogin();

    // This listener is used to update the visible elements in the ViewPort using the
    // class CoordinateConverter. When the user is done scrolling, the new element's positions
    // are generated.
    document.window.addEventListener('scroll', (event) {
      if (scrolling == null || !scrolling.isActive)
        scrolling =
            GanderUtils.setTimeout((() => scrollingCallback(event)), 250);
    });

    ///Listener used to log highlighted text in the prototype. Used to recreate highlighting during replay
    //  document.querySelector('.container').addEventListener('mouseup', (event) {
    //    var hlSelection = window.getSelection();
    //    var highlighted = hlSelection.toString();
    //    if (highlighted.trim() != '') {
    //      //print(elem.parent.id);
    //      //socket.sendHighlightInteration(hlSelection, event, highlighted);
    //    }
    //  });

    ///Event listener used to log the movement of the mouse, to recreate during replay.
    document.window.addEventListener('mousemove', (event) {
      GanderUtils.clearTimeout(mouseMove);
      mouseMove = GanderUtils.setTimeout(() => mouseMoveCallback(event), 10);
    });

    document.body.addEventListener('focus', (Event event) {
      final target = event.target;

      if (target is TextAreaElement || target is TextInputElement) {
        UserInfo.isTyping = true; 
      }
    }, true);

    document.body.addEventListener('blur', (Event event) {
      final target = event.target;

      if (target is TextAreaElement || target is TextInputElement) {
        UserInfo.isTyping = false; 
      }
    }, true);
  }

  /// Sets only one of the view buttons to be visible, dictates which mode of the platform is usable
  void setPlatformMode() async {
    var configContent = await HttpRequest.getString('config.json?ts=${DateTime.now().millisecondsSinceEpoch}');
    Map<String, dynamic> config = jsonDecode(configContent);

    if (config['mode'] == 'github') {
      querySelector('#getPullRequestView').style.display = 'inline-block';
    } else if (config['mode'] == 'gerrit') {
      querySelector('#getChangesView').style.display = 'inline-block';
    } else {
      querySelector('#getPullRequestView').style.display = 'inline-block';
      querySelector('#getChangesView').style.display = 'inline-block';
    }
  }

  ///Callback for mousemovements
  void mouseMoveCallback(MouseEvent event) {
    var pos = event.page;
    var x = pos.x / window.outerWidth;
    var y = pos.y / window.outerHeight;
    //gets the smallest element at the recorded position.
    var webElem = coordinateConverter.posToElement(pos, false)?.element;

    socket.sendMouseMoveEvent(event, webElem, '{"x":$x,"y":$y}');
  }

  /// Generates the new visible elements after a scrolling event has occured.
  void scrollingCallback(Event e) {
    scrolling = null;

    BodyElement target = document.body;
    int data = target
        .getBoundingClientRect()
        .top
        .round(); //On laptop this returns double value
    /*
      Changed css for .container-body.(Removed overflow-x: hidden)
      It caused data to always be equal to 0.
      Should be researched if errors occur.
      New error:
      div class='row relative' now penetrates header
      in pull request view. Unclear why.
     */

    socket.sendInteractionEvent(e, data.toString());
  }

  /*
  /// Currently not used! 
  /// Retrieves the list of files in PurService and updates the list of files in the navbar. 
  Future<void> updateFilesList() async {
    fileCollection = await model.getFileNames();
    fileView.update(fileCollection);
    diffView = DiffView(
        fileCollection,
        model,
        diff,
        coordinateConverter.generateVisibleElements,
        socket.sendInteractionEvent);

    coordinateConverter.generateVisibleElements();
  }*/

  ///
  Future<void> updateContent(MouseEvent e) async {
    socket.sendInteractionEvent(e);

    await model.updateContent(e, textArea.innerText);
    fileView.displayFileContent(
        e, model.currentFile.name); //update the hovering for the new changes
  }

  void showPullRequestView(MouseEvent e) {
    socket.sendInteractionEvent(e);
    try {
      inLoopSocket.webSocket.send('{"action": "enteringPRList"}');
    } catch (e) {
      print(e);
      print('couldnt access fixation service');
    }
    pullRequestView.generatePullRequestList();
  }

  void showChangesView(MouseEvent e) {
    socket.sendInteractionEvent(e);
    changesView.generateChanges();
  }

  void showReplayView(MouseEvent e) {
    socket.sendInteractionEvent(e);
    replay.getPaths();
    replayView.generateStartReplayPage().then((value) {
      var Btn = querySelector('#checkPathButton');
      Btn.addEventListener('click', (event) => purgeData());
    });
  }

  //clears the locally stored valuables when starting replay
  void purgeData() {
    querySelector('#profile-information').innerHtml = '';
//    var elem = querySelector('#profile-information');
//    elem.remove();
    annModel.clearStorage();
    UserInfo.restoreImagelist();
  }

  void escapeReplayView(MouseEvent e) {
    replay.exitReplay();
    replayView.generateStartReplayPage();
    purgeData();
  }

  void showDiffView(MouseEvent e) {
    socket.sendInteractionEvent(e);

    diffView.generateDiffView();
    diffView.generateDiffPage(model.currentFile);
  }

  void showFileView(MouseEvent e) {
    socket.sendInteractionEvent(e);

    fileView.generateFileView();
    fileView.update(fileCollection);
  }

  void showChangeLog(MouseEvent e) {
    socket.sendInteractionEvent(e);

    changelogView.generateChangeLog();
  }

  void toggleTracking(Event e) {
    socket.sendInteractionEvent(e);

    if (!isTracking) {
      //socket.init(coordinateConverter.convertDataToElement, receiveASTData);
      //service.sendRequest('http://localhost:8001/startfix');
      inLoopSocket.init(coordinateConverter.convertDataToElement);
    } else {
      //socket.close();
      inLoopSocket.close();
      //service.sendRequest('http://localhost:8001/stopfix');
    }
    isTracking = !isTracking;
  }
}
