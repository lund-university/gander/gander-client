import 'dart:html';
import '../file/diff/diff_model.dart';

class ChangelogDetailsView {
  Commit shownCommit;
  Function buttonCallback;

  ChangelogDetailsView(this.shownCommit, this.buttonCallback);

  /// Resets the view and generates a new commit view.
  void generateCommit() {
    querySelector('.container-body').innerHtml = '';
    displayCommitView();
  }

  /// Displays the current [shownCommit] as a card with its information.
  void displayCommitView() {
    var cardDiv = DivElement()
      ..className = 'card'
      ..id = 'commit-view-card-${shownCommit.sha}';
    var labelSrc = LabelElement()
      ..text = 'Commit ${shownCommit.sha}'
      ..id = 'commit-view-label-${shownCommit.sha}';
    var commitInfo = generateInfoCard();
    var fileList = generateFileList();
    var buttonFooter = generateButtonFooter();

    cardDiv.append(labelSrc);
    cardDiv.append(commitInfo);
    cardDiv.append(fileList);
    cardDiv.append(buttonFooter);

    querySelector('.container-body').append(cardDiv);
  }

  /// Creates the card containing the information about the commit, like the sha number,
  /// author, date, etc.
  DivElement generateInfoCard() {
    var card = DivElement()..className = 'commit-info-card';
    var list = UListElement()..id = 'change-object';

    var sha = LIElement()
      ..className = 'list-item'
      ..id = 'list-item-sha'
      ..value = 0
      ..text = shownCommit.sha;

    var date = LIElement()
      ..className = 'list-item'
      ..id = 'list-item-date'
      ..value = 0
      ..text = shownCommit.date.toString();

    var message = LIElement()
      ..className = 'list-item'
      ..id = 'list-item-message'
      ..value = 0
      ..text = 'Message: ${shownCommit.message}';

    var author = LIElement()
      ..className = 'list-item'
      ..id = 'list-item-author'
      ..value = 0
      ..text = 'Author: ${shownCommit.author}';

    list.append(sha);
    list.append(date);
    list.append(message);
    list.append(author);
    card.append(list);
    return card;
  }

  /// Generates the list of files that is included in the commit. Returns a UListElement.
  UListElement generateFileList() {
    var list = UListElement()..id = 'changes-file-list';
    for (var changedFile in shownCommit.changedFiles) {
      var fileList = UListElement()
        ..id = 'changes-file-list-${changedFile.file.name}';

      var name = LIElement()
        ..className = 'list-item'
        ..id = 'changes-file-list-item-${changedFile.file.name}-name'
        ..value = 0
        ..text = changedFile.file.name;

      var additions = LIElement()
        ..className = 'list-item addition'
        ..id = 'changes-file-list-item-${changedFile.file.name}-additions'
        ..value = 0
        ..text = '+${changedFile.additions.toString()}';

      var deletions = LIElement()
        ..className = 'list-item deletion'
        ..id = 'changes-file-list-item-${changedFile.file.name}-deletions'
        ..value = 0
        ..text = '-${changedFile.deletions.toString()}';

      list.append(name);
      fileList.append(additions);
      fileList.append(deletions);

      list.append(fileList);
    }

    return list;
  }

  /// Generates a DivElement containing the footer with a back-button using the buttonCallback.
  DivElement generateButtonFooter() {
    var footerDiv = DivElement()
      ..className = 'footer'
      ..id = 'changelog-footer';
    var button = ButtonElement()
      ..className = 'changelog-button'
      ..id = 'changelog-button'
      ..text = 'Back'
      ..onClick.listen(buttonCallback);
    footerDiv.append(button);
    return footerDiv;
  }
}
