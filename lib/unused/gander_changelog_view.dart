/// TODO DOCUMENTATION
import 'dart:html';
import '../file/diff/diff_model.dart';
import 'gander_changelog_details_view.dart';

class ChangelogView {
  GanderDiff diff;
  Function visibleElementsCallback;
  Function sendInteractionCallback;

  ChangelogView(this.diff, this.sendInteractionCallback);

  /// Resets the Changelog view with the new list of commits.
  void generateChangeLog() {
    querySelector('.container-body').innerHtml = '';
    displayProgressionView();
  }

  /// Generates the progression view, containing the list of all of the commits in
  /// the collection.
  void displayProgressionView() async {
    var cardDiv = DivElement()
      ..className = 'card'
      ..id = 'progression-view-card';
    var labelSrc = LabelElement()
      ..text = 'Change log'
      ..id = 'changelog-label';

    cardDiv.append(labelSrc);

    var commitCollection = await diff.getCommits();
    var list = UListElement()..id = 'change-commit-list';
    var commits = commitCollection.commits;

    for (var current in commits) {
      var id = current.sha.substring(5);
      var changeObjList = UListElement()..id = '$id-commit-list';
      var smallCardDiv = DivElement()
        ..className = 'card'
        ..id = '$id-content-card';

      var sha = LIElement()
        ..className = 'list-item'
        ..id = '$id-list-item-sha'
        ..value = 0
        ..text = current.sha;

      var date = LIElement()
        ..className = 'list-item'
        ..id = '$id-list-item-date'
        ..value = 0
        ..text = 'Date: ${current.date.toString()}';

      var message = LIElement()
        ..className = 'list-item'
        ..id = '$id-list-item-message'
        ..value = 0
        ..text = 'Message: ${current.message}';

      var author = LIElement()
        ..className = 'list-item'
        ..id = '$id-list-item-author'
        ..value = 0
        ..text = 'Author: ${current.author}';

      changeObjList.append(sha);
      changeObjList.append(date);
      changeObjList.append(message);
      changeObjList.append(author);

      smallCardDiv.append(changeObjList);
      list.append(smallCardDiv);
      //smallCardDiv.onClick.listen((e) => displayChangeView(e, current));
    }
    cardDiv.append(list);

    querySelector('.container-body').append(cardDiv);
    visibleElementsCallback();
  }

  // /// Function for displaying the selected [commit] as a ChangeLogDetailsView.
  // void displayChangeView(MouseEvent event, Commit commit) async {
  //   sendInteractionCallback(event);
  //   var fetchedChanges = await diff.getCommitChanges(commit);
  //   var commitView = ChangelogDetailsView(fetchedChanges, returnToChangelog);
  //   commitView.generateCommit();
  //   visibleElementsCallback();
  // }

  /// Callback function that is passed to the ChangeLogDetailsView to be able to return to the ChangeLog view.
  void returnToChangelog(MouseEvent e) {
    sendInteractionCallback(e);
    generateChangeLog();
    visibleElementsCallback();
  }
}
