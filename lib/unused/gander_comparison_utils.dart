/// TODO DOCUMENTATION
import 'dart:collection';
import 'dart:html';

import '../file/compare/compare_model.dart';
import '../file/diff/diff_model.dart';
import '../file/file_model.dart';
import '../file/patch_handler.dart';

class ComparisonUtils {
  static void addComparisonView(
      List<Comparison> compList,
      String srcSha,
      String targetSha,
      GanderDiff diff,
      CommitCollection commitCollection,
      FileCollectionModel fileCollectionModel,
      Element listElem) async {
    listElem.innerHtml = '';

    List<String> srcCommitFiles = await diff.fetchCommitsFromSha(srcSha);
    //list of the files included in the srcCommit

    var dateLimit = commitCollection.getCommit(srcSha).date;

    for (var comp in compList) {
      var name = comp.filename;

      if (name.startsWith('.')) continue;
      //This if statement is a temporary solution, to make it possible to test the progress view
      var add = comp.additions;
      var del = comp.deletions;
      var status = comp.status;
      var fileObject = DivElement()
        ..className = 'card'
        ..id = 'file-object';
      var fileLabel = LabelElement()
        ..id = 'file-label-$name'
        ..text = '$name: $add additions $del deletions    status: $status';
      fileObject.append(fileLabel);
      var srcContent;
      if (srcCommitFiles.contains(name)) {
        //Check if the file was included in the srcCommit
        //If a commit does not affect the final version of a the file, it will not be included in the commit list for that file.
        //That is why we need to check if it is included in srcCommit before we fetch the latest version of the file content
        srcContent = await diff.fetchFileContentFromCommit(name, srcSha);
        srcContent = (srcContent == null) ? '' : srcContent;
      } else {
        srcContent = await fileCollectionModel.getLatestContentUntilDate(
            name, dateLimit.toString());
      }

      var targetContent =
          await diff.fetchFileContentFromCommit(name, targetSha);
      targetContent = (targetContent == null) ? '' : targetContent;

      generateAndAddContentAreas(
          name, srcContent, targetContent, comp, fileObject);
      listElem.append(fileObject);
      if (comp.patchList != null) {
        //If a file is missing a diff patch, the patchList is null. E.g. when the file name is changed.
        applyPatch(comp);
      }
    }
  }

  static void generateAndAddContentAreas(String fileName, String contentSrc,
      String contentTarget, Comparison comp, DivElement fileObject) {
    var srcSplitPointQueue = Queue<int>();
    var targetSplitPointQueue = Queue<int>();

    if (comp.patchList != null) {
      //When a file is missing diff patch, their patchList is null. This can happen when the file name is changed e.g.
      var patchObjectList = comp.patchList;
      srcSplitPointQueue =
          PatchHandler.createSplitPointQueue(patchObjectList, isSrc: true);
      targetSplitPointQueue =
          PatchHandler.createSplitPointQueue(patchObjectList, isSrc: false);
    }
    var srcAreaList =
        displayContent(contentSrc, 'src', fileName, srcSplitPointQueue);
    var targetAreaList = displayContent(
        contentTarget, 'target', fileName, targetSplitPointQueue);

    if (srcAreaList.length != targetAreaList.length) {
      return;
    }
    //TODO: make it possible to generate content areas even if srcAreaList.length != targetAreaList.length
    for (var i = 0; i < srcAreaList.length; i++) {
      DivElement srcArea = srcAreaList[i].div;
      DivElement targetArea = targetAreaList[i].div;
      var cardRow = DivElement()..className = 'row';
      var srcCol = DivElement()..className = 'col';
      srcCol.append(srcArea);
      cardRow.append(srcCol);

      var targetCol = DivElement()
        ..className = 'col'
        ..id = 'content-area-target-col';
      ;
      targetCol.append(targetArea);
      cardRow.append(targetCol);
      fileObject.append(cardRow);
      if (!targetAreaList[i].isPatch) {
        //srcArea and targetArea have the same content. We will not show them by default.
        srcArea.style.display = 'none';
        targetArea.style.display = 'none';

        var showMoreButton = ButtonElement()
          ..text = 'Show more'
          ..className = 'show-more';
        showMoreButton.onClick.listen(
            (event) => handleShowMore(srcArea, targetArea, showMoreButton));
        fileObject.append(showMoreButton);
      }
    }
  }

  static void handleShowMore(
      DivElement srcArea, DivElement targetArea, ButtonElement showMoreButton) {
    srcArea.style.display = 'block';
    targetArea.style.display = 'block';
    showMoreButton.style.display = 'none';
  }

  static void applyPatch(Comparison comp) {
    var patchList = comp.patchList;

    for (var patch in patchList) {
      // applyClassNamesFromPatch(patch);
    }
  }

  // static void applyClassNamesFromPatch(Patch patch) {
  //   var fileName = patch.fileName;
  //   fileName = fileName.replaceAll('.', '').replaceAll('/', '');
  //   var srcLineIndex = 0;
  //   var targetLineIndex = 0;
  //   var currentSrcElement = querySelector(getLineID(fileName, 1, CodeType.src));
  //   var currentTargetElement =
  //       querySelector(getLineID(fileName, 1, CodeType.src));
  //   var earlierLine;
  //   for (var i = 0; i < patch.codeList.length; i++) {
  //     var codeLine = patch.codeList[i];
  //     var diffList = codeLine.diffList;
  //     var firstChar = '';
  //     if (codeLine != null) {
  //       firstChar = codeLine.startsWith;
  //       var nextLine;
  //       var nextLineIndex = i + 1;
  //       if (nextLineIndex < patch.codeList.length &&
  //           (patch.codeList[nextLineIndex] != null)) {
  //         nextLine = patch.codeList[nextLineIndex];
  //       }

  //       if (firstChar == '+') {
  //         var nbrOfLines = codeLine.nbrOfLines;

  //         var nbrCharsFromStart = 0;

  //         for (var j = 0; j < nbrOfLines; j++) {
  //           //TODO: parsing to an intermediary data structure and working with that
  //           var targetLine = patch.startTagretLine + targetLineIndex;

  //           targetLineIndex++;
  //           var targetID = getLineID(fileName, targetLine, CodeType.target);
  //           var targetElement = querySelector(targetID);
  //           if (targetElement != null) {
  //             //This is a temporary solution that will be fixed in a future issue
  //             targetElement.className = 'addition';
  //             if (diffList.isNotEmpty) {
  //               diffList = applyLessTransparency(targetElement, diffList,
  //                   nbrCharsFromStart, CodeType.target);
  //             }

  //             nbrCharsFromStart += targetElement.children.length;

  //             currentTargetElement = targetElement;

  //             if (!currentCodePartIsShorter(j, earlierLine, '-') &&
  //                 !nextLineStartsWithOppositeSymbol(nextLine, '-')) {
  //               /*We want to add an empty line to the src if 
  //               the current Code part is longer than the one before
  //               and if the next codePart is not a deletion*/
  //               currentSrcElement =
  //                   generateWhiteSpace(currentSrcElement, currentTargetElement);
  //             }

  //             if (isEarlierCodePartLonger(
  //                 j, earlierLine, codeLine.nbrOfLines, '-')) {
  //               /*We want to add an empty line to the target if 
  //                   the current CodePart is shorter than the one before, 
  //                   and the one before was a deletion.*/
  //               var diff = earlierLine.nbrOfLines - codeLine.nbrOfLines;
  //               for (var k = 0; k < diff; k++) {
  //                 currentTargetElement = generateWhiteSpace(
  //                     currentTargetElement, currentSrcElement);
  //               }
  //             }
  //           }
  //         }
  //       } else if (firstChar == '-') {
  //         var nbrOfLines = codeLine.nbrOfLines;
  //         var nbrCharsFromStart = 0;
  //         for (var j = 0; j < nbrOfLines; j++) {
  //           var srcLine = patch.startSrcLine + srcLineIndex;
  //           srcLineIndex++;
  //           var srcID = getLineID(fileName, srcLine, CodeType.src);
  //           var srcElement = querySelector(srcID);
  //           if (srcElement != null) {
  //             //This is a temporary solution that will be fixed in a future issue
  //             currentSrcElement = srcElement;
  //             srcElement.className = 'deletion';
  //             if (diffList.isNotEmpty) {
  //               diffList = applyLessTransparency(
  //                   srcElement, diffList, nbrCharsFromStart, CodeType.src);
  //             }
  //             nbrCharsFromStart += srcElement.children.length;

  //             if (!currentCodePartIsShorter(j, earlierLine, '+') &&
  //                 !nextLineStartsWithOppositeSymbol(nextLine, '+')) {
  //               /*We want to add an empty line to the target if 
  //               the current Code part is longer than the one before
  //               and if the next codePart is not a addition*/
  //               currentTargetElement =
  //                   generateWhiteSpace(currentTargetElement, currentSrcElement);
  //             }

  //             if (isEarlierCodePartLonger(
  //                 j, earlierLine, codeLine.nbrOfLines, '+')) {
  //               /*We want to add an empty line to the src if 
  //                   the current CodePart is shorter than the one before, 
  //                   and the one before was a addition.*/
  //               var diff = earlierLine.nbrOfLines - codeLine.nbrOfLines;
  //               for (var k = 0; k < diff; k++) {
  //                 currentSrcElement = generateWhiteSpace(
  //                     currentSrcElement, currentTargetElement);
  //               }
  //             }
  //           }
  //         }
  //       } else if (firstChar == '\\') {
  //         //This line is '\ No newline at end of file'
  //         //Do nothing
  //       } else {
  //         var nbrOfLines = codeLine.nbrOfLines;

  //         for (var j = 0; j < nbrOfLines; j++) {
  //           var srcLine = patch.startSrcLine + srcLineIndex;
  //           var targetLine = patch.startTagretLine + targetLineIndex;
  //           srcLineIndex++;
  //           targetLineIndex++;
  //           currentSrcElement =
  //               querySelector(getLineID(fileName, srcLine, CodeType.src));
  //           currentTargetElement =
  //               querySelector(getLineID(fileName, targetLine, CodeType.target));
  //         }
  //       }
  //     } else {
  //       var srcLine = patch.startSrcLine + srcLineIndex;
  //       var targetLine = patch.startTagretLine + targetLineIndex;
  //       srcLineIndex++;
  //       targetLineIndex++;
  //       currentSrcElement =
  //           querySelector(getLineID(fileName, srcLine, CodeType.src));
  //       currentTargetElement =
  //           querySelector(getLineID(fileName, targetLine, CodeType.target));
  //     }
  //     earlierLine = codeLine;
  //   }
  // }

  // static List<Diff> applyLessTransparency(Element element, List<Diff> diffList,
  //     int nbrCharsFromStart, CodeType codeType) {
  //   var childList = element.children;
  //   var diff = diffList[0];

  //   for (var child in childList) {
  //     if (currentDiffIsOutOfRange(diff, nbrCharsFromStart)) {
  //       diffList.removeAt(0);
  //       if (diffList.isEmpty) {
  //         return <Diff>[];
  //       } else {
  //         diff = diffList[0];
  //       }
  //     }
  //     if (elementIsInRange(diff, nbrCharsFromStart)) {
  //       if (codeType == CodeType.src) {
  //         child.classes.add('diff-deletion');
  //       } else {
  //         child.classes.add('diff-addition');
  //       }
  //     }

  //     nbrCharsFromStart++;
  //   }
  //   return diffList;
  // }

  // ///Generates an ID for the pre elements in the progress view
  // static String getLineID(String name, int lineNumber, CodeType codeType) {
  //   var type = (codeType == CodeType.src) ? 'src' : 'target';
  //   return '#$name-$type-l$lineNumber';
  // }

  static Element generateWhiteSpace(
      Element currentOpposite, Element currentElement) {
    var emptyLine = Element.pre();

    var childList = currentElement.children;

    for (var child in childList) {
      emptyLine.children.add(Element.span()..text = ' ');
    }

    var elemList = currentOpposite.parent.children;
    var currentIndex = elemList.indexOf(currentOpposite);
    var emptyIndex = currentIndex + 1;

    if (emptyIndex < elemList.length) {
      currentOpposite.parent.children.insert(emptyIndex, emptyLine);
    } else {
      currentOpposite.parent.children.add(emptyLine);
    }
    return emptyLine;
  }

  static bool isEarlierCodePartLonger(int index, CodePart earlierLine,
      int currentNbrOfLines, String oppositeSymbol) {
    //Checks if the CodePart before this one hade the opposite first symbol and consists of more lines
    if ((index + 1) != currentNbrOfLines) {
      return false;
    }
    if (earlierLine == null) {
      return false;
    }

    return earlierLine.startsWith == oppositeSymbol &&
        earlierLine.nbrOfLines > currentNbrOfLines;
  }

  ///Checks if the CodePart before the current one hade the opposite first symbol and consists of fewer lines.
  static bool currentCodePartIsShorter(
      int indexOfCurrentLine, CodePart earlierLine, String oppositeSymbol) {
    if (earlierLine == null) {
      return false;
    }
    return oppositeSymbol == earlierLine.startsWith &&
        indexOfCurrentLine < earlierLine.nbrOfLines;
  }

  static bool nextLineStartsWithOppositeSymbol(
      CodePart nextLine, String oppositeSymbol) {
    //Checks is the next CodePart in the list is the opposite type by checking the firsta symbol
    if (nextLine == null) {
      return false;
    }
    return nextLine.startsWith == oppositeSymbol;
  }

  static bool currentDiffIsOutOfRange(Diff diff, int column) {
    if (column > diff.endColumn) {
      return true;
    }
    return false;
  }

  static bool elementIsInRange(Diff diff, int column) {
    var startColumn = diff.startColumn;

    var endColumn = diff.endColumn;

    if (column >= startColumn && column < endColumn) {
      return true;
    }
    return false;
  }

  static List<PatchDivWrapper> displayContent(String content, String type,
      String fileName, Queue<int> splitPointQueue) {
    var divElementList = <PatchDivWrapper>[];
    var name = fileName.replaceAll('.', '').replaceAll('/', '');

    var lineNbr = 0;
    var contentList = content.split('\n');

    var patchArea = DivElement();

    var splitPoint = 0;
    if (splitPointQueue.isNotEmpty) {
      splitPoint = splitPointQueue.removeFirst();
    }
    var isPatch = false;

    //We do not need to add anything if the first split point is at line 1.
    if (splitPoint != 1) {
      //This if statment will only add a new div if there is code above the first patch.
      var patchDivWrapper = PatchDivWrapper(patchArea, isPatch);
      divElementList.add(patchDivWrapper);
    }

    for (var line in contentList) {
      lineNbr++;
      if (lineNbr == splitPoint && lineNbr != contentList.length) {
        isPatch = !isPatch;
        patchArea = DivElement();
        divElementList.add(PatchDivWrapper(patchArea, isPatch));
        if (splitPointQueue.isNotEmpty) {
          splitPoint = splitPointQueue.removeFirst();
        }
      }
      var lineCode = Element.pre();

      lineCode.id = '$name-$type-l${lineNbr.toString()}';

      var colNbr = 0;
      for (var i = 0; i < line.length; i++) {
        var ch = line[i];
        colNbr++;
        var colTag = Element.span();

        colTag.text = ch;

        colTag.id = '$name-$type-l$lineNbr:$colNbr';

        lineCode.children.add(colTag);
      }
      if (!lineCode.hasChildNodes()) {
        //If lineCode has no children, it's an empty line
        lineCode.children.add(Element.span()..text = '\n');
      }
      patchArea.children.add(lineCode);
    }
    //Add css classes to the first and last div
    var firstDiv = divElementList[0].div;
    firstDiv.className = firstDiv.className + ' top-patch-div';
    patchArea.className = patchArea.className + ' bottom-patch-div';

    return divElementList;
  }
}
