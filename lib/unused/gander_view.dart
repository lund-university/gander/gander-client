import 'dart:html';
import '../file/file_model.dart';

/// Used for showing the file view, e.g. a text-area view of the content in a specific file.
/// When a file is loaded, it also checks for the AST information through Gander_model to provide highlighting to find the different variables and methods. See
/// Gander_model for more information
class PurView {
  FileCollectionModel model;

  /// Used for updating the coordinate converter with the current elements on the screen. Should be used after the viewport has been updated.
  Function visibleElementsCallback;

  /// Used for sending the different interactions to the storage service.
  Function sendInteractionCallback;

  PurView(this.model, this.sendInteractionCallback) {
    generateFileView();
  }

  /// Puts the Div-Element containing the file view in the container body.
  void generateFileView() {
    DivElement containerBody = querySelector('.container-body')..innerHtml = '';
    var contentView = DivElement()
      ..className = 'content-view-body'
      ..id = 'content-view-body';
    containerBody.append(contentView);

    // if (model.currentFile != null) {
    //   generateContentView(model.currentFile, model.currentFile.name);
    // }
    //visibleElementsCallback();
  }

  /// Updates the list of files shown to [collection].
  void update(FileCollection collection) {
    querySelector('#nameList').innerHtml =
        template(collection.files, collection.folders);
    addEventListeners(collection);
  }

  /// Adds event listeners for the new files [collection] in the list of files.
  /// When they are pressed, we want to display their content.
  void addEventListeners(FileCollection collection) {
    // For the files, we want to display their content.
    var liFileElements = querySelectorAll('.file');
    for (var item in liFileElements) {
      item.setAttribute('data-listener', 'true');
      item.onClick.listen((e) => displayFileContent(e, item.id));
    }
    // For the folders, we want to expand and show the folder content in the list.
    var liFolderElements = querySelectorAll('.folder');
    for (var item in liFolderElements) {
      item.setAttribute('data-listener', 'true');
      item.onClick.listen((e) => displayFolderContent(e, item.id));
    }
  }

  /// HTML template for the [files] and [folders] and how they are shown in the list of files.
  /// Returns a HTML String.
  String template(files, folders) {
    return folders.map(_singleFolderTemplate).join() +
        files.map(_singleFileTemplate).join();
  }

  /// Shows the pressed file [name]'s content in the container body.
  void displayFileContent(Event e, name) async {
    sendInteractionCallback(e);
    // Activates the possiblity to go into the diff view. A file needs to be selected for the user to do so.
    ButtonElement btn = querySelector('#getDiffView');
    btn.disabled = false;
    model.currentFile = model.getFile(name);
    DivElement contentDisplay = querySelector('.content-view-body');
    if (contentDisplay != null) {
      contentDisplay.innerHtml = '';
    } else {
      contentDisplay = DivElement()
        ..className = '.content-view-body'
        ..id = 'content-view-body';
      querySelector('.container-body')
        ..innerHtml = ''
        ..append(contentDisplay);
    }
    generateContentView(model.currentFile, name);
  }

  /// Adds the files contained in the pressed folder [name] to the list of files.
  void displayFolderContent(Event e, name) async {
    sendInteractionCallback(e);
    var folderName = name.substring(6);
    var folder = model.getFolder(folderName);
    var content = await model.getFolderContent(folder.name);
    LIElement folderElement = querySelector('#$folderName');

    var subList = querySelector('#$folderName-list namelist');
    subList ??= UListElement()..id = '$folderName-list namelist';
    subList..innerHtml = '';
    for (var c in content) {
      var subListEl = LIElement()
        ..id = '${c['name']}'
        ..text = '${c['name']}'
        ..className = 'pur file'
        ..setAttribute('data-listener', 'true')
        ..onClick.listen((e) => displayFileContent(e, c['name']));
      subList.children.add(subListEl);
    }
    folderElement
      ..lastChild.remove()
      ..append(subList);
  }

  /// Prepares the [list] of either methods or variables for higlighting.
  void prepForHighLight(list) {
    for (var def in list) {
      var methodName = def.name;
      var defLoc = def.location;

      highlightRange(defLoc.startLine, defLoc.startCol, defLoc.endLine,
          defLoc.endCol, 'def-$methodName');
      var useList = def.list;
      useList.forEach((element) {
        var useLoc = element.location;
        highlightRange(useLoc.startLine, useLoc.startCol, useLoc.endLine,
            useLoc.endCol, 'use-$methodName');
        createTooltip(useLoc, defLoc);
      });
    }
  }

  /// Sets event-listeners for the definitions. When a [def] is hovered, both the def and the respective list of [use]s should be highlighted.
  /// Depending on if the def [isVar], the highlights will have different colors.
  void setEventListenerDef(var def, var use, var isVar) {
    var defList = querySelectorAll('.$def');
    var useList = querySelectorAll('.$use');
    final spanCells = querySelectorAll('.code-line span');
    var defColor;
    var useColor;
    if (isVar) {
      defColor = 'DodgerBlue';
      useColor = 'CornflowerBlue';
    } else {
      defColor = 'gold';
      useColor = 'khaki';
    }

    defList.onMouseOver.listen((e) {
      for (var cell in spanCells) {
        cell.style.backgroundColor = 'white';
      }

      for (var cell in defList) {
        cell.style.backgroundColor = defColor;
      }
      for (var cell in useList) {
        cell.style.backgroundColor = useColor;
      }
    });

    defList.onMouseLeave.listen((e) {
      for (var cell in spanCells) {
        cell.style.backgroundColor = 'white';
      }
    });
  }

  /// Sets event-listeners for the uses. When a [use] is hovered, both the def and the respective list of [def]s should be highlighted.
  /// Depending on if the use [isVar], the highlights will have different colors.
  void setEventListenerUse(String def, String use, bool isVar) {
    var defList = querySelectorAll('.$def');
    var useList = querySelectorAll('.$use');
    final spanCells = querySelectorAll('#code-line span');
    var defColor;
    var useColor;
    if (isVar) {
      defColor = 'DodgerBlue';
      useColor = 'CornflowerBlue';
    } else {
      defColor = 'gold';
      useColor = 'khaki';
    }

    useList.onMouseOver.listen((e) {
      for (var cell in spanCells) {
        cell.style.backgroundColor = 'white';
      }
      for (var cell in defList) {
        cell.style.backgroundColor = defColor;
      }
      for (var cell in useList) {
        cell.style.backgroundColor = useColor;
      }
    });

    useList.onMouseLeave.listen((e) {
      for (var cell in spanCells) {
        cell.style.backgroundColor = 'white';
      }
    });
  }

  /// Adds the correct [className] to the respective spans in the range [startLine]:[startCol] - [endLine]:[endCol].
  /// This will enable the proper highlighting when they are hovered.
  void highlightRange(
      int startLine, int startCol, int endLine, int endCol, String className) {
    // retrieve code cells with a CSS selector
    final spanCells = querySelectorAll('span.span');
    spanCells.forEach((span) {
      var pos = span.id.substring(1); //remove 'l'
      var posParts = pos.split('-');
      var line = int.parse(posParts[0]);
      var col = int.parse(posParts[1]);

      if ((line > startLine && line < endLine) ||
          (startLine == line &&
              endLine == line &&
              col >= startCol &&
              col <= endCol) ||
          (startLine == line && col >= startCol && line != endLine) ||
          (endLine == line && col <= endCol && line != startLine)) {
        span.classes.add(className);
      }
    });
  }

  /// Template for a single [purFile] in the list of content.
  String _singleFileTemplate(purFile) => '''
  <li id="${purFile.name}" class = "pur file"  >
    ${purFile.name}
    </li>
  ''';

  /// Template for a single [purFolder] in the list of content.
  String _singleFolderTemplate(purFolder) {
    var template = '''
    <li id="${purFolder.name}" class = "pur"  >
      <div id="title-${purFolder.name}" class="folder">${purFolder.name}/</div>
      <ul id="${purFolder.name}-list namelist">
      </ul>
    </li>
    ''';
    return template;
  }

  /// Generates the view of the [file] in the container body.
  void generateContentView(PurFile file, String fileName) async {
    var content = await model.getContent(fileName);
    DivElement contentDisplay = querySelector('.content-view-body');
    if (contentDisplay != null) {
      contentDisplay.innerHtml = '';
    } else {
      contentDisplay = DivElement()
        ..className = 'content-view-body'
        ..id = 'content-view-body';
      querySelector('.container-body').append(contentDisplay);
    }
    contentDisplay.append(generateNameDiv(fileName));
    generateContentArea(content, file);
    contentDisplay.append(generateButtons());
    querySelector('#saveUpdate').onClick.listen(updateContent);
    visibleElementsCallback();
  }

  /// Updates the content of the file.
  void updateContent(MouseEvent e) async {
    DivElement area = querySelector('#code-line');
    sendInteractionCallback(e);
    await model.updateContent(e, area.innerHtml);
  }

  /// Creates a Div-Element containing the [name] of the file.
  DivElement generateNameDiv(String name) {
    var nameDiv = DivElement()
      ..className = 'file-name'
      ..id = 'file-name-div-$name';
    var label = LabelElement()
      ..htmlFor = 'code-line'
      ..id = 'file-name-label-$name'
      ..innerHtml = 'Code content of $name';
    nameDiv.append(label);
    return nameDiv;
  }

  /// Generates the text area for the [file] with content [content].
  /// If it is a parsable file, it also checks for var and method uses and calls the
  /// methods for preparing the highlight.
  void generateContentArea(String content, PurFile file) async {
    DivElement contentDisplay = querySelector('.content-view-body');
    var contentDiv = DivElement()
      ..className = 'file-content card textarea'
      ..id = 'file-content-div-${file.name}';
    var textArea = DivElement()
      ..id = 'code-line-textarea-${file.name}'
      ..className = 'code-line'
      ..contentEditable = 'true';

    textArea.append(populateContentArea(content));

    contentDiv.append(textArea);
    contentDisplay.append(contentDiv);
  }

  /// Creates the text area with the content [content].
  /// Each character is transformed into a span, with an unique id. This is used to get the highlight to work
  /// properly.
  PreElement populateContentArea(String content) {
    var contentElement = PreElement();
    if (content == '') {
      return contentElement;
    } else {
      var lineNbr = 0;
      for (var line in content.split('\n')) {
        lineNbr++;
        var lineCode = Element.pre();
        lineCode.id = 'l ${lineNbr.toString()}';
        var colNbr = 0;
        for (var i = 0; i < line.length; i++) {
          var ch = line[i];
          colNbr++;
          var colTag = Element.span()
            ..text = ch
            ..id = 'l$lineNbr-$colNbr'
            ..className = 'span l$lineNbr'
            ..onClick.listen((event) {
              SpanElement target = event.target;
              model.getPositionInformation(target.id);
            });
          lineCode.children.add(colTag);
        }
        contentElement.children.add(lineCode);
      }
      return contentElement;
    }
  }

  /// Sets the definition spans of the list of Definitions and adds event listeners to them.
  void setUseDefEventListeners(List<Def> list) {
    for (var useDef in list) {
      var name = useDef.name;
      var isVar = useDef.isVar;

      setEventListenerUse('def-$name', 'use-$name', isVar);
      setEventListenerDef('def-$name', 'use-$name', isVar);
    }
  }

  /// Creates a Save and a Update button, and returns a Div with them both in it.
  DivElement generateButtons() {
    var btnDiv = DivElement()..className = 'file-buttons';
    var saveBtn = ButtonElement()
      ..id = 'saveUpdate'
      ..innerHtml = 'Save';
    var discardBtn = ButtonElement()
      ..id = 'removeUpdate'
      ..innerHtml = 'Discard';
    btnDiv.append(saveBtn);
    btnDiv.append(discardBtn);
    return btnDiv;
  }

  /// Creates a tooltip for showing the definition when hovering over the usage location.
  void createTooltip(dynamic useLocation, dynamic defLocation) {
    var text = getFirstLine(defLocation);

    for (int i = useLocation.startCol; i <= useLocation.endCol; i++) {
      var span = querySelector('#l${useLocation.startLine}-$i');
      span.attributes['data-tooltip'] = text;
      span.attributes['data-tooltip-position'] = 'top';
    }
  }

  /// Retrieves the first line of the definition [defLocation]. This is shown when hovering over a var- or method-use.
  String getFirstLine(var defLocation) {
    var startLine = defLocation.startLine;
    var spans = querySelectorAll('.span.l$startLine');
    var res = '';
    var withespaceTrimmed = false;
    for (var span in spans) {
      if (!withespaceTrimmed) {
        if (span.text != ' ') {
          withespaceTrimmed = true;
          res = span.text;
        }
      } else {
        res += span.text;
      }
    }
    return res;
  }
}
