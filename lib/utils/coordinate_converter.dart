import 'dart:convert';
import 'dart:html';
import 'package:collection/collection.dart';

/// A class used to keep track of the Website Elements located in the viewport.
class GanderConverter {
  List<WebsiteElement> visibleElements;

  GanderConverter() {
    visibleElements = <WebsiteElement>[];
  }

  /// Method for converting the eye-tracker coordinates [jsonString] to an element on the screen.
  String convertDataToElement(String jsonString, {isScaled = false}) {
    var pos = eyetrackingDataToPos(jsonString);
    if (pos != null) {
      //used for finding correct element on screen
      //var el = posToElement(pos, isScaled);
      var el = posToAOI(pos, isScaled);
      if (el != null) {
        var wrappedEl = createFoundElement(el, pos, jsonString);
        //If element exists, check if it's an AOI and trigger fixationm event on said element
        //print('_________________');
        //print(el.element.id);
        //print(el.element.className);
        if (el.element.id.contains('aoi-')) {
          var evt = Event('fixation');
          el.element.dispatchEvent(evt);
          //print(el.element.id);
        }
        return wrappedEl.getJson();
      }
    } else {
      var ts =  getTS(jsonString);
      return '{"coordinates":{"x":null, "y":null}, "ts":"$ts", "element":"", "element-id":""}';
    }
    return null;
  }

  /// Takes the eye-tracker data [jsonString] and returns the timestamp.
  double getTS(String jsonString) {
    Map<String, dynamic> eyetrackingData = jsonDecode(jsonString);
    return double.parse(eyetrackingData['ts']);
  }

  /// Converts the eye-tracking coordinates to a Position-object with the corresponding coordinates.
  /// we keep the raw data, i.e. not scaled, to make it work on different devices
  Point eyetrackingDataToPos(String jsonString) {
    Map<String, dynamic> eyetrackingData = jsonDecode(jsonString);
    var x = -1.0;
    var y = -1.0;
//    if (eyetrackingData['lx'] == null &&
//        eyetrackingData['rx'] == null &&
//        eyetrackingData['ly'] == null &&
//        eyetrackingData['ry'] == null) {
//      return null;
//    }
//    if(eyetrackingData['lx'] == null && eyetrackingData['rx'] == null) {
//      x = 0;
//    }
//    else {
//      try {
//        x = 0.5 * (eyetrackingData['lx'] + eyetrackingData['rx']);
//      } catch(e) {
//        x = eyetrackingData['lx'] ?? eyetrackingData['rx'];
//      }
//    }
//
//    if(eyetrackingData['ly'] == null && eyetrackingData['ry'] == null) {
//      y = 0;
//    }
//    else {
//      try {
//        y = 0.5 * (eyetrackingData['ly'] + eyetrackingData['ry']);
//      } catch(e) {
//        y = eyetrackingData['ly'] ?? eyetrackingData['ry'];
//      }
//    }

//    if (eyetrackingData['lx'] == null && eyetrackingData['rx'] != null) {
//      x = eyetrackingData['rx'];
//    } else if (eyetrackingData['lx'] != null && eyetrackingData['rx'] == null) {
//      x = eyetrackingData['lx'];
//    } else {
//      x = 0.5 * (eyetrackingData['lx'] + eyetrackingData['rx']);
//    }
//    if (eyetrackingData['ly'] == null && eyetrackingData['ry'] != null) {
//      y = eyetrackingData['ry'];
//    } else if (eyetrackingData['ly'] != null && eyetrackingData['ry'] == null) {
//      y = eyetrackingData['ly'];
//    } else {
//      y = 0.5 * (eyetrackingData['ly'] + eyetrackingData['ry']);
//    }
//    if (x == -1.0) {
//      x = y;
//    }
//    if (y == -1.0) {
//      y = x;
//    }
    try {
      x = eyetrackingData['x'].runtimeType == double
          ? eyetrackingData['x']
          : double.parse(eyetrackingData['x']);
      y = eyetrackingData['y'].runtimeType == double
          ? eyetrackingData['y']
          : double.parse(eyetrackingData['y']);
    } catch (e) {
      print('Could not parse eyetracking data');
      return Point(-1, -1);
    }
    return Point(x, y);
  }

  WebsiteElement posToAOI(Point pos, bool isScaled) {
    var scaledPos =
        Point(pos.x, pos.y - window.outerHeight + window.innerHeight);

    var elements =
        document.elementsFromPoint(scaledPos.x.round(), scaledPos.y.round());
    var b = false;
    if (elements.isNotEmpty) {
      var smallest = elements[0];
      for (var el in elements) {
        if (el.id.contains('aoi-') && !el.id.contains('arrow')) {
          //print(elements.map((e) => e.id));
          // print('${el.id}, ${scaledPos.x}, ${scaledPos.y}, ${window.scrollY}');
          smallest = el;
          b = true;
          break;
        }
      }

      if (smallest.className != 'placeholder' && b) {
        var startPos = Point(smallest.getBoundingClientRect().left,
            smallest.getBoundingClientRect().top);
        var endPos = Point(
            smallest.getBoundingClientRect().left + smallest.clientWidth,
            smallest.getBoundingClientRect().top + smallest.clientHeight);
        var smallestCoord = Coordinate(startPos, endPos);
        return WebsiteElement(smallestCoord, smallest);
      }
    }
    return null;
  }

  /// Finds the WebsiteElement located at Position [pos]
  WebsiteElement posToElement(Point pos, bool isScaled) {
    var scaledPos = isScaled
        ? pos
        : Point(
            pos.x * window.screen.width,
            pos.y * window.screen.height -
                window.outerHeight +
                window.innerHeight);

    var elements =
        document.elementsFromPoint(scaledPos.x.round(), scaledPos.y.round());

    if (elements.isNotEmpty) {
      var smallest = elements[0];
      for (var el in elements) {
        if (el.clientHeight * el.clientWidth <
            smallest.clientHeight * smallest.clientWidth) {
          smallest = el;
        }
      }

      if (smallest.className != 'placeholder') {
        var startPos = Point(smallest.getBoundingClientRect().left,
            smallest.getBoundingClientRect().top);
        var endPos = Point(
            smallest.getBoundingClientRect().left + smallest.clientWidth,
            smallest.getBoundingClientRect().top + smallest.clientHeight);
        var smallestCoord = Coordinate(startPos, endPos);
        return WebsiteElement(smallestCoord, smallest);
      }
    }
    return null;
  }

  /// Converts the Element [e]'s position to coordinates.
  Coordinate elementToCoords(Element e) {
    var foundElement =
        visibleElements.firstWhere((el) => el.element == e, orElse: () => null);
    return foundElement.coord;
  }

  /// Retrieves the address field element.
  WebsiteElement getAddressField() {
    return visibleElements.firstWhere(
        (websiteEl) => websiteEl.element.id == 'addressField',
        orElse: () => null);
  }

  /// Creates a representation of the element that was looked at in a format that it should be stored as.
  WrappedElement createFoundElement(
      WebsiteElement el, Point pos, String jsonString) {
    Map<String, dynamic> eyetrackingData = jsonDecode(jsonString);
    return WrappedElement(
        el, double.parse(eyetrackingData['ts']), pos, el.element.id);
  }
}

class WebsiteElement {
  Coordinate coord;
  Element element;
  num ts;

  WebsiteElement(this.coord, this.element, {this.ts});

  /// Checks if the WebsiteElement is inside the position [pos]
  bool isPosInElement(Point pos) {
    if (pos.x >= coord.startPos.x && pos.y >= coord.startPos.y) {
      if (pos.x <= coord.endPos.x && pos.y <= coord.endPos.y) {
        return true;
      }
    }
    return false;
  }

  /// Retrieves the size of the element.
  num size() {
    var yLength = coord.endPos.y - coord.startPos.y;
    var xLength = coord.endPos.x - coord.startPos.x;
    return (yLength * xLength);
  }

  @override
  String toString() {
    return '$element classname ${element.className} id ${element.id}: (${coord.startPos.x.toString()}:${coord.startPos.y.toString()}) - (${coord.endPos.x.toString()}:${coord.endPos.y.toString()})';
  }
}

/// A Website Element on the format that is used in storage.
class WrappedElement {
  WebsiteElement websiteEl;
  num ts;
  Point pos;
  String elementId;

  WrappedElement(this.websiteEl, this.ts, this.pos, this.elementId);

  String getJson() {
    //return '{ "ts" : "${ts.toString()}", "element" : "${websiteEl.element.toString()}", "element-id" : "$elementId", "coordinates" : { "x" : ${pos.x * window.outerWidth}, "y" : ${pos.y * window.outerHeight}}}';
    return '{ "ts" : "${ts.toString()}", "element" : "${websiteEl.element.toString()}", "element-id" : "$elementId", "coordinates" : { "x" : ${pos.x}, "y" : ${pos.y}}}';
  }
}

class Coordinate {
  Point startPos, endPos;

  Coordinate(this.startPos, this.endPos);
}
