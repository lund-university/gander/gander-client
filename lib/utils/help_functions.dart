///Contains information about a file type
//I can see no reason to keep these classes. They are only causing 
//confusion and the functionality could be implemented in a much more straight forward way.
abstract class FileType {
  String firstSymbol;
  String type;
  String className;
  String oppositSymbol;
  FileType();
}

class SrcType extends FileType {
  @override
  final String firstSymbol = '-';
  @override
  final String type = 'src';
  @override
  final String className = 'deletion';
  @override
  final String oppositSymbol = '+';

  SrcType();
}

class TargetType extends FileType {
  @override
  final String firstSymbol = '+';
  @override
  final String type = 'target';
  //the className for some reason defines the color of the text
  @override
  final String className = 'addition';
  @override
  final String oppositSymbol = '-';

  TargetType();
}

///Creates a mutable index varible
//This class just represents an int?
//In what kind of world would you need a class to represent an int?
class LineIndex {
  int currentLine = 1;
  int getLine() {
    return currentLine;
  }

  void increaseWith(int i) {
    currentLine += i;
  }

  void increase() {
    currentLine++;
  }
}
