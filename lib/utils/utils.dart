import 'dart:convert';
import 'dart:async';

/// Class containing different utility methods.
class GanderUtils {
  /// Decodes the base 64 encoded content [encodedContent], and returns a decoded String.
  static String decodeContent(String encodedContent) {
    //In case of encoded content being null, return an empty string to aviod crashing the client.
    var content = encodedContent ?? '';
    var contentList = content.split('\n');
    var buffer = StringBuffer();
    for (var line in contentList) {
      try {
        line = utf8.decode(base64.decode(base64.normalize(line)));
      } catch (e) {
        line = '';
      }

      buffer.write(line);
    }
    return buffer.toString();
  }

  /// Removes the characters that are not allowed in a html id or html class.
  static String santizeFileName(String name) {
    return name.replaceAll('.', '').replaceAll('/', '');
  }

  /// Sets a timeout that calls the [callback] function when the timer has expired.
  /// Used when updating the visible elements after scrolling events.
  static Timer setTimeout(callback, [int duration = 1000]) {
    return Timer(Duration(milliseconds: duration), callback);
  }

  /// Clears the timeout of timer [t].
  static void clearTimeout(Timer t) {
    if (t != null) {
      t.cancel();
    }
  }
}
