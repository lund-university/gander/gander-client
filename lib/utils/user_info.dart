/// TODO DOCUMENTATION
class UserInfo {
  static String username = 'SuperUser';
  static String imagePath = './favicon.ico';
  static List<String> imagePathList = [
    './goose.png',
    './green_bird.png',
    './swan.png',
    './bullfinch.png',
    './flamingo.png',
    './yellow_bird.png',
    './puffin.png',
    './colibri.png'
  ];

  static bool isTyping = false;

  static Map usernameToImagePath = <String, String>{};

  static void restoreImagelist() {
    imagePathList = [
      './goose.png',
      './green_bird.png',
      './swan.png',
      './bullfinch.png',
      './flamingo.png',
      './yellow_bird.png',
      './puffin.png',
      './colibri.png'
    ];
  }
}
