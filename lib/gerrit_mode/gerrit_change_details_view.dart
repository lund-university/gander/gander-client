import 'dart:html';
import 'dart:math';

import 'package:gander/utils/user_info.dart';

import '../comments/annotation_model.dart';
import '../connection/in_loop_ws_client.dart';
import '../file/content_area_handler.dart';
import 'gerrit_model.dart';

class ChangeDetailsView {
  static const int OPEN_SQUARE_BRACKET = 91;
  static const int CLOSED_SQUARE_BRACKET = 93;

  CommentCollection commentColl;
  InLoopWsClient socket;
  Function postCommentCallback;
  Function returnToOverview;
  Function interactionCallback;
  List<ChangeFile> fileList;
  DivElement cardDiv;

  int currentIndex = 0;
  List<DivElement> contentDivList;
  DivElement navigationDiv;
  DivElement fileInfoDiv;
  SelectElement fileDropdown;
  ButtonElement upButton;

  ChangeDetailsView(this.fileList, this.commentColl, this.socket, this.postCommentCallback,
      this.returnToOverview, this.interactionCallback);

  void initChangeDetailsView() async {
    cardDiv = DivElement()
      ..id = 'change-view-card'
      ..style.display = 'none'
      ..className = 'card';

    querySelector('.container-body').append(cardDiv);

    initNavigation();

    await prepareContent();

    window.onPopState.listen((PopStateEvent event) {
      if (event.state != null) {
        var view = event.state['view'];

        if (view == 'overview') {
          var mouseEvent = MouseEvent('click');
          upButton.dispatchEvent(mouseEvent);
        }
      } 
    });
  }

  void showChangeDetailsView(index) async {
    window.scrollTo(0, 0);

    showContentByIndex(index);

    navigationDiv.style.display = 'flex';
    cardDiv.style.display = 'block';
  }

  void showChangeOverview(event, [String data = '']) {
    interactionCallback(event, data);

    returnToOverview();

    hideChangeDetailsView();
  }

  void hideChangeDetailsView() {
    cardDiv.style.display = 'none';
    navigationDiv.style.display = 'none';
  }

  void initNavigation() {
    var prevButton = ButtonElement()
      ..text = 'Previous'
      ..id = 'prev-button'
      ..className = 'navigation-button'
      ..style.marginRight = '10px'
      ..onClick.listen((e) => showPreviousContent(e));

    upButton = ButtonElement()
      ..text = 'Up'
      ..id = 'up-button'
      ..className = 'navigation-button'
      ..style.marginRight = '10px'
      ..onClick.listen((e) => showChangeOverview(e));

    var nextButton = ButtonElement()
      ..text = 'Next'
      ..id = 'next-button'
      ..className = 'navigation-button'
      ..style.marginRight = '30px'
      ..onClick.listen((e) => showNextContent(e));

    fileInfoDiv = DivElement()
      ..id = 'file-info'
      ..style.display = 'flex'
      ..style.alignItems = 'center'
      ..style.justifyContent = 'center'
      ..style.margin = '0 10px';

    var buttonDiv = DivElement()
    ..style.display = 'flex';
    buttonDiv.append(fileInfoDiv);
    buttonDiv.append(prevButton);
    if (returnToOverview != null) buttonDiv.append(upButton);
    buttonDiv.append(nextButton);

    fileDropdown = SelectElement()
      ..id = 'content-dropdown'
      ..style.margin = '0 10px';

    var fileDropDiv = DivElement();
    fileDropDiv.append(fileDropdown);

    for (var i = 0; i < fileList.length; i++) {
      var file = fileList[i];
      var option = OptionElement()
        ..value = i.toString()
        ..text = file.filename;
      fileDropdown.append(option);
    }

    fileDropdown.onChange.listen((event) {
      final target = event.target as SelectElement;
      final value = target.value;
      if (value != null) {
        interactionCallback(event, value);
        var selectedIndex = int.parse(value);
        showContentByIndex(selectedIndex);
      }
    });

    navigationDiv = DivElement()
      ..style.display = 'none'
      ..className = 'navigation';

    navigationDiv.append(fileDropDiv);
    navigationDiv.append(buttonDiv);

    querySelector('.container-navigation').append(navigationDiv);

    document.onKeyPress.listen((event) {
      navigateWithShortcut(event);
    });

    document.on['customKeypress'].listen((event) {
      navigateWithShortcutReplay(event);
    });
  }

  void prepareContent() async {
    // By creating a list of Futures we can execute _initContentView in parallel
    var contentHandlers = <ContentAreaHandler>[];

    for (var file in fileList) {
      if (file.patchList != null) {
        var view = ContentAreaHandler(file, commentColl, file.content,
            interactionCallback, postCommentCallback,
            commentsAreEnabled: true);
        contentHandlers.add(view);
      }
    }

    // This is to ensure each contentView is placed in the correct sorted order
    contentDivList = List<DivElement>.filled(contentHandlers.length, null);
    for (var i = 0; i < contentHandlers.length; i++) {
      var contentDiv = await contentHandlers[i].getContentView();
      contentDiv.style.display = 'none';
      contentDivList[i] = contentDiv;
      cardDiv.append(contentDiv);
    }

    for (var view in contentHandlers) {
      view.generateAOI();
    }
  }

  void _initContentView(ContentAreaHandler view, int index) async {
    var contentDiv = await view.getContentView();
    contentDiv.style.display = 'none';
    contentDivList[index] = contentDiv;
  }

  void _generateAOIs(ContentAreaHandler view) {
    view.generateAOI();
  }

  Future<void> showCurrentContent() async {
    if (contentDivList.isEmpty) return;

    for (var contentDiv in contentDivList) {
      contentDiv.style.display = 'none';
    }

    try {
      socket.webSocket.send(
        '{"action": "enteringFile", "data": "${fileList[currentIndex].sanitizedName}"}');
    } catch (e) {
      print('couldnt access fixation service');
    }

    contentDivList[currentIndex].style.display = 'block';

    fileInfoDiv.text = 'File ${currentIndex + 1} of ${contentDivList.length}';

    fileDropdown.selectedIndex = currentIndex;
  }

  void showContentByIndex(int index) async {
    if (index < 0 || index >= contentDivList.length) return;

    currentIndex = index;
    await showCurrentContent();
  }

  void showPreviousContent(event, [String data = '']) async {
    interactionCallback(event, data);
    if (currentIndex > 0) {
      currentIndex--;
      await showCurrentContent();
    }
  }

  void showNextContent(event, [String data = '']) async {
    interactionCallback(event, data);
    if (currentIndex < contentDivList.length - 1) {
      currentIndex++;
      await showCurrentContent();
    }
  }

  void navigateWithShortcut(KeyboardEvent event) async {
    if (UserInfo.isTyping) return;

    if (event.keyCode == OPEN_SQUARE_BRACKET) {
      showPreviousContent(event, event.keyCode.toString());
    } else if (event.keyCode == CLOSED_SQUARE_BRACKET) {
      showNextContent(event, event.keyCode.toString());
    } else if (event.key.toLowerCase() == 'u') {
      showChangeOverview(event, event.key.toLowerCase());
    }
  }

  // This method is only called in replay. This is because KeyboardEvents 
  // cannot be created programmatically and KeyEvents cannot be dispatched. 
  void navigateWithShortcutReplay(CustomEvent event) async {
    var key;

    try {
      key = int.parse(event.detail);  // Try to parse numeric key
    } catch (e) {
      handleAlphabeticKeys(event.detail, event);
      return;
    }

    handleNumericKeys(key, event);
  }

  void handleAlphabeticKeys(String key, CustomEvent event) {
    switch (key.toLowerCase()) {  
      case 'u':
        showChangeOverview(event, key);
        break;
      default:
        print('Unknown alphabetic key: $key');
    }
  }

  void handleNumericKeys(int key, CustomEvent event) {
    switch (key) {
      case OPEN_SQUARE_BRACKET:
        showPreviousContent(event, key.toString());
        break;
      case CLOSED_SQUARE_BRACKET:
        showNextContent(event, key.toString());
        break;
      default:
        print('Unknown numeric key: $key');
    }
  }
}
