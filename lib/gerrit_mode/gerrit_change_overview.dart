import 'dart:html';
import 'dart:math';
import 'dart:svg';

import '../comments/annotation_model.dart';
import '../connection/in_loop_ws_client.dart';
import '../file/content_area_handler.dart';
import 'gerrit_change_details_view.dart';
import 'gerrit_model.dart';

class ChangeOverview {
  Change change;
  GerritModel gerritModel;
  CommentCollection commentColl;
  InLoopWsClient socket;
  Function postCommentCallback;
  Function interactionCallback;
  Function returnToChanges;

  DivElement changeOverviewDiv;

  ChangeDetailsView changeDetailsView;
  List<ChangeFile> fileList;
  List<TableCellElement> titleList = [];

  bool useOverview;

  ChangeOverview(
      this.change,
      this.gerritModel,
      this.commentColl,
      this.socket,
      this.postCommentCallback,
      this.interactionCallback,
      this.returnToChanges,
      this.useOverview);

  void initChangeOverview() async {
    window.scrollTo(0, 0);

    querySelector('.container-body').innerHtml = '';
    querySelector('body').className = '';

    removeDuplicateComments();

    showLoadingScreen();

    await initFileList();

    if (useOverview) {
      await generateOverview();
    } else {
      await generateDetailsView();
    }

    hideLoadingScreen();

    window.onPopState.listen((PopStateEvent event) {
      if (event.state != null) {
        var view = event.state['view'];
        if (view == 'details') {
          var mouseEvent = MouseEvent('click');

          titleList[changeDetailsView.currentIndex].dispatchEvent(mouseEvent);
        }
      }
    });

    window.history.pushState({'view': 'overview'}, '', '');
  }

  void generateOverview() async {
    changeOverviewDiv = DivElement()
      ..id = 'change-overview-card'
      ..className = 'card';

    querySelector('.container-body').append(changeOverviewDiv);

    var header = HeadingElement.h1()
      ..text = change.subject
      ..id = 'aoi-overview-header';
    header.addEventListener('fixation', (event) {
      interactionCallback(event);
    });

    changeOverviewDiv.append(header);

    addCommitMsg();
    addFileTable();

    changeDetailsView = ChangeDetailsView(fileList, commentColl, socket,
        postCommentCallback, showChangeOverview, interactionCallback);
    await changeDetailsView.initChangeDetailsView();
  }

  void generateDetailsView() async {
    changeDetailsView = ChangeDetailsView(
        fileList, commentColl, socket, postCommentCallback, null, interactionCallback);
    await changeDetailsView.initChangeDetailsView();
    changeDetailsView.showChangeDetailsView(0);
  }

  // Function to show a loading overlay
  void showLoadingScreen() {
    var loadingOverlay = DivElement()..id = 'loading-overlay';

    var loadingText = DivElement()
      ..className = 'loading-text'
      ..text = 'Loading...';

    loadingOverlay.append(loadingText);
    querySelector('.container-body').append(loadingOverlay);
  }

  // Function to hide the loading overlay
  void hideLoadingScreen() {
    var loadingOverlay = querySelector('#loading-overlay');
    if (loadingOverlay != null) {
      loadingOverlay.remove();
    }
  }

  void initFileList() async {
    fileList = await gerritModel.getChangeFiles(change.number);
    fileList.sort((a, b) => a.filename.compareTo(b.filename.toLowerCase()));
  }

  void addFileTable() {
    var header = HeadingElement.h1()
      ..text = 'Files'
      ..id = 'files-header';

    changeOverviewDiv.append(header);

    var table = TableElement()..id = 'files';
    var thead = table.createTHead();
    var tbody = table.createTBody();

    var headerRow = thead.addRow();
    headerRow.classes.add('table-header');
    headerRow.append(TableCellElement()..text = 'Status');
    headerRow.append(TableCellElement()..text = 'File');
    headerRow.append(TableCellElement()..text = 'Delta');

    for (var file in fileList) {
      addFileRow(file, tbody);
    }

    changeOverviewDiv.append(table);
  }

  void addFileRow(file, body) {
    var fileInfoRow = body.addRow();
    fileInfoRow.className = 'file-info-row';
    fileInfoRow.id = 'aoi-file-info-row-${file.filename}';
    fileInfoRow.addEventListener(
        'fixation', (event) => interactionCallback(event));
    var filename =
        file.filename == '/COMMIT_MSG' ? 'Commit message' : file.filename;

    var path = filename.substring(0, filename.lastIndexOf('/') + 1);
    var filenameOnly = filename.substring(filename.lastIndexOf('/') + 1);

    var title = TableCellElement()
      ..id = 'file-list-item-title-${fileList.indexOf(file)}'
      ..className = 'file-title';

    var pathSpan = SpanElement()..text = path;

    var filenameSpan = SpanElement()
      ..text = filenameOnly
      ..style.color = '#1565c6'
      ..style.cursor = 'pointer';

    title.append(pathSpan);
    title.append(filenameSpan);

    var delta = TableCellElement()
      ..style.display = 'flex'
      ..style.justifyContent = 'space-between'
      ..style.width = '90px';

    if (file.filename == '/COMMIT_MSG') {
      fileInfoRow.append(TableCellElement());
      fileInfoRow.append(title);
      fileInfoRow.append(delta);
    } else {
      var additions = ParagraphElement()
        ..text = '+' + file.additions.toString()
        ..style.color = 'green'
        ..style.margin = '0'
        ..style.fontSize = '12px';

      var deletions = ParagraphElement()
        ..text = '-' + file.deletions.toString()
        ..style.color = 'red'
        ..style.margin = '0'
        ..style.fontSize = '12px';

      delta.append(additions);
      delta.append(deletions);

      fileInfoRow
          .append(TableCellElement()..text = file.status[0].toUpperCase());
      fileInfoRow.append(title);
      fileInfoRow.append(delta);
    }

    title.addEventListener('click', (event) async {
      enterFile(fileList.indexOf(file), event);
    }, true);

    titleList.add(title);
  }

  void enterFile(index, event) {
    interactionCallback(event);

    if (window.history.state['view'] != 'details') {
      window.history.pushState({'view': 'details'}, '', '');
    }

    changeOverviewDiv.style.display = 'none';
    changeDetailsView.showChangeDetailsView(index);
  }

  void showChangeOverview() {
    if (window.history.state['view'] != 'overview') {
      window.history.pushState({'view': 'overview'}, '', '');
    }

    try {
      socket.webSocket.send('{"action": "enteringOverview", "data": "${change.number}"}');  
    } catch (e) {
      print('couldnt access fixation service');
    }

    window.scrollTo(0, 0);

    changeOverviewDiv.style.display = 'block';
  }

  void addCommitMsg() {
    var content = fileList[0].content;

    var commitMsgDiv = DivElement()
      ..id = 'commit-msg-div'
      ..style.display = 'inline-block'
      ..className = 'card';

    var pre = PreElement()
      ..id = 'aoi-commit-msg'
      ..innerText = content;
    pre.addEventListener('fixation', (event) => interactionCallback(event));
    commitMsgDiv.append(pre);

    changeOverviewDiv.append(commitMsgDiv);
  }

  void removeDuplicateComments() {
    var tempColl = commentColl;

    for (var com in commentColl.comments) {
      if (tempColl.comments
              .where((c) => c.body == com.body && c.line == com.line)
              .length >
          1) {
        tempColl.comments.remove(com);
      }
    }
  }
}
