import 'dart:convert';

import '../file/compare/compare_model.dart';
import '../service_models/gander_service_model.dart';
import '../utils/utils.dart';

class GerritModel {
  static const _HOST = 'http://localhost';
  static const _GERRITPATH = '/gerrit';
  static const _CHANGESPATH= '/changes';
  static const _DIFFPATH = '/diff';
  static const _FILEPATH = '/files';
  var beenHere = [];

  static const int _PORT = 8001;

  GerritModel();

  Future<List<Change>> getChanges() async {
    try {
      var result = await GanderServiceModel.getRequest(
          '$_HOST:$_PORT$_GERRITPATH$_CHANGESPATH');

      return _processChanges(result);
    } catch (e) {
      print("Couldn't open $_HOST$_PORT$_GERRITPATH$_CHANGESPATH");
      rethrow;
    }
  }

  Future<List<ChangeFile>> getChangeFiles(int changeNumber) async {
    try {
      var result = await GanderServiceModel.getRequest(
          '$_HOST:$_PORT$_DIFFPATH$_CHANGESPATH/$changeNumber$_FILEPATH');

      return _processChangeFiles(result);
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_DIFFPATH$_CHANGESPATH/$changeNumber$_FILEPATH");
      rethrow;
    }
  }
  
  List<Change> _processChanges(String jsonString) {
    var changes = <Change>[];

    List<dynamic> fetchedChanges = json.decode(jsonString);
    for (var jsonChange in fetchedChanges) {
      var change = Change(
          jsonChange['number'],
          jsonChange['id'],
          jsonChange['subject'],
          jsonChange['created'],
          jsonChange['updated'],
          jsonChange['repo'],
          jsonChange['branch'],
          jsonChange['status']
          );
      changes.add(change);
    }
    return changes;
  }
  
  List<ChangeFile> _processChangeFiles(String jsonString) {
    var fileList = <ChangeFile>[];
    List<dynamic> jsonFileList = json.decode(jsonString);
    for (var file in jsonFileList) {

      if (file['additions'] == 0 && file['deletions'] == 0) {
        continue;
      }
      
      //In case of renaming, the file content is null, which causes Ganderutils.decodeContent to crash.
      var encodedContent = file['content'] ?? '';

      var content = GanderUtils.decodeContent(encodedContent);
      var jsonPatch = file['patch'];

      var filename = file['filename'];
      var sanitizedName = sanitizeFileName(filename); 
      var patchList;
      if (jsonPatch != null) {
        patchList = parsePatchFromMap(filename, jsonPatch);
      }
      var changeFile = ChangeFile(
          filename,
          sanitizedName,
          file['status'],
          file['additions'],
          file['deletions'],
          patchList,
          content,
          file['path']);

      fileList.add(changeFile);
    }
    return fileList;
  }

  String sanitizeFileName(String name) {
    return name.replaceAll('.', '').replaceAll('/', '').replaceAll('-', '').replaceAll('_', '');
  }
}

class ChangeFile extends Comparison {
  String sanitizedName;
  String path;
  String content;

  ChangeFile(String filename, this.sanitizedName, String status, int additions, int deletions,
      List<Patch> patchList, this.content, this.path)
      : super(filename, status, additions, deletions, patchList);
}

class Change {
  final int number;
  final String id;
  final String subject;
  final String created;
  final String updated;
  final String repo;
  final String branch;
  final String status;

  Change(this.number, this.id, this.subject, this.created, this.updated, this.repo, this.branch, this. status);
}