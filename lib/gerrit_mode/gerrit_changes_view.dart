import 'dart:html';

import 'package:gander/comments/annotation_model.dart';
import 'package:gander/connection/in_loop_ws_client.dart';
import 'package:gander/gerrit_mode/gerrit_change_overview.dart';
import 'package:intl/intl.dart';
import 'gerrit_change_details_view.dart';
import 'gerrit_model.dart';
import '../file/file_model.dart';

class ChangesView {
  GerritModel gerritModel;
  Function interactionCallback;
  AnnotationModel annModel;
  InLoopWsClient socket;

  bool useOverview = true;

  ChangesView(
      this.gerritModel, this.annModel, this.interactionCallback, this.socket);

  Future<void> generateChanges() async {
    querySelector('.container-body').innerHtml = '';
    querySelector('.container-navigation').innerHtml = '';
    await displayChanges();
    await FileCollectionModel.fetchSupportedExtensions();

    try {
      socket.webSocket.send('{"action": "enteringChanges"}');
    } catch (e) {
      print(e);
      print('couldnt access fixation service');
    }
  }

  Future<void> displayChanges() async {
    var changes = await gerritModel.getChanges();

    var cardDiv = DivElement()
      ..className = 'card'
      ..id = 'change-view-card';
    var header = HeadingElement.h1()
      ..text = 'Incoming reviews'
      ..id = 'changes-header';

    cardDiv.append(header);

    var table = TableElement()..id = 'changes';
    var thead = table.createTHead();
    var tbody = table.createTBody();

    var headerRow = thead.addRow();
    headerRow.classes.add('table-header');
    headerRow.append(TableCellElement()..text = 'Subject');
    headerRow.append(TableCellElement()..text = 'Number');
    headerRow.append(TableCellElement()..text = 'Repo');
    headerRow.append(TableCellElement()..text = 'Branch');
    headerRow.append(TableCellElement()..text = 'Updated');
    headerRow.append(TableCellElement()..text = 'Status');

    for (var current in changes) {
      var changeNumber = current.number;
      var changeInfoRow = tbody.addRow();
      changeInfoRow.className = 'change-info-row';
      changeInfoRow.id = 'change-info-row-$changeNumber';

      var title = TableCellElement()
        ..id = 'change-list-item-title-$changeNumber'
        ..className = 'change-title'
        ..text = current.subject;

      var updated = DateTime.parse(current.updated);
      var status = capitalize(current.status);

      changeInfoRow.append(title);
      changeInfoRow.append(TableCellElement()..text = changeNumber.toString());
      changeInfoRow.append(TableCellElement()..text = current.repo);
      changeInfoRow.append(TableCellElement()..text = current.branch);
      changeInfoRow.append(TableCellElement()..text = formatTimestamp(updated));
      changeInfoRow.append(TableCellElement()..text = status);

      var username = window.localStorage['currentUser'];
      if (username != null) {
        if (window.localStorage['$username-clicked-title-$changeNumber'] == 'true') {
          title.style.fontWeight = 'normal'; 
        } else {
          title.style.fontWeight = 'bold';
        }
      }

      title.addEventListener('click', (event) async {
        try {
          socket.webSocket.send(
              '{"action": "enterChange", "data": "${current.number}"}');
        } catch (e) {
          print('couldnt access fixation service');
        }

        if (username != null) {
          window.localStorage['${username}-clicked-title-$changeNumber'] = 'true';
        }

        enterChangeOverview(event, current);
        gerritModel.beenHere.add(current
            .number);
      }, true);
    }
    cardDiv.append(table);

    querySelector('.container-body').append(cardDiv);
  }

  void enterChangeOverview(event, current) async {
    interactionCallback(event);

    querySelector('body').className = 'wait';

    var changeNumber = current.number;

    await annModel.getComments('gerrit', changeNumber.toString());

    var commentColl = annModel.commentCollections.firstWhere(
        (c) => c.number == current.number.toString(),
        orElse: () => null);


    var changeOverview = ChangeOverview(current, gerritModel, commentColl, socket, annModel.postComment, interactionCallback, returnToChanges, useOverview);
    await changeOverview.initChangeOverview();
  }

  void returnToChanges(MouseEvent e) {
    interactionCallback(e);
    generateChanges();
  }

  String formatTimestamp(DateTime dateTime) {
    final now = DateTime.now();

    if (isToday(dateTime, now)) {
      return DateFormat.Hm().format(dateTime);
    } else if (isSameYear(dateTime, now)) {
      return DateFormat('MMM dd').format(dateTime);
    } else {
      return DateFormat('yyyy MMM dd').format(dateTime);
    }
  }

  bool isToday(DateTime dateTime, DateTime now) {
    return dateTime.year == now.year &&
      dateTime.month == now.month &&
      dateTime.day == now.day;
  }

  bool isSameYear(DateTime dateTime, DateTime now) {
    return dateTime.year == now.year;
  }

  String capitalize(String text) {
    var words = text.toLowerCase().split(' ');

    var capitalizedWords = words.map((word) {
      if (word.isNotEmpty) {
        return word[0].toUpperCase() + word.substring(1);
      }
      return word;
    }).toList();

    return capitalizedWords.join(' ');
  }
}
