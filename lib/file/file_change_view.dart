/// TODO DOCUMENTATION
import 'dart:collection';
import 'dart:html';
import 'dart:math';
import 'dart:convert';
import 'package:markdown/markdown.dart' show markdownToHtml;
import '../aoi/aoi_handler.dart';
import '../aoi/ast_converter.dart';
import 'compare/compare_model.dart';
import '../utils/help_functions.dart';
import 'file_model.dart';
import 'patch_handler.dart';
import '../comments/annotation_model.dart';
import '../utils/user_info.dart';
import '../aoi/aoi_base.dart';

import 'package:highlighting/highlighting.dart';
import 'package:highlighting/languages/java.dart';
import '../aoi/tokenizer.dart';

class ChangeView {
  FileCollectionModel fileCollModel;
  CommentCollection commentColl;
  Comparison comp;
  TargetType targetType = TargetType();
  SrcType srcType = SrcType();
  AOIHandlerBase aoi;
  Map<String, dynamic> config;

  Element targetPre;
  Element srcPre;

  String targetContent;
  String name;

  ///The file name without characters that are not allowed in a html id or html class.
  String sanitizedName;

  Function postCommentCallback;
  Function interactionCallback;

  DivElement fileObject;
  DivElement fileContent = DivElement()..id = 'file-content';

  bool commentsAreEnabled;
  PreElement commentBubbleSaved;

  //The ASTInformation for the source and target file
  ASTInformation srcAST = ASTInformation();
  ASTInformation targetAST = ASTInformation();
  //ASTFile astFile;
  //Map<String, List<Def>> targetUseDefmap;
  //Map<String, List<Def>> srcUseDefmap;

  //Used to keep track of the html id for items of the class row-relatvie
  int rowRelativCounter = 0;
  //Used to keep track of the html id for the whitespaces
  int whitespaceCounter = 0;

  ChangeView(
      this.comp,
      this.commentColl,
      this.targetContent,
      this.fileCollModel,
      this.interactionCallback,
      this.postCommentCallback,
      this.aoi,
      {this.commentsAreEnabled}) {
    name = comp.filename;
    sanitizedName = sanitizeFileName(name);
    getConfigInfo();
  }

  Future<void> getConfigInfo() async {
    var configContent = await HttpRequest.getString('config.json');
    Map<String, dynamic> conf = jsonDecode(configContent);
    config = conf;
  }

  /// Returns a version of [name] without any characters that are not allowed in a html id or html class.
  String sanitizeFileName(String name) {
    return name
        .replaceAll('.', '')
        .replaceAll('/', '')
        .replaceAll('-', '')
        .replaceAll('_', '');
  }

  DivElement getPRCommentSection() {
    var comments =
        commentColl.comments.where((comments) => comments.line == -1);
    var div =
        generateCommentSection(-1, commentColl.number, 'PR', false, comments);
    return div;
  }

  /// Creates and returns the DivElement that will contain the content.
  DivElement getFileObject() {
    var add = comp.additions;
    var del = comp.deletions;
    var status = comp.status;

    var fileObject = DivElement()
      ..className = 'file-content-box'
      ..id = 'file-object-$sanitizedName';
    var fileLabel = LabelElement()
      ..id = 'file-label-$name'
      ..text = '$name: $add additions $del deletions    status: $status';

    var labelElement = DivElement()
      ..className = 'file-label'
      ..id = 'file-div-label-id-$name';
    labelElement.append(fileLabel);
    fileObject.append(labelElement);
    fileObject.append(fileContent);
    return fileObject;
  }

  /// Returns a pre element consisting of one span for each char in [line].
  Element generateLine(int lineNbr, String type, String line) {
    var lineCode = Element.span();

    //var lineNumDiv = Element.span();
    var lineNum = Element.span();
    lineNum.text = lineNbr.toString();
    lineNum.id = '$sanitizedName-$type-line-$lineNbr';
    lineNum.className = 'lineNbr';
    //lineNumDiv.append(lineNum);
    //lineCode.children.add(lineNumDiv);

    var colNbr = 0;

    //Replace all \r with spaces to prevent empty spans
    line = line.replaceAll('\r', ' ');
    var spanString =
        Tokenizer.generateSpan(line, name, sanitizedName, type, lineNbr);
    //checks for which parts of the generated span that has highlighting, and we want to keep, the rest we
    //generate span by span to keep the highlighting and be able to use AOIs
    var map = Map<int, List<String>>();
    RegExp exp = RegExp(
        r'<span class=[^<>]*><span id=[a-zA-Z]*-[a-z]*-(l[0-9]*:[0-9]*)>([^<>]*)</span></span>');
    //RegExp exp2 =
    //    RegExp(r'<span id=[a-zA-Z]*-[a-z]*-(l[0-9]*:[0-9]*)>[^<>]*</span>');
    var m = exp.allMatches(spanString);
    if (m != null) {
      for (Match match in m) {
        //makes a key-value pair of the generated span and word and its starting column number
        //the word, or match.group(2) is needed to correctly update colNbr
        map[int.parse(match.group(1).split(':')[1])] = [
          match.group(0),
          match.group(2)
        ];
      }
    }
    for (var i = 0; i < line.length; i++) {
      var ch = line[i];
      colNbr++;
      if (map.containsKey(colNbr) && !map[colNbr][0].contains('params')) {
        //lineCode.children.add(lineNum);
        var span = Element.html(map[colNbr][0]);
        lineCode.children.add(span);
        //print(map[colNbr]);
        i += map[colNbr][1].length - 1;
        colNbr += map[colNbr][1].length - 1;
      } else {
        var colTag = Element.span();
        colTag.text = ch;
        colTag.id = '$sanitizedName-$type-l$lineNbr:$colNbr';
        lineCode.children.add(colTag);
      }
    }

    if (!lineCode.hasChildNodes()) {
      // If lineCode has no children, it's an empty line
      lineCode.text = '\n';
    }
    (type == 'target')
        ? targetAST.content.write(line + '\n')
        : srcAST.content.write(line + '\n');
    var lines = Element.pre();
    lines.id = '$sanitizedName-$type-l${lineNbr.toString()}';
    lineCode.className = 'lineCode';
    lines.append(lineNum);
    lines.append(lineCode);
    return lines;
  }

  /// Add the same string [line] to both the target and src file.
  void addSameLineToSrcAndTarget(
      int srcIndex, int targetIndex, String line, String buttonPos) {
    var targetLine = generateLine(targetIndex, 'target', line)
      ..className = '$sanitizedName-button-$buttonPos'
      ..style.display = 'none';
    var srcLine = generateLine(srcIndex, 'src', line)
      ..className = '$sanitizedName-button-$buttonPos'
      ..style.display = 'none';
    targetPre = targetLine;
    srcPre = srcLine;
  }

  /// Returns True if diff is out of range for the current column
  /// weird with the -2, don't know why it works, maybe look into
  static bool currentDiffIsOutOfRange(Diff diff, int column) {
    if (column - 2 >= diff.endColumn) {
      return true;
    }
    return false;
  }

  /// Returns True if diff is in range for the current column.
  /// Weird with the -1, don't udnerstand why it works but it does
  static bool elementIsInRange(Diff diff, int column) {
    var startColumn = diff.startColumn;

    var endColumn = diff.endColumn;

    if (column - 1 >= startColumn && column <= endColumn) {
      return true;
    }
    return false;
  }

  /// Applies a darker color to all spans that are in range of [nbrCharsFromStart].
  /// If a char has a darker color it means that char was changed between the src and the target file.
  void applyLessTransparency(Element element, int nbrCharsFromStart,
      List<Diff> diffList, String codeType) {
    var childList = element.children;
    var diff = diffList[0];

    for (var child in childList) {
      if (currentDiffIsOutOfRange(diff, nbrCharsFromStart)) {
        diffList.removeAt(0);
        if (diffList.isEmpty) {
          return;
        } else {
          diff = diffList[0];
        }
      }
      if (elementIsInRange(diff, nbrCharsFromStart)) {
        if (codeType == 'src') {
          child.classes.add('diff-deletion');
        } else {
          child.classes.add('diff-addition');
        }
      }

      nbrCharsFromStart++;
    }
  }

  /// Uses the patches and the content of the target file to generate
  /// a src file and a target file in change view with highlighting showing what was changed.
  void addLinesFromPatches() {
    var targetContentList = targetContent.split('\n');

    targetContentList = removeTralingEmptyString(targetContentList);

    var srcIndex = LineIndex(); //Create a mutable srcIndex

    // A copy of the patch list is made since items form the list will be removed
    var patchList = List<Patch>.from(comp.patchList);

    var srcSplitPointQueue =
        PatchHandler.createSplitPointQueuePR(patchList, isSrc: true);
    var targetSplitPointQueue =
        PatchHandler.createSplitPointQueuePR(patchList, isSrc: false);
    var srcSplitPoint = srcSplitPointQueue.removeFirst();
    var targetSplitPoint = targetSplitPointQueue.removeFirst();
    var hideArea = false;
    var buttonPos = 'top';
    var targetIndex = LineIndex(); //Create a mutable targetIndex
    // && !lines.contains(targetIndex.currentLine)

    var startHide = 0;

    var endHide = 0;
    while (targetIndex.currentLine < targetContentList.length + 1) {
      //LineIndex causes bug:
      //if not indexed in this way, we loose the first line of each file and a lot of the files wont
      //be assigned AOIs, which casuses all kinds of problems, both in client and in compileService.
      //Lineindex should be replaced by a better solution but until then this works.
      var line = targetContentList[targetIndex.currentLine - 1];
      if (srcIndex.currentLine != srcSplitPoint &&
          targetIndex.currentLine != targetSplitPoint) {
        if (!hideArea) {
          startHide = targetIndex.currentLine;
        }
        hideArea = true;

        addSameLineToSrcAndTarget(
            srcIndex.currentLine, targetIndex.currentLine, line, buttonPos);
        applyLastCodeSection();

        srcIndex.increase();
        targetIndex.increase();
      } else {
        if (hideArea) {
          endHide = targetIndex.currentLine;
          addShowMoreButton(buttonPos, endHide - startHide);
          buttonPos = 'bottom-${targetIndex.currentLine}';
          hideArea = false;
        }
        var oldValue;
        var onlyTarget = false;
        var onlySrc = false;
        // Check if we are adding the patch to only the src or only the target
        if (srcIndex.currentLine != srcSplitPoint) {
          onlyTarget = true;
          oldValue = srcIndex.currentLine;
        } else if (targetIndex.currentLine != targetSplitPoint) {
          onlySrc = true;
          oldValue = targetIndex.currentLine;
        }

        srcSplitPoint = getNextSplitPoint(srcSplitPointQueue);
        targetSplitPoint = getNextSplitPoint(targetSplitPointQueue);
        applyPatches(srcIndex, targetIndex, patchList);
        patchList.removeAt(0);

        if (onlyTarget) {
          srcIndex.increaseWith(targetIndex.currentLine - oldValue);
        } else if (onlySrc) {
          targetIndex.increaseWith(srcIndex.currentLine - oldValue);
        }
      }
    }
    if (hideArea) {
      endHide = targetIndex.currentLine;
      addShowMoreButton(buttonPos, endHide - startHide);
    }
  }

  /// Returns a list of strings with the last string deleted, to handle trailing empty Strings.
  List<String> removeTralingEmptyString(List<String> splitList) {
    // When a string is split and ends with the '\n', the last item in the split list will be an empty string
    if (splitList.isEmpty) {
      return splitList;
    }
    splitList.removeLast();
    return splitList;
  }

  /// Adds a show more-button to the fileContent
  void addShowMoreButton(String buttonPos, int lines) {
    var showMoreIcon = ImageElement()
      ..src = './showMore1.png'
      ..height = 22
      ..width = 22;

    var showMoreButton = ButtonElement()
      ..id = 'show-more-btn-$sanitizedName-$buttonPos'
      ..text = 'Show more - $lines common lines'
      ..style.backgroundColor = 'LightGray'
      ..style.borderStyle = 'solid'
      ..style.borderColor = 'black'
      ..className = 'show-more-$sanitizedName-button-$buttonPos';

    showMoreButton.append(showMoreIcon);

    showMoreButton.onClick
        .listen((event) => handleShowMore(event, buttonPos, showMoreButton));
    fileContent.append(showMoreButton);
  }

  /// The action that occurs after the showMore button is pressed.
  /// This will change the display from 'none' to 'block' for the elements
  /// that have the same className as the button.
  void handleShowMore(
      MouseEvent e, String buttonPos, ButtonElement showMoreButton) {
    interactionCallback(e);
    var className = '.$sanitizedName-button-$buttonPos';
    var elemList = fileContent.querySelectorAll(className);
    for (var elem in elemList) {
      if (elem.tagName == 'PRE') {
        elem.style.display = 'grid';
      } else {
        elem.style.display = 'block';
      }
    }
    showMoreButton.style.display = 'none';
    aoi.moveAOIs(showMoreButton.className);
    //var container = querySelector('#' + Css.escape('pull-request-view-card'));
    //for (var aoi in list) {
    //  container.append(aoi);
    //}
  }

  /// Add the latest code section to the [fileContent] object.
  void applyLastCodeSection() {
    //Counter used to keep track of ids
    rowRelativCounter++;
    var lineNumSrc = Element.span();
    lineNumSrc.text = rowRelativCounter.toString();
    lineNumSrc.id = '$sanitizedName-src-line-$rowRelativCounter';

    var lineNumTarget = Element.span();
    lineNumTarget.text = rowRelativCounter.toString();
    lineNumTarget.id = '$sanitizedName-target-line-$rowRelativCounter';

    var counterString = rowRelativCounter.toString();
    var cardRow = DivElement()
      ..className = 'row relative'
      ..id = '$sanitizedName-row-relative-$counterString';

    var srcCol = DivElement()
      ..className = 'col left'
      ..id = '${srcPre.id}-col-left';
    var targetCol = DivElement()
      ..className = 'col right'
      ..id = '${targetPre.id}-col-right';

    // srcCol.append(lineNumSrc);
    // targetCol.append(lineNumTarget);

    srcCol.append(srcPre);
    cardRow.append(srcCol);
    targetCol.append(targetPre);
    var targetId = targetPre;
    cardRow.append(targetCol);

    // If comments are enabled, add the comment bubble icon.
    // The user is not able to start a new comment thread without the comment bubble.
    if (commentsAreEnabled) {
      addCommentBubble(counterString, cardRow, targetId);
    }

    fileContent.append(cardRow);
  }

  /// Add the comment bubble icon that appears when the user hovers over the change view.
  void addCommentBubble(
      String counterString, DivElement cardRow, Element targetId) {
    var commentIcon = ImageElement()
      ..src = './comment-icon.png'
      ..width = 45
      ..height = 45
      ..style.margin = '5px'
      ..id = '$sanitizedName-comment-icon-$counterString';
    var commentBubble = DivElement()
      ..className = 'comment-bubble-hidden'
      ..id = '$sanitizedName-comment-bubble-$counterString'
      ..append(commentIcon);
    cardRow.append(commentBubble);
    cardRow.onMouseEnter.listen((event) {
      commentBubble.className = 'comment-bubble-shown';
    });
    cardRow.onMouseLeave.listen((event) {
      commentBubble.className = 'comment-bubble-hidden';
    });
    cardRow.onMouseUp.listen((event) {
      var selection = window.getSelection();
      if (selection.toString().isNotEmpty) {
        commentBubble.className = 'comment-bubble-shown';
        commentBubbleSaved = targetId;
      }
    });
    commentBubble.onClick.listen((event) {
      toggleCommentSection(event, targetId);
    });
    document.on['customKeypress'].listen((event) {
      toggleCommentSectionReplay(event);
    });
    document.onKeyPress.listen((event) {
      if (event.key == 'c' && commentBubbleSaved != null && !UserInfo.isTyping) {
        interactionCallback(event, commentBubbleSaved.id);
        toggleCommentSection(event, commentBubbleSaved);
      }
    });
  }

  ///Use the patches in [patchList] to generate the src and target file that
  ///is displayed in the change view.
  void applyPatches(
      LineIndex srcIndex, LineIndex targetIndex, List<Patch> patchList) {
    var patch = patchList[0];
    var codeList = patch.codeList;
    var prevFirstSymbol = '';
    var nextFirstSymbol = '';

    var prevLineLength = 0;
    var prevContentList = <String>[];
    var srcList = <Element>[];
    var targetList = <Element>[];

    for (var n = 0; n < codeList.length; n++) {
      var codePart = codeList[n];
      var nextCodePartIndex = n + 1;
      if (nextCodePartIndex < codeList.length) {
        var nextCodePart = codeList[nextCodePartIndex];

        nextFirstSymbol = nextCodePart.startsWith;
      }
      var startsWith = codePart.startsWith;
      var contentList = codePart.content.split('\n');
      //Remove the last item of the list (a trailing empty string)
      contentList = removeTralingEmptyString(contentList);

      if (startsWith == ' ') {
        addBothSidesPatch(srcList, targetList);
        srcList = <Element>[];
        targetList = <Element>[];
        for (var lineString in contentList) {
          var targetLine =
              generateLine(targetIndex.currentLine, 'target', lineString);
          var srcLine = generateLine(srcIndex.currentLine, 'src', lineString);

          targetPre = targetLine;
          srcPre = srcLine;
          applyLastCodeSection();

          srcIndex.increase();
          targetIndex.increase();
        }
      } else if (startsWith == '+') {
        //Add the patch to the target
        addPatchToOneSide(
            codePart,
            contentList,
            prevContentList,
            prevFirstSymbol,
            prevLineLength,
            nextFirstSymbol,
            targetIndex,
            targetType,
            targetList,
            srcList);
      } else if (startsWith == '-') {
        //Add the patch to the src
        addPatchToOneSide(
            codePart,
            contentList,
            prevContentList,
            prevFirstSymbol,
            prevLineLength,
            nextFirstSymbol,
            srcIndex,
            srcType,
            srcList,
            targetList);
      }
      prevFirstSymbol = startsWith;
      prevLineLength = codePart.nbrOfLines;
      prevContentList = contentList;
    }
    addBothSidesPatch(srcList, targetList);
  }

  ///Apply the content from the patch to both the src and target file.
  void addBothSidesPatch(List<Element> srcList, List<Element> targetList) {
    //print(srcList.length == targetList.length);
    for (var i = 0; i < srcList.length; i++) {
      var targetLine = targetList[i];

      var srcLine = srcList[i];
      srcPre = srcLine;
      targetPre = targetLine;

      applyLastCodeSection();
    }
  }

  ///Add the content of the patch to either only the src file or the target file
  void addPatchToOneSide(
      CodePart codePart,
      List<String> contentList,
      List<String> prevContentList,
      String prevFirstSymbol,
      int prevLineLength,
      String nextFirstSymbol,
      LineIndex index,
      FileType fileType,
      List<Element> list,
      List<Element> prevList) {
    var nbrCharsFromStart = 0;
    var nbrOfLines = codePart.nbrOfLines;
    var diffList = codePart.diffList;
    for (var i = 0; i < nbrOfLines; i++) {
      var lineString = contentList[i];
      var lineElem = generateLine(index.currentLine, fileType.type, lineString);

      //this className sets the styling of the line, which makes the text background red or green.
      lineElem.className = fileType.className;

      if (diffList.isNotEmpty) {
        applyLessTransparency(
            lineElem, nbrCharsFromStart, diffList, fileType.type);
      }

      nbrCharsFromStart += lineElem.children.length - 1;

      list.add(lineElem);
      //Fills out the other file with empty lines if the patch is not aligned
      if (!(fileType.oppositSymbol == prevFirstSymbol && i < prevLineLength) &&
          nextFirstSymbol != fileType.oppositSymbol) {
        var emptyLine = generateWhiteSpace();
        prevList.add(emptyLine);
      }
      index.increase();
    }
    //Following is responsible to fill out with empty lines to make the patches align in diff view
    if (prevFirstSymbol == fileType.oppositSymbol &&
          nextFirstSymbol != fileType.oppositSymbol) {
      if (prevLineLength > nbrOfLines) {
        for (var j = nbrOfLines; j < prevLineLength; j++) {
          var emptyLine = generateWhiteSpace();
          list.add(emptyLine);
        }
      }
    }
  }

  ///Returns the index of the next split point (the index of the next start of a patch)
  int getNextSplitPoint(Queue<int> splitPointQueue) {
    if (splitPointQueue.isNotEmpty) {
      return splitPointQueue.removeFirst();
    }
    return -1;
  }

  /// Retuns a DivElement containing the ChangeView for the current file.
  Future<DivElement> generateContentAreas() async {
    fileObject = getFileObject();
    if (comp.patchList != null) {
      addLinesFromPatches();

      //MOVE THIS TO A SEPARATE FUNCTION
      if (comp.isParsable) {
        // targetAST.collection = await fileCollModel.getAndSaveASTInformation(
        //   '0:0', targetAST.content.toString(), sanitizedName, 'target');
        //targetUseDefmap = fileCollModel.getUseDefMap();
        //srcAST.collection = await fileCollModel.getAndSaveASTInformation(
        //    '0:0', srcAST.content.toString(), sanitizedName, 'src');
        //srcUseDefmap = fileCollModel.getUseDefMap();
        //astFile = ASTFile(srcAST, targetAST, name);
        await fileCollModel.getAndSaveASTInformation(
            '0:0', targetAST.content.toString(), name, sanitizedName, 'target');
        await fileCollModel.getAndSaveASTInformation(
            '0:0', srcAST.content.toString(), name, sanitizedName, 'src');

        //print(fileCollModel.getAstMap());
      }
    }

    return fileObject;
  }

  /// Returns a empty pre element that can be used to align the code in src and the target.
  Element generateWhiteSpace() {
    whitespaceCounter++;
    var whitespaceCounterString = whitespaceCounter.toString();

    return Element.pre()
      ..text = '\n'
      ..id = '$sanitizedName-whitespace-$whitespaceCounterString';
  }

  /// Adds all of the comments for the file in the content area [fileObject].
  void addCommentForFile(DivElement fileObject) {
    var comments = commentColl.comments.where((element) =>
        sanitizeFileName(element.filePath) ==
        sanitizeFileName(comp.filename)); //fileNames does not always match

    var lines = comments.map((e) => e.line).toSet();
    for (var commentLine in lines) {
      var line = fileObject
          .querySelector('#$sanitizedName-target-l${(commentLine).toString()}');
      var commentsInLine =
          comments.where((element) => element.line == commentLine);
      var parent = line?.parent;
      var next = line?.nextElementSibling;
      var includeCancelButton = false;
      parent?.insertBefore(
          (generateCommentSection(commentLine, commentColl.number,
              comp.filename, includeCancelButton, commentsInLine)),
          next);
    }
  }

  /// Creates the div with the found comments [comments], if included. Otherwise, it will create a new comment dialogue-div for a user to insert new comments on the line [lineNo].
  DivElement generateCommentSection(int lineNo, String pullReqNumber,
      String fileName, bool includeCancelButton,
      [Iterable<Comment> comments]) {
    var name;
    if (fileName == 'PR') {
      name = fileName;
    } else {
      name = sanitizedName;
    }
    var commentDiv = DivElement()..id = '$name-comment-div-$lineNo';
    if (config != null && config['mode'] == 'github') {
      commentDiv.className = 'comment-div';
    } else {
      commentDiv.className = 'gerrit-comment-div';
    }
    if (querySelector('#$sanitizedName-comment-div-$lineNo-posted-comments') !=
        null) {
      return querySelector(
          '#$sanitizedName-comment-div-$lineNo-posted-comments');
    }

    if (comments != null) {
      commentDiv.append(generatePostedCommentsArea(lineNo, comments));
    } else {
      commentDiv.append(generatePostedCommentsArea(lineNo));
    }
    commentDiv.append(generateAddContentArea(
        lineNo, pullReqNumber, fileName, includeCancelButton));
    return commentDiv;
  }

  /// Returns an image path for the username [name]. If the user already has an image,
  /// the path to that image is returned. Otherwise, the user is given an available image.
  String getImagePathForUser(String name) {
    var userMap = UserInfo.usernameToImagePath;
    if (userMap.containsKey(name)) {
      return userMap[name];
    }
    var imagePathList = UserInfo.imagePathList;
    if (imagePathList.isNotEmpty) {
      var imagePath = imagePathList.removeAt(0);
      userMap[name] = imagePath;
      UserInfo.imagePathList = imagePathList;
      return imagePath;
    }
    // If there is no available images, the dart logo is used as default.
    return './favicon.ico';
  }

  ///Returns the div containing the button that the user can press to maximize a comment section.
  DivElement getMaximizeElement(
      DivElement commentDiv, int lineNo, int nbrOfComments) {
    var maximizeDiv = DivElement()
      ..className = 'comment-div maximize'
      ..id = '$sanitizedName-comment-div-$lineNo-posted-comments';

    var maximizeIcon = ImageElement()
      ..src = './expand-icon.png'
      ..className = 'change-comment-size-icon'
      ..id = '$sanitizedName-change-comment-size-icon-$lineNo';

    var maximizeButton = ButtonElement()
      ..append(maximizeIcon)
      ..id = '$sanitizedName-maximize-button-$lineNo'
      ..className = 'change-comment-size-button'
      ..onClick.listen((event) {
        interactionCallback(event);
        maximizeDiv.style.display = 'none';
        commentDiv.style.display = 'block';
      });
    maximizeDiv.append(maximizeButton);
    var showCommentElement = Element.p()..className = 'show-all-comments';

    //if statement to determine if "comment" of "comments" should be used
    showCommentElement.text = (nbrOfComments == 1)
        ? 'Show $nbrOfComments comment'
        : 'Show $nbrOfComments comments';

    maximizeDiv.append(showCommentElement);
    return maximizeDiv;
  }

  DivElement generatePostedCommentsArea(int lineNo,
      [Iterable<Comment> comments]) {
    var postedCommentsSection = DivElement()
      ..id = '$sanitizedName-comment-div-$lineNo-posted-comments'
      ..className = 'comment-div-posted-section';

    if (comments != null) {
      var nbrOfComments = comments.length;
      var minimizeIcon = ImageElement()
        ..src = './collapse-icon.png'
        ..className = 'change-comment-size-icon'
        ..id = '$sanitizedName-change-comment-size-icon-$lineNo';

      var minimizeButton = ButtonElement()
        ..append(minimizeIcon)
        ..id = '$sanitizedName-minimize-button-$lineNo'
        ..className = 'change-comment-size-button minimize'
        ..onClick.listen((event) {
          interactionCallback(event);
          var parentDiv = postedCommentsSection.parent;
          parentDiv.style.display = 'none';
          var grandparent = parentDiv.parent;
          grandparent.insertBefore(
              getMaximizeElement(parentDiv, lineNo, nbrOfComments), parentDiv);
        });

      postedCommentsSection.append(minimizeButton);
      for (var comment in comments) {
        var userinfo = DivElement()
          ..className = 'posted-comment-user-info'
          ..id = '$sanitizedName-comment-userinfo-${comment.id}';
        var name = comment.user;

        var userIcon = ImageElement()
          ..src = getImagePathForUser(name)
          ..width = 50
          ..height = 50
          ..style.margin = '5px'
          ..className = 'profile-picture'
          ..id = '$sanitizedName-comment-usericon-${comment.id}';
        var userName = DivElement()
          ..text = '${comment.user}'
          ..id = '$sanitizedName-comment-username-${comment.id}';
        userinfo
          ..append(userIcon)
          ..append(userName);
        var image = ImageElement();
        var containsImage = false;

        if (markdownToHtml(comment.body).contains('<img src="')) {
          //checks if the comment has image ref
          var link = comment.body
              .split('(')
              .firstWhere((s) => s.contains('https:'), orElse: () => null);
          var ref = link.split(')')[0];
          image.src =
              ref; //since dart removes the image with markdown to html we need to manually do this :/
          containsImage = true;
        }
        var commentContentDiv = DivElement()
          ..className = 'posted-comment-content'
          ..id = '$sanitizedName-comment-content-div-${comment.id}'
          ..appendHtml(markdownToHtml(comment.body));
        if (containsImage) {
          commentContentDiv.children[0].children
              .removeWhere((element) => element.tagName == 'IMG');
          commentContentDiv.append(image);
        }
        var commentDiv = DivElement()
          ..className = 'posted-comment-div'
          ..id = '$sanitizedName-comment-div-${comment.id}'
          ..append(userinfo)
          ..append(commentContentDiv);
        postedCommentsSection.append(commentDiv);
      }
    }

    return postedCommentsSection;
  }

  DivElement generateAddContentArea(int lineNo, String pullReqNumber,
      String fileName, bool includeCancelButton) {
    var addCommentsSection = DivElement()
      ..id = '$sanitizedName-comment-div-$lineNo-add-comments'
      ..className = 'comment-div-add-section';

    var writeCommentsSection = DivElement()
      ..id = '$sanitizedName-comment-div-$lineNo-write-comments'
      ..className = 'comment-div-write-section';

    var textArea = TextInputElement()
      ..id = '$sanitizedName-comment-div-$lineNo-text-add-comments'
      ..className = 'text-area-write-comment';
    textArea.onInput.listen((event) {
      interactionCallback(event, textArea.value);
    });

    var btnArea = DivElement()
      ..id = '$sanitizedName-comment-div-$lineNo-btn-div'
      ..className = 'comment-div-btn-section';
    var submitBtn = ButtonElement()
      ..id = '$sanitizedName-comment-div-$lineNo-submit-button'
      ..className = 'button submit-comment'
      ..text = 'Submit'
      ..onClick.listen((e) {
        sendPostComment(e, textArea, pullReqNumber, lineNo, fileName);
        querySelector('#$sanitizedName-comment-div-$lineNo-cancel-button')
            ?.remove();
      });

    var currentPre = querySelector('#$sanitizedName-target-l$lineNo');
    var profileImage = querySelector('.profile-picture');
    var src = UserInfo.imagePath;
    if (profileImage != null) {
      src = (profileImage as ImageElement).src;
    }

    var userProfile = ImageElement()
      ..src = src
      ..height = 50
      ..width = 50
      ..className = 'profile-picture'
      ..id = '$sanitizedName-${UserInfo.username}-comment-$lineNo';
    writeCommentsSection.append(userProfile);
    writeCommentsSection.append(textArea);
    addCommentsSection.append(writeCommentsSection);
    btnArea.append(submitBtn);

    if (includeCancelButton) {
      var cancelBtn = ButtonElement()
        ..id = '$sanitizedName-comment-div-$lineNo-cancel-button'
        ..className = 'cancel-button cancel-comment'
        ..text = 'Cancel'
        ..onClick.listen((event) {
          interactionCallback(event);
          textArea.value = '';
          toggleCommentSection(event, currentPre);
        });
      btnArea.append(cancelBtn);
    }

    addCommentsSection.append(btnArea);

    return addCommentsSection;
  }

  ///Updates the comment area in the Overview view. Appends a DivElement containing [newComment]
  void updateOverviewCommentContent(Comment newComment) {
    var commentDiv = DivElement();
    var row = DivElement()..className = 'row';
    var fileCol = DivElement()
      ..className = 'col'
      ..text = 'In file ${newComment.filePath}'
      ..id = 'comment-container-file-column';
    var lineCol = DivElement()
      ..className = 'col'
      ..text = 'Line ${newComment.line}'
      ..id = 'comment-container-line-column';
    var contentCol = AnchorElement()
      ..className = 'col'
      ..innerHtml = markdownToHtml(newComment.body)
      ..id = 'comment-container-content-column'
      ..href = '#$sanitizedName-comment-div-${newComment.line}-posted-comments';
    row.append(fileCol);
    row.append(lineCol);
    row.append(contentCol);

    commentDiv.append(row);
    //querySelector('#comment-section-div-card').append(commentDiv);
  }

  /// Sends the posted comment to Annotation Service, to be stored.
  void sendPostComment(Event e, TextInputElement commentBox,
      String pullReqNumber, int lineNo, String fileName) async {
    var comment = commentBox.value;

    if (comment.trim() == '') {
      return;
    }

    var newComment = Comment(comment, fileName, lineNo)
      ..createdAt = DateTime.now()
      ..updatedAt = DateTime.now()
      ..user = UserInfo.username
      ..id = Random().nextInt(1000000000).toString();

    updateOverviewCommentContent(newComment);

    var comments =
        commentColl.comments.where((element) => element.filePath == fileName);
    var commentsInLine = comments.where((element) => element.line == lineNo);
    if (commentsInLine.isNotEmpty) {
      newComment
        ..is_a_reply = true
        ..reply_to_id = commentsInLine.last.id;
    } else {
      newComment.is_a_reply = false;
    }

    //Send interaction and content of comment field
    interactionCallback(e, newComment.body);

    var res = await postCommentCallback(pullReqNumber, newComment.toJson());
    if (res == 'OK') {
      commentBox.value = '';
      createNewShownComment(newComment, lineNo);
    }
  }

  /// Adds the newly posted comment to the comment dialogue, that shows that the comment has been added.
  void createNewShownComment(Comment comment, int lineNo) {
    var postedCommentsSection =
        querySelector('#$sanitizedName-comment-div-$lineNo-posted-comments');

    var profileImage = querySelector('.profile-picture');
    var src = UserInfo.imagePath;
    if (profileImage != null) {
      src = (profileImage as ImageElement).src;
    }

    var userinfo = DivElement()
      ..className = 'posted-comment-user-info'
      ..id =
          '$sanitizedName-${UserInfo.username}-comment-posted-userinfo-$lineNo';
    var userIcon = ImageElement()
      ..src = src
      ..width = 50
      ..height = 50
      ..className = 'profile-picture'
      ..id =
          '$sanitizedName-${UserInfo.username}-comment-posted-usericon-$lineNo'
      ..style.margin = '5px';
    var userName = DivElement()..text = '${comment.user}';
    userinfo
      ..append(userIcon)
      ..append(userName);
    var commentContentDiv = DivElement()
      ..className = 'posted-comment-content'
      ..id =
          '$sanitizedName-${UserInfo.username}-comment-posted-content-div-$lineNo'
      ..appendHtml(markdownToHtml(comment.body));
    var commentDiv = DivElement()
      ..className = 'posted-comment-div'
      ..id =
          '$sanitizedName-${UserInfo.username}-comment-posted-comment-div-$lineNo'
      ..append(userinfo)
      ..append(commentContentDiv);
    postedCommentsSection.append(commentDiv);
    if (lineNo != -1) {
      var codeLinePre = querySelector('#$sanitizedName-target-l$lineNo');
      var codeLine;
      if (codeLinePre.children.length > 1) {
        codeLine = codeLinePre.children[1].className == 'lineCode'
            ? codeLinePre.children[1]
            : codeLinePre;
      } else {
        codeLine = codeLinePre;
      }
      codeLine.style.borderBottomStyle = 'solid';
      codeLine.style.borderLeftStyle = 'solid';
      codeLine.style.borderRightStyle = 'solid';
    }
  }

  /// Used to toggle the comment section when pressing the icon next to the line. It shows or hides the comment dialogue.
  void toggleCommentSection(Event event, Element target) {
    commentBubbleSaved = null;
    interactionCallback(event);
    var parent = target.parent;
    var fileName = parent.parent.parent.parent.id.split('-')[2];
    var santitized = sanitizeFileName(fileName);

    var commentLineID = target.id.split('-');
    if (commentLineID.contains('whitespace')) {
      return;
    }

    var id = int.parse(commentLineID[2].substring(1));

    //Checks if there exists a comment from before
    var el = querySelector('#$santitized-comment-div-$id');
    if (el == null) {
      var includeCancelButton = true;
      el = generateCommentSection(
          id, commentColl.number, fileName, includeCancelButton);
      parent.append(el);
      event.preventDefault();
      querySelector('#$fileName-comment-div-$id-text-add-comments').focus();
    } else {
      el = querySelector('#$santitized-comment-div-$id');

      if (el.style.display == 'none') {
        el.style.display = '';
        /**<input type="text" id="srcBirdjava-comment-div-35-text-add-comments" class="text-area-write-comment"> */
        event.preventDefault();
        querySelector('#$fileName-comment-div-$id-text-add-comments').focus();
      } else {
        el.style.display = 'none';
      }
    }
  }

  void toggleCommentSectionReplay(CustomEvent event) {
    commentBubbleSaved = null;

    var target = querySelector('#' + Css.escape(event.detail));
    if (target == null) {
      return;
    }
    var parent = target.parent;
    var fileName = parent.parent.parent.parent.id.split('-')[2];
    var santitized = sanitizeFileName(fileName);

    var commentLineID = target.id.split('-');
    if (commentLineID.contains('whitespace')) {
      return;
    }

    var id = int.parse(commentLineID[2].substring(1));

    //Checks if there exists a comment from before
    var el = querySelector('#$santitized-comment-div-$id');
    if (el == null) {
      var includeCancelButton = true;
      el = generateCommentSection(
          id, commentColl.number, fileName, includeCancelButton);
      parent.append(el);
      event.preventDefault();
      querySelector('#$fileName-comment-div-$id-text-add-comments').focus();
    } else {
      el = querySelector('#$santitized-comment-div-$id');

      if (el.style.display == 'none') {
        el.style.display = '';
        /**<input type="text" id="srcBirdjava-comment-div-35-text-add-comments" class="text-area-write-comment"> */
        event.preventDefault();
        querySelector('#$fileName-comment-div-$id-text-add-comments').focus();
      } else {
        el.style.display = 'none';
      }
    }
  }

  Future<DivElement> getViewDiv() async {
    return await generateContentAreas();
  }

  ///// Generates the content areas for the file. It creates the code view and the comment view.
  //Future<DivElement> getView(DivElement div) async {
  //  var fileObjectDiv = await generateContentAreas();
  //  var foundFile;
  //  foundFile ??= commentColl.comments.firstWhere(
  //    (element) =>
  //        sanitizeFileName(element.filePath) ==
  //        sanitizeFileName(comp.filename), orElse: () => null);
  //  if (foundFile != null) {
  //    addCommentForFile(fileObjectDiv);
  //  }
  //  div.append(fileObjectDiv);
  //  generateCommentCollectionComments(fileObjectDiv);
  //
  //  return div;
  //}

  ///Generates the comments found in commentColl.comments for the file.
  void generateCommentCollectionComments(DivElement fileObjectDiv) {
    for (var comment in commentColl.comments) {
      //if comment is on the same file
      if (sanitizeFileName(comment.filePath) ==
          sanitizeFileName(comp.filename)) {
        //if comment is not a reply, add it as a new comment thread.
        if (!comment.is_a_reply) {
          var commentLine = comment.line;
          var codeLine = fileObjectDiv.querySelector(
              '#${sanitizeFileName(comp.filename)}-target-l$commentLine');
          //If comment is assigned a line we want to show it on this line
          //If commentline = -1 it is a comment on the file or PR as a whole
          if (commentLine != -1) {
            codeLine.style.borderBottomStyle = 'solid';
            codeLine.style.borderLeftStyle = 'solid';
            codeLine.style.borderRightStyle = 'solid';
            var commentPostLocation;
            var click = MouseEvent('click');
            if (codeLine.className.contains('button-')) {
              var ind = codeLine.className.lastIndexOf('button-');
              commentPostLocation = codeLine.className.substring(ind + 7);
            }

            fileObjectDiv
                .querySelector(
                    '#show-more-btn-${sanitizeFileName(comp.filename)}-$commentPostLocation')
                ?.dispatchEvent(click);
            //Old version of triggering show more buttons, keep until new version is tested.
            // if (codeLine.className.contains('button-top')) {
            //   var click = MouseEvent('click');
            //   fileObjectDiv
            //       .querySelector(
            //           '#show-more-btn-${sanitizeFileName(comp.filename)}-top')
            //       .dispatchEvent(click);
            // } else if (codeLine.className.contains('button-bottom')) {
            //   var click = MouseEvent('click');
            //   fileObjectDiv
            //       .querySelector(
            //           '#show-more-btn-${sanitizeFileName(comp.filename)}-bottom')
            //       .dispatchEvent(click);
            // }
          }
        }
      } //#show-more-btn-srcBirdjava-button-bottom-18
    }
  }
}
