/// TODO DOCUMENTATION
import 'dart:collection';
import 'dart:html';

import 'compare/compare_model.dart';

class PatchHandler {
  static Queue<int> createSplitPointQueue(List<Patch> patchList, {bool isSrc}) {
    //Creates a queue of the line numbers of the start and end of the patches for a file.
    var splitPoint = Queue<int>();
    for (var i = 0; i < patchList.length; i++) {
      var startOfPatch = -1;
      var endOfPatch = -1;
      if (isSrc) {
        startOfPatch = patchList[i].startSrcLine;
        endOfPatch = startOfPatch + patchList[i].srcLinesLength;
      } else {
        startOfPatch = patchList[i].startTargetLine;
        endOfPatch = startOfPatch + patchList[i].targetLinesLength;
      }

      splitPoint.add(startOfPatch);
      splitPoint.add(endOfPatch);
    }
    return splitPoint;
  }

  static Queue<int> createSplitPointQueuePR(List<Patch> patchList,
      {bool isSrc}) {
    //Creates a queue of the line numbers of the start and end of the patches for a file.
    var splitPoint = Queue<int>();
    // splitPoint.addAll(com);
    for (var i = 0; i < patchList.length; i++) {
      var startOfPatch = -1;

      if (isSrc) {
        startOfPatch = patchList[i].startSrcLine;
      } else {
        startOfPatch = patchList[i].startTargetLine;
      }

      splitPoint.add(startOfPatch);
    }
    return splitPoint;
  }
}

class PatchDivWrapper {
  DivElement div;
  bool isPatch;
  PatchDivWrapper(this.div, this.isPatch) {
    div.className = 'patch-div system-generated box';
  }
}
