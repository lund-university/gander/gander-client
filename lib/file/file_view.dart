/// TODO DOCUMENTATION
import 'dart:collection';
import 'dart:html';
import '../aoi/ast_converter.dart';
import 'compare/compare_model.dart';
import '../utils/help_functions.dart';
import 'file_model.dart';
import 'patch_handler.dart';
import '../comments/annotation_model.dart';
import '../utils/user_info.dart';
import '../aoi/aoi_base.dart';

import '../aoi/tokenizer.dart';

class FileView {
  FileCollectionModel fileCollModel;
  CommentCollection commentColl;
  Comparison comp;
  String targetContent;
  Element targetPre;
  Element srcPre;
  TargetType targetType = TargetType();
  SrcType srcType = SrcType();
  String name;
  AOIHandlerBase aoi;

  ///The file name without characters that are not allowed in a html id or html class.
  String sanitizedName;
  DivElement fileObject;
  ASTInformation srcAST = ASTInformation();
  ASTInformation targetAST = ASTInformation();
  ASTFile astFile;
  Function interactionCallback;
  Function postCommentCallback;
  DivElement fileContent = DivElement()..id = 'file-content';
  bool commentsAreEnabled;
  Map<String, List<Def>> targetUseDefmap;
  Map<String, List<Def>> srcUseDefmap;

  //Used to keep track of the html id for items of the class row-relatvie
  int rowRelativCounter = 0;
  //Used to keep track of the html id for the whitespaces
  int whitespaceCounter = 0;

  FileView(this.comp, this.commentColl, this.targetContent, this.fileCollModel,
      this.interactionCallback, this.postCommentCallback, this.aoi,
      {this.commentsAreEnabled}) {
    name = comp.filename;
    sanitizedName = sanitizeFileName(name);
  }

  /// Returns a version of [name] without any characters that are not allowed in a html id or html class.
  String sanitizeFileName(String name) {
    return name
        .replaceAll('.', '')
        .replaceAll('/', '')
        .replaceAll('-', '')
        .replaceAll('_', '');
  }

  /// Creates and returns the DivElement that will contain the content.
  DivElement getFileObject() {
    var add = comp.additions;
    var del = comp.deletions;
    var status = comp.status;

    var fileObject = DivElement()
      ..className = 'file-content-box'
      ..id = 'file-object-$sanitizedName';
    var fileLabel = LabelElement()
      ..id = 'file-label-$name'
      ..text = '$name: $add additions $del deletions    status: $status';

    var labelElement = DivElement()
      ..className = 'file-label'
      ..id = 'file-div-label-id-$name';
    labelElement.append(fileLabel);
    fileObject.append(labelElement);
    fileObject.append(fileContent);
    return fileObject;
  }

  /// Returns a pre element consisting of one span for each char in [line].
  Element generateLine(int lineNbr, String type, String line) {
    var lineCode = Element.span();

    var lineNum = Element.span();
    lineNum.text = lineNbr.toString();
    lineNum.id = '$sanitizedName-$type-line-$lineNbr';
    lineNum.className = 'lineNbr';
    //lineCode.children.add(lineNum);

    var colNbr = 0;

    //Replace all \r with spaces to prevent empty spans
    line = line.replaceAll('\r', ' ');
    var spanString =
        Tokenizer.generateSpan(line, name, sanitizedName, type, lineNbr);
    //checks for which parts of the generated span that has highlighting, and we want to keep, the rest we
    //generate span by span to keep the highlighting and be able to use AOIs
    var map = Map<int, List<String>>();
    RegExp exp = RegExp(
        r'<span class=[^<>]*><span id=[a-zA-Z]*-[a-z]*-(l[0-9]*:[0-9]*)>([^<>]*)</span></span>');
    //RegExp exp2 =
    //    RegExp(r'<span id=[a-zA-Z]*-[a-z]*-(l[0-9]*:[0-9]*)>[^<>]*</span>');
    var m = exp.allMatches(spanString);
    if (m != null) {
      for (Match match in m) {
        //makes a key-value pair of the generated span and word and its starting column number
        //the word, or match.group(2) is needed to correctly update colNbr
        map[int.parse(match.group(1).split(':')[1])] = [
          match.group(0),
          match.group(2)
        ];
      }
    }
    for (var i = 0; i < line.length; i++) {
      var ch = line[i];
      colNbr++;
      if (map.containsKey(colNbr)) {
        //lineCode.children.add(lineNum);
        var span = Element.html(map[colNbr][0]);
        lineCode.children.add(span);
        //print(map[colNbr]);
        i += map[colNbr][1].length - 1;
        colNbr += map[colNbr][1].length - 1;
      } else {
        var colTag = Element.span();
        colTag.text = ch;
        colTag.id = '$sanitizedName-$type-l$lineNbr:$colNbr';
        lineCode.children.add(colTag);
      }
    }

    if (!lineCode.hasChildNodes()) {
      // If lineCode has no children, it's an empty line
      lineCode.text = '\n';
    }
    (type == 'target')
        ? targetAST.content.write(line + '\n')
        : srcAST.content.write(line + '\n');

    var lines = Element.pre();
    lines.id = '$sanitizedName-$type-l${lineNbr.toString()}';
    lineCode.className = 'lineCode';
    lines.append(lineNum);
    lines.append(lineCode);
    return lines;
  }

  /// Returns True if diff is out of range for the current column
  /// weird with the -2, don't know why it works, maybe look into
  static bool currentDiffIsOutOfRange(Diff diff, int column) {
    if (column - 2 >= diff.endColumn) {
      return true;
    }
    return false;
  }

  /// Returns True if diff is in range for the current column.
  /// Weird with the -1, don't udnerstand why it works but it does
  static bool elementIsInRange(Diff diff, int column) {
    var startColumn = diff.startColumn;

    var endColumn = diff.endColumn;

    if (column - 1 >= startColumn && column <= endColumn) {
      return true;
    }
    return false;
  }

  /// Applies a darker color to all spans that are in range of [nbrCharsFromStart].
  /// If a char has a darker color it means that char was changed between the src and the target file.
  void applyLessTransparency(Element element, int nbrCharsFromStart,
      List<Diff> diffList, String codeType) {
    var childList = element.children;
    var diff = diffList[0];

    for (var child in childList) {
      if (currentDiffIsOutOfRange(diff, nbrCharsFromStart)) {
        diffList.removeAt(0);
        if (diffList.isEmpty) {
          return;
        } else {
          diff = diffList[0];
        }
      }
      if (elementIsInRange(diff, nbrCharsFromStart)) {
        if (codeType == 'src') {
          child.classes.add('diff-deletion');
        } else {
          child.classes.add('diff-addition');
        }
      }

      nbrCharsFromStart++;
    }
  }

  /// Uses the patches and the content of the target file to generate
  /// a src file and a target file in change view with highlighting showing what was changed.
  void addLinesFromPatches() {
    var targetContentList = targetContent.split('\n');
    targetContentList = removeTralingEmptyString(targetContentList);

    // A copy of the patch list is made since items form the list will be removed
    var patchList = List<Patch>.from(comp.patchList);

    var targetSplitPointQueue =
        PatchHandler.createSplitPointQueuePR(patchList, isSrc: false);

    var targetSplitPoint = targetSplitPointQueue.removeFirst();
    var hideArea = false;
    var buttonPos = 'top';
    var targetIndex = LineIndex(); //Create a mutable targetIndex

    while (targetIndex.currentLine < targetContentList.length + 1) {
      var line = targetContentList[targetIndex.currentLine - 1];
      //The lists has to be indexed like this due to the LineIndex class being initiated as 1
      //It looks weird, but changing this would require a lot of precaussions and could cause bugs.
      if (targetIndex.currentLine != targetSplitPoint) {
        hideArea = true;

        var targetLine = generateLine(targetIndex.currentLine, 'target', line)
          ..className = '$sanitizedName-button-$buttonPos'
          ..style.display = 'none';
        targetPre = targetLine;

        applyLastCodeSection();
        targetIndex.increase();
      } else {
        if (hideArea) {
          addShowMoreButton(buttonPos);
          buttonPos = 'bottom-${targetIndex.currentLine}';
          hideArea = false;
        }
        var oldValue;
        var onlySrc = false;

        if (targetIndex.currentLine != targetSplitPoint) {
          onlySrc = true;
          oldValue = targetIndex.currentLine;
        }

        targetSplitPoint = getNextSplitPoint(targetSplitPointQueue);
        applyPatches(targetIndex, patchList);
        patchList.removeAt(0);
      }
    }
    if (hideArea) {
      addShowMoreButton(buttonPos);
    }
  }

  /// Returns a list of strings with the last string deleted, to handle trailing empty Strings.
  List<String> removeTralingEmptyString(List<String> splitList) {
    //This is to simlpe, not necessary to do seperatly. Remove this method.
    // When a string is split and ends with the '\n', the last item in the split list will be an empty string
    if (splitList.isEmpty) {
      return splitList;
    }
    splitList.removeLast();
    return splitList;
  }

  /// Adds a show more-button to the fileContent
  void addShowMoreButton(String buttonPos) {
    var showMoreIcon = ImageElement()
      ..src = './showMore1.png'
      ..height = 22
      ..width = 22;

    var showMoreButton = ButtonElement()
      ..id = 'show-more-btn-$sanitizedName-$buttonPos'
      ..text = 'Show more'
      ..style.backgroundColor = 'LightGray'
      ..style.borderStyle = 'solid'
      ..style.borderColor = 'black'
      ..className = 'show-more-$sanitizedName-button-$buttonPos';

    showMoreButton.append(showMoreIcon);

    showMoreButton.onClick
        .listen((event) => handleShowMore(event, buttonPos, showMoreButton));
    fileContent.append(showMoreButton);
  }

  /// The action that occurs after the showMore button is pressed.
  /// This will change the display from 'none' to 'block' for the elements
  /// that have the same className as the button.
  void handleShowMore(
      MouseEvent e, String buttonPos, ButtonElement showMoreButton) {
    interactionCallback(e);
    var className = '.$sanitizedName-button-$buttonPos';
    var elemList = fileContent.querySelectorAll(className);
    for (var elem in elemList) {
      elem.style.display = 'block';
    }
    showMoreButton.style.display = 'none';
    aoi.moveAOIs(showMoreButton.className);
  }

  /// Add the latest code section to the [fileContent] object.
  void applyLastCodeSection() {
    rowRelativCounter++;

    var lineNumTarget = Element.span();
    lineNumTarget.text = rowRelativCounter.toString();
    lineNumTarget.id = '$sanitizedName-target-line-$rowRelativCounter';

    var counterString = rowRelativCounter.toString();
    var cardRow = DivElement()
      ..className = 'row relative'
      ..id = '$sanitizedName-row-relative-$counterString';

    var targetCol = DivElement()
      ..className = 'col right'
      ..id = '${targetPre.id}-col-right';

    targetCol.append(targetPre);
    cardRow.append(targetCol);
    fileContent.append(cardRow);
  }

  ///Use the patches in [patchList] to generate the src and target file that
  ///is displayed in the change view.
  void applyPatches(LineIndex targetIndex, List<Patch> patchList) {
    var codeList = patchList[0].codeList;
    var targetList = <Element>[];

    for (var n = 0; n < codeList.length; n++) {
      var codePart = codeList[n];
      var startsWith = codePart.startsWith;
      var contentList = codePart.content.split('\n');
      //Remove the last item of the list (a trailing empty string)
      contentList = removeTralingEmptyString(contentList);

      if (startsWith == ' ') {
        //Since we only want to show single version of the code, we might just need to combine this if-stmt with the next one.
        addBothSidesPatch(targetList);
        targetList = <Element>[];

        for (var lineString in contentList) {
          var targetLine =
              generateLine(targetIndex.currentLine, 'target', lineString);
          targetPre = targetLine;
          applyLastCodeSection();
          targetIndex.increase();
        }
      } else if (startsWith == '+') {
        //Add the patch to the target
        addPatchToOneSide(contentList, targetIndex, targetType, targetList);
      } else if (startsWith == '-') {
        //If we want to show src files sometime??
      }
    }
    addBothSidesPatch(targetList);
  }

  ///Apply the content from the patch to both the src and target file.
  //Removed src from this function
  void addBothSidesPatch(List<Element> targetList) {
    for (var i = 0; i < targetList.length; i++) {
      //target list and srcList contains elements
      var targetLine = targetList[i];
      targetPre = targetLine;

      applyLastCodeSection();
    }
  }

  ///Add the content of the patch to either only the src file or the target file
  void addPatchToOneSide(
    List<String> contentList,
    LineIndex index,
    FileType fileType,
    List<Element> list, //target
  ) {
    for (var i = 0; i < contentList.length; i++) {
      var lineElem =
          generateLine(index.currentLine, fileType.type, contentList[i]);

      list.add(lineElem);

      index.increase();
    }
  }

  ///Returns the index of the next split point (the index of the next start of a patch)
  int getNextSplitPoint(Queue<int> splitPointQueue) {
    if (splitPointQueue.isNotEmpty) {
      return splitPointQueue.removeFirst();
    }
    return -1;
  }

  /// Retuns a DivElement containing the ChangeView for the current file.
  Future<DivElement> generateContentAreas() async {
    fileObject = getFileObject();
    if (comp.patchList != null) {
      addLinesFromPatches();
      //The following will have to be omitted until compiler service is okay again.
      if (comp.isParsable) {
        //targetAST.collection = await fileCollModel.getAndSaveASTInformation(
        //    '0:0', targetAST.content.toString(), sanitizedName, 'target');
        await fileCollModel.getAndSaveASTInformation(
            '0:0', targetAST.content.toString(), name, sanitizedName, 'target');
      }
    }

    return fileObject;
  }

  /// Returns a empty pre element that can be used to align the code in src and the target.
  Element generateWhiteSpace() {
    whitespaceCounter++;
    var whitespaceCounterString = whitespaceCounter.toString();

    return Element.pre()
      ..text = '\n'
      ..id = '$sanitizedName-whitespace-$whitespaceCounterString';
  }

  /// Returns an image path for the username [name]. If the user already has an image,
  /// the path to that image is returned. Otherwise, the user is given an available image.
  String getImagePathForUser(String name) {
    var userMap = UserInfo.usernameToImagePath;
    if (userMap.containsKey(name)) {
      return userMap[name];
    }
    var imagePathList = UserInfo.imagePathList;
    if (imagePathList.isNotEmpty) {
      var imagePath = imagePathList.removeAt(0);
      userMap[name] = imagePath;
      UserInfo.imagePathList = imagePathList;
      return imagePath;
    }
    // If there is no available images, the dart logo is used as default.
    return './favicon.ico';
  }

  Future<DivElement> getViewDiv() async {
    return await generateContentAreas();
  }
}
