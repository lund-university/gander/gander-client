import 'dart:async';
import 'dart:html';
import 'dart:convert';

import '../aoi/ast_converter.dart';
import '../gander_package.dart';
import '../service_models/gander_service_model.dart';
import 'package:collection/collection.dart';
import 'package:path/path.dart' as p;

/// A model for handling the retrieval of files from pur service.
class FileCollectionModel {
  List<PurFile> fileModels = [];
  List<PurFolder> folderModels = [];
  PurFile currentFile;
  Map<String, List<Def>> useDefMap = {};
  Map<String, Map<String, dynamic>> astMap = {};
  ASTCollection astCollection = ASTCollection();
  dynamic compilationUnitAST;

  static Set<String> supportedExtensions;

  static const String _HOST = 'http://localhost';
  static const String _PURPATH = '/purs';
  static const String _GITPATH = '/pur/git';
  static const String _CONTENTPATH = '/pur';
  static const String _EDITPATH = '/pur/';
  static const String _COMPILERPATH = '/compiler';
  static const String _PARSERPATH = '/parser';
  static const String _PARSEFILEPATH= '/parse';
  static const String _SUPPORTED_EXTENSIONS_PATH = '/supportedExtensions';
  static const String _ASTPATH = '/ast';
  static const String _USEPATH = '/use';

  static const int _PORT = 8001;

  /// Retrieves the file with the name [name]. Only checks the client's local storage.
  /// If no file is found, it returns null.
  PurFile getFile(String name) {
    return fileModels.firstWhere((file) => file.name == name,
        orElse: () => null);
  }

  /// Retrieves the folder with the name [name]. Only checks the client's local storage.
  /// If no folder is found, it returns null.
  PurFolder getFolder(String name) {
    return folderModels.firstWhere((folder) => folder.name == name,
        orElse: () => null);
  }

  /// Returns the collection of files and folders in a FileCollection-object.
  FileCollection getCollection() {
    return FileCollection(fileModels, folderModels);
  }

  ///returns the map of useDef relationships for latest file sent.
  Map<String, List<Def>> getUseDefMap() {
    //var map = useDefMap;
    //useDefMap = {};
    return useDefMap;
  }

  /// Constructor for FileCollectionModel.
  FileCollectionModel();

  static Future<void> fetchSupportedExtensions() async {
    final url = '$_HOST:$_PORT$_PARSERPATH$_SUPPORTED_EXTENSIONS_PATH';
    try {
      var result = await GanderServiceModel.getRequest(url);
      List<dynamic> extensionsList = json.decode(result);
      supportedExtensions = extensionsList.map((e) => e.toString()).toSet();
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_PARSERPATH$_SUPPORTED_EXTENSIONS_PATH");
      print(e.toString());
      rethrow;
    }
  }

  static bool isExtensionSupported(String extension) {
    if (supportedExtensions == null) {
      throw Exception('Supported extensions not fetched yet');
    }

    return supportedExtensions.contains(extension);
  }

  /// Send a file to the parser service and receive the AST for the file with the content [content].
  Future<void> postFileToParser(
      String content, String fileName, String sanitizedName, String fileType) async {
    try {
      var extension = p.extension(fileName);
      
      var data = {
        'filename': sanitizedName + '-' + fileType,
        'content': content,
        'extension': extension,
        'full': false
        };

      var result = await GanderServiceModel.sendRequestwithRespons(
          '$_HOST:$_PORT$_PARSERPATH$_PARSEFILEPATH',
          method: 'POST',
          sendData: json.encode(data),
          fileName: '$sanitizedName-$fileType',
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});

      data['full'] = true;

      unawaited(GanderServiceModel.sendRequestwithRespons(
          '$_HOST:$_PORT$_PARSERPATH$_PARSEFILEPATH',
          method: 'POST',
          sendData: json.encode(data),
          fileName: '$sanitizedName-$fileType',
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'}));
      _processParserResponse(result, sanitizedName, fileType);
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_PARSERPATH$_PARSEFILEPATH");
      print(e.toString());
      rethrow;
    }
  }

  void _processParserResponse(String result, String fileName, String fileType) {
    try {
      Map<String, dynamic> content = json.decode(result);
      astMap.update('$fileName-$fileType', (value) => content,
          ifAbsent: () => content);
    } catch (e) {
      print('Error with processing the request, ${e.toString()}');
      rethrow;
    }
  }

  Map<String, Map<String, dynamic>> getAstMap() {
    return astMap;
  }

  /// Send a file to the compiler API to receive the use def relationships for the file with the content [content].
  Future<void> postFileToUseDefCheck(
      String content, String fileName, String fileType) async {
    var data = {'file': content};
    try {
      var result = await GanderServiceModel.sendRequestwithRespons(
          '$_HOST:$_PORT$_COMPILERPATH$_USEPATH',
          method: 'POST',
          sendData: json.encode(data),
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
      _processUseResponse(result, fileName, fileType);
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_COMPILERPATH$_USEPATH");
      print(e.toString());
      rethrow;
    }
  }

  Def _extractDef(dynamic jsonData) {
    var location =
        Location(jsonData['start'].toString(), jsonData['end'].toString());

    var def = Def(jsonData['name'], location, jsonData['isVar']);
    var varUseList = jsonData['uses'];
    varUseList.forEach((item) {
      var location = Location(item['start'], item['end']);
      var use = InstanceUse(location);
      def.addUse(use);
    });
    return def;
  }

  /// Takes the response [result] from the server and adds the methods and
  /// variables to the client side storage.
  void _processUseResponse(String result, String fileName, String fileType) {
    try {
      Map<String, dynamic> content = json.decode(result);
      var varList = content['variables'];
      varList.forEach((variable) {
        var def = _extractDef(variable);
        useDefMap.update('$fileName-$fileType', (value) => value + [def],
            ifAbsent: () => [def]);
      });

      var methodList = content['methods'];
      methodList.forEach((method) {
        var def = _extractDef(method);
        useDefMap.update('$fileName-$fileType', (value) => value + [def],
            ifAbsent: () => [def]);
      });

      var paramList = content['parameters'];
      paramList.forEach((variable) {
        var def = _extractDef(variable);

        useDefMap.update('$fileName-$fileType', (value) => value + [def],
            ifAbsent: () => [def]);
      });
    } catch (e) {
      print('Error with processing the request, ${e.toString()}');
      rethrow;
    }
  }

  /// Retrieves the files stored in Pur Service in the specific repo on the master branch.
  Future<FileCollection> getFileNames() async {
    fileModels = [];
    folderModels = [];
    try {
      var result =
          await GanderServiceModel.getRequest('$_HOST:$_PORT$_GITPATH');
      return await _handleRequestComplete(result);
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_GITPATH: " + e.toString());
      rethrow;
    }
  }

  /// Retrieves the content of the file with filename [name] from git.
  Future<String> getContent(String name) async {
    var encodedName = Uri.encodeComponent(name);
    try {
      var result = await GanderServiceModel.getRequest(
          '$_HOST:$_PORT$_CONTENTPATH/$encodedName');
      var encodedContent = _handleContentRequestComplete(result);
      var content = GanderUtils.decodeContent(encodedContent);
      var fetchedFile = getFile(name);
      fetchedFile.content = content;
      return content;
    } catch (e) {
      print("Couldn't open $_HOST$_PORT$_PURPATH");
      rethrow;
    }
  }

  /// Retrieves the content of the folder with foldername [name] from git.
  Future<List<dynamic>> getFolderContent(String name) async {
    var encodedName = Uri.encodeComponent(name);
    try {
      var result = await GanderServiceModel.getRequest(
          '$_HOST:$_PORT$_CONTENTPATH/$encodedName');
      var content = await _handleFolderContentRequestComplete(result);

      return content;
    } catch (e) {
      print("Couldn't open $_HOST$_PORT$_PURPATH");
      rethrow;
    }
  }

  /// Retrieves the content of the file with filename [name] up until the date [date] from git.
  Future<String> getLatestContentUntilDate(String name, String date) async {
    var encodedName = Uri.encodeComponent(name);
    try {
      var result = await GanderServiceModel.getRequest(
          '$_HOST:$_PORT$_GITPATH/$encodedName/$date');
      var content = _handleContentRequestComplete(result);
      return GanderUtils.decodeContent(content);
    } catch (e) {
      print("Couldn't open $_HOST$_PORT$_PURPATH");
      rethrow;
    }
  }

  /// Posts the new content [content] to the current file and stores it on the pur service.
  Future<void> updateContent(Event _, content) async {
    if (currentFile.name != null) {
      try {
        var data = {'content': content};
        await GanderServiceModel.sendRequest(
            '$_HOST:$_PORT$_EDITPATH' + currentFile.name,
            method: 'PUT',
            sendData: json.encode(data),
            requestHeaders: {
              'Content-Type': 'application/json; charset=UTF-8'
            });
      } catch (e) {
        print("Couldn't open $_HOST$_PORT$_EDITPATH" + currentFile.name);
      }
    }
  }

  /// Takes the response [response] from the server and returns the content of the file.
  String _handleContentRequestComplete(String response) {
    Map<String, dynamic> responseText = json.decode(response);
    var content = responseText['content'];
    return content;
  }

  /// Takes the response [response] from the server and returns the content of the folder as a list of PurContent, i.e. PurFiles and PurFolder objects.
  List<dynamic> _handleFolderContentRequestComplete(String response) {
    var responseText = json.decode(response);
    var folderName = responseText['name'];
    var folder = getFolder(folderName);
    var newContainedFiles = <PurFile>[];
    var newContainedFolders = <PurFolder>[];
    for (var file in responseText['files']) {
      if (file['type'] == 'file') {
        var newFile = PurFile(file['name']);
        newContainedFiles.add(newFile);
        fileModels.add(newFile);
      } else if (file['type'] == 'dir') {
        var newFolder = PurFolder(file['name']);
        newContainedFolders.add(newFolder);
        folderModels.add(newFolder);
      }
    }
    folder.containedFiles = newContainedFiles;
    folder.containedFolders = newContainedFolders;
    return responseText['files'];
  }

  /// Takes the response [request] and returns a FileCollection of the files found.
  Future<FileCollection> _handleRequestComplete(String request) async {
    return _processResponse(request);
  }

  /// Loops through the incoming [jsonString] and creates a PurFile or PurFolder for each of the JSON entries.
  /// Returns a FileCollection containing all of the found files and folders.
  Future<FileCollection> _processResponse(String jsonString) async {
    try {
      List<dynamic> content = json.decode(jsonString);
      content.forEach((entry) async {
        var name = entry['name'].toString();
        if (entry['type'] == 'file') {
          var versions = <PurFile>[];
          if (entry['commits'] != null) {
            for (var version in entry['commits']) {
              var newVersion = PurFile(version['sha']);
              newVersion.content = version['content'];
              versions.add(newVersion);
            }
          }
          var newPurFile = PurFile(name);
          fileModels.add(newPurFile);
        } else if (entry['type'] == 'dir') {
          // Create new purfolder and add to collection
          var newPurFolder = PurFolder(name);
          folderModels.add(newPurFolder);
        }
      });
      return FileCollection(fileModels, folderModels);
    } catch (e) {
      print('Error with processing the request, ${e.toString()}');
      rethrow;
    }
  }

  /// Method for both retrieving and saving the AST information of the current file with content [content] on line [line].'
  /// sends for use-def information, but in replay the replay model will access that information
  Future<ASTCollection> getAndSaveASTInformation(
      String line, String content, String fileName, String sanitizedName, String fileType) async {
    //These are ways to talk to the compiler service, but we are not using them right now.
    //await getASTInformation(line, content);
    //await postFileToUseDefCheck(content, fileName, fileType);
    await postFileToParser(content, fileName, sanitizedName, fileType);

    return astCollection;
  }

  /// Retrieves the AST Information for a file with content [content]. Currently, the line does not do anything. Instead, you get a ASTCollection with all of the found methods and variables as AST Nodes.
  Future<void> getASTInformation(String line, String content) async {
    try {
      var data = {'line': line, 'file': content};
      var result = await GanderServiceModel.sendRequestwithRespons(
          '$_HOST:$_PORT$_COMPILERPATH$_ASTPATH',
          method: 'POST',
          sendData: json.encode(data),
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
      var jsonArr = json.decode(result);
      //print(result);
      compilationUnitAST = jsonArr[0];
      return _processASTResponse(result);
    } catch (e) {
      print('Error with retrieving the AST information: ${e}');
      rethrow;
    }
  }

  void processASTNodes() {
    if (compilationUnitAST == null) return;

    Map<String, dynamic> compAst = compilationUnitAST;
    //print all ids contained in compAst as id + children.ids()

    // var idArr = getChildrenId(compAst['CompilationUnit']);
    // idArr.removeWhere((item) => item == '');

    // print(compAst['CompilationUnit']);
    // print(idArr);

    // print(printChildren(compAst['CompilationUnit']));
  }

  List<String> getChildrenId(Map<String, dynamic> childMap) {
    //Detta funkar intre som jag tänkte
    List<String> arr = [];
    arr.add(childMap['id'] ?? '');
    if (childMap['children'] != null) {
      for (var child in childMap['children']) {
        arr.addAll(getChildrenId(child));
      }
    }
    return arr;
  }

  String printChildren(Map<String, dynamic> childMap) {
    //Detta funkar intre som jag tänkte
    var id = '';
    id = childMap['id'] ?? '';
    if (childMap['children'] != null) {
      for (var child in childMap['children']) {
        id += ' ' + printChildren(child);
      }
    }
    return id;
  }

  /// Takes the server response [response] and returns the found AST Locations for the methods and variables found in the file.
  void _processASTResponse(String response) {
    List<dynamic> responseText = json.decode(response);
    astCollection = ASTCollection();
    astCollection.addLocations(responseText[0]);
  }

  /// Returns the found AST Location on line [line] in the current file.
  ASTLocation getPositionInformation(String line) {
    var found = astCollection.findPosition(line);
    return found;
  }
}

/// Class for representing a file.
class PurFile {
  Map attributes;
  String name;
  String extension;
  String content;
  List<PurFile> versions;
  bool isParsable;

  /// Versions of a file.
  FileCommitCollection fileVersions;

  PurFile get file {
    return this;
  }

  PurFile(this.name) {
    extension = p.extension(name);
    
    try {
      isParsable = FileCollectionModel.isExtensionSupported(extension);
    } catch (e) {
      print('Error while trying to access supported extensions: $e');
      rethrow;
    }
  }

  operator [](attr) => attributes[attr];
}

/// Class for representing a folder.
class PurFolder {
  List<PurFile> containedFiles;
  List<PurFolder> containedFolders;
  String name;

  PurFolder(this.name, [this.containedFiles, this.containedFolders]);

  /// A bool checker to see if the folder is empty.
  bool isEmpty() {
    return containedFiles.isEmpty && containedFolders.isEmpty;
  }
}

/// A class for keeeping track of the different commits of a file. Used in the diff view to keep track of how a file has been changed.
class FileCommit {
  final String sha;
  final String date;
  final String message;
  String content;

  FileCommit(this.sha, this.date, this.message);

  /// Sets the content of a FileCommit to [newContent].
  set fileContent(String newContent) {
    content = newContent;
  }
}

/// Class for representing a collection of FileCommits.
class FileCommitCollection {
  List<FileCommit> fileCommits;

  FileCommitCollection(this.fileCommits);

  List<FileCommit> get files => fileCommits;

  /// Adds a FileCommit [fc] to the collection.
  void addFileCommit(FileCommit fc) => fileCommits.add(fc);

  /// Returns a FileCommit with sha number [sha] from the FileCommitCollection.
  FileCommit getFileCommit(String sha) {
    for (var commit in fileCommits) {
      if (commit.sha == sha) return commit;
    }

    return null;
  }
}

/// Class for representing a location. Used when looking for the AST Nodes.
class Location {
  int startLine;
  int startCol;
  int endLine;
  int endCol;

  Location(String start, String end) {
    var startParts = start.split(':');
    var endParts = end.split(':');
    startLine = int.parse(startParts[0]);
    startCol = int.parse(startParts[1]);
    endLine = int.parse(endParts[0]);
    endCol = int.parse(endParts[1]);
  }

  @override
  String toString() {
    return startLine.toString() + '-' + startCol.toString();
  }
}

/// Class that keeps track of the positon of the method and var uses.
class InstanceUse {
  final Location loc;
  const InstanceUse(this.loc);

  Location get location {
    return loc;
  }
}

/// Class that keeps track of the position of the var and method definitions.
/// It also keep tracks of were the def is used.
class Def {
  String name;
  final Location loc;
  List<InstanceUse> useList = [];
  bool isVar;

  Def(this.name, this.loc, this.isVar);

  /// Returns the list of InstanceUses of the Def.
  List<InstanceUse> get list => useList;

  /// Returns the location of the Def.
  Location get location {
    return loc;
  }

  /// Adds a InstanceUse of the Def to the list of usages.
  void addUse(use) {
    useList.add(use);
  }

  String toString() {
    String uses = '{';
    for (var use in useList) {
      uses += '"useLoc": "$use"';
    }
    uses += '}';
    return '{"$name" : {"Def" :"$loc", "uses": $uses}}';
  }
}

/// Class for representing a collection of files and folders.
class FileCollection {
  List<PurFile> purFiles;
  List<PurFolder> purFolders;

  List<PurFile> get files => purFiles;
  List<PurFolder> get folders => purFolders;

  /// Retrieves a file or folder in the FileCollection with the name [name].
  PurFile getFile(String name) {
    return purFiles.firstWhere((file) => file.name == name, orElse: () => null);
  }

  FileCollection(this.purFiles, this.purFolders) {
    print("test");
  }
}
