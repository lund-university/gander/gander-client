import 'dart:html';

//import 'aoi_handler.dart';
import 'package:gander/aoi/aoi_base.dart';

import '../aoi/aoi_temp.dart';
import 'file_change_view.dart';
import 'file_view.dart';
import '../aoi/ast_converter.dart';
import 'compare/compare_model.dart';
import 'file_model.dart';
import '../comments/annotation_model.dart';
import '../comments/comment_model.dart';
import '../aoi/linear_aoi.dart';
import '../aoi/aoi_tokenized_handler.dart';
import '../aoi/aoi_ast_handler.dart';

enum Display {
  ///Display the changeView, value 0
  changeView,

  ///Display the fileView
  fileView
}

class ContentAreaHandler {
  FileCollectionModel fileCollModel = FileCollectionModel();
  CommentCollection commentColl;
  Comparison comp;
  String targetContent;
  String name;
  AOIHandlerBase aoi;

  ///The file name without characters that are not allowed in a html id or html class.
  String sanitizedName;
  DivElement fileObjDiv;
  ASTInformation srcAST = ASTInformation();
  ASTInformation targetAST = ASTInformation();
  ASTFile astFile;
  Function interactionCallback;
  Function postCommentCallback;
  DivElement fileContent = DivElement()..id = 'file-content';
  bool commentsAreEnabled;
  Map<String, List<Def>> targetUseDefmap;
  Map<String, List<Def>> srcUseDefmap;

  DivElement contentDiv;
  //Store<int> store;

  //Enum to keep track of which view is currently displayed
  Display currentDisplay = Display.changeView;

  CommentModel commentModel;

  ContentAreaHandler(this.comp, this.commentColl, this.targetContent,
      this.interactionCallback, this.postCommentCallback,
      {this.commentsAreEnabled}) {
    fileCollModel.useDefMap = {};

    name = comp.filename;
    sanitizedName = sanitizeFileName(name);
    commentModel = CommentModel(comp, commentColl, interactionCallback,
        postCommentCallback, sanitizedName);
  }

  /// Returns a version of [name] without any characters that are not allowed in a html id or html class.
  String sanitizeFileName(String name) {
    return name.replaceAll('.', '').replaceAll('/', '').replaceAll('-', '').replaceAll('_', '');
  }

  ///Generates the contentView of the file
  Future<DivElement> getContentView() async {
    contentDiv = DivElement()..id = 'code-view-div';


    //generate div contatining the correct view
    var viewDiv = await getViewDiv();
    //generate the switch button
    var switchBtn = SwitchViewToggleButton();
    //add the switch button to the content div
    contentDiv.append(switchBtn);
    commentModel.addCommentForFile(viewDiv);
    //add the view div to the content div
    contentDiv.append(viewDiv);

    commentModel.generateCommentCollectionComments(viewDiv);

    fileCollModel.processASTNodes();
  

    return contentDiv;
  }

  //temporary solution, we want the aois to be generated per-file and from within the view.
  List<DivElement> generateAOI() {
    return aoi.handleAOIs();
  }

  Future<DivElement> getViewDiv() async {
    var view;
    //var diffViewMap = fileCollModel.getUseDefMap();
    //var fileViewMap = diffViewMap;

    //fileViewMap.removeWhere((key, value) => key.contains('src')); not needed for this kind of AOI
    if (currentDisplay == Display.changeView) {
      //aoi = LinearAOI(interactionCallback, diffViewMap);
      //aoi = TokenizedAOI(interactionCallback, sanitizedName);
      if (comp.isParsable) {
        aoi = AOIASTHandler(interactionCallback, fileCollModel.getAstMap());
        view = ChangeView(comp, commentColl, targetContent, fileCollModel,
            interactionCallback, postCommentCallback, aoi,
            commentsAreEnabled: commentsAreEnabled);
      } else {
        //print(sanitizedName);
        aoi = TokenizedAOI(interactionCallback, sanitizedName);
        view = ChangeView(comp, commentColl, targetContent, fileCollModel,
            interactionCallback, postCommentCallback, aoi,
            commentsAreEnabled: commentsAreEnabled);
      }
      //print('diffViewMap');
      //print(diffViewMap);
    } else {
      //aoi = LinearAOI(interactionCallback, fileViewMap);
      //aoi = TokenizedAOI(interactionCallback, sanitizedName);
      if (comp.isParsable) {
        aoi = AOIASTHandler(interactionCallback, fileCollModel.getAstMap());
        view = FileView(comp, commentColl, targetContent, fileCollModel,
            interactionCallback, postCommentCallback, aoi,
            commentsAreEnabled: commentsAreEnabled);
      } else {
        aoi = TokenizedAOI(interactionCallback, sanitizedName);
        view = FileView(comp, commentColl, targetContent, fileCollModel,
            interactionCallback, postCommentCallback, aoi,
            commentsAreEnabled: commentsAreEnabled);
      }
    }
    return await view.getViewDiv();
  }

  void switchView() async {
    if (currentDisplay == Display.changeView) {
      currentDisplay = Display.fileView;
    } else {
      currentDisplay = Display.changeView;
    }
    var viewDiv = await getViewDiv();
    contentDiv.children.last.remove();
    contentDiv.append(viewDiv);

    //If we switch back to changeView we need to generate the aois again
    //temp solution, could be done better.
    //if (currentDisplay == Display.changeView) {
    //targetAST.collection.astString(targetAST.collection.location);
    var aois = generateAOI();
    //for (var aoiDiv in aois) {
    //  contentDiv.append(aoiDiv);
    //}

    //}
  }

  DivElement SwitchViewToggleButton() {
    var div = DivElement()..id = 'switch-view-div';
    //create a button element for toggling the view mode
    var lbl = LabelElement()..className = 'switch';

    var toggleButton = InputElement()
      ..type = 'checkbox'
      ..id = 'switch-view-button-$sanitizedName'
      ..onClick.listen((e) {
        interactionCallback(e);
        switchView();
      });

    var spn = SpanElement()
      ..id = 'switch-view-span'
      ..className = 'slider round';

    lbl.append(toggleButton);
    lbl.append(spn);
    div.append(lbl);
    return div;
  }
}
