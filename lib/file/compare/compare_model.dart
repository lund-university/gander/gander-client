/// TODO DOCUMENTATION
import 'dart:convert';

import '../file_model.dart';
import '../../service_models/gander_service_model.dart';
import 'package:path/path.dart' as p;

List<Patch> parsePatchFromMap(String fileName, List<dynamic> jsonPatchList) {
  var patchList = <Patch>[];
  for (var jsonPatch in jsonPatchList) {
    var patch = Patch(
        fileName,
        jsonPatch['startSrcLine'],
        jsonPatch['startTargetLine'],
        jsonPatch['srcLinesLength'],
        jsonPatch['targetLinesLength'],
        GanderCompare.handleCodePart(jsonPatch['codeParts']));
    patchList.add(patch);
  }
  return patchList;
}

class GanderCompare {
  static const int _PORT = 8001;
  static const String _HOST = 'http://localhost';
  static const String _DIFFPATH = '/diff';
  static const String _COMPAREPATH = '/compare';
  static const String _COMPAREFILES = '/compareFiles';
  static const String _PULLPATH = '/pulls';
 
  GanderCompare();

  Future<PullRequestPatch> getPullRequestPatch(int pullNumber) async {
    try {
      var result = await GanderServiceModel
          .getRequest('$_HOST:$_PORT$_DIFFPATH$_PULLPATH/$pullNumber');

      return _processPullRequestPatchResponse(result);
    } catch (e) {
      print('Exception thrown on $_HOST:$_PORT$_DIFFPATH$_PULLPATH/$pullNumber');
      rethrow;
    }
  }

  PullRequestPatch _processPullRequestPatchResponse(String jsonString) {
    Map<String, dynamic> jsonObj = json.decode(jsonString);
    var patchListString = json.encode(jsonObj[
        'patch_list']); //Convert to string to be able to use _handlePatch
    var comparisons = _handlePatch(patchListString);
    var pullRequestPatch = PullRequestPatch(
        jsonObj['head_sha'],
        jsonObj['head_ref'],
        jsonObj['base_sha'],
        jsonObj['base_ref'],
        comparisons);
    return pullRequestPatch;
  }

  Future<List<Comparison>> fetchCompareCommits(String sha1, String sha2) async {
    try {
      var result = await GanderServiceModel
          .getRequest('$_HOST:$_PORT$_DIFFPATH$_COMPAREPATH/$sha1...$sha2');
      return _handlePatch((result));
    } catch (e) {
      print(
          "Couldn't open $_HOST:$_PORT$_DIFFPATH$_COMPAREPATH/$sha1...$sha2: " +
              e.toString());
      rethrow;
    }
  }

  Future<String> fetchCompareFiles(
      String srcContent, String targetContent) async {
    var body = {'src': srcContent, 'target': targetContent};
    try {
      var result = await GanderServiceModel.sendRequestwithRespons(
          '$_HOST:$_PORT$_COMPAREFILES',
          method: 'POST',
          sendData: json.encode(body),
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
      return _handleCompareFiles(result);
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_COMPAREFILES");
      rethrow;
    }
  }

  String _handleCompareFiles(String response) {
    Map<String, dynamic> jsonResponse = json.decode(response);
    var patch = jsonResponse['patch'];
    return patch;
  }

  List<Comparison> _handlePatch(String response) {
    List<dynamic> responseText = json.decode(response);

    var comparisons = <Comparison>[];

    for (var commit in responseText) {
      var fileName = commit['filename'];
      var jsonPatchList = commit['patch'];
      var patchList;
      if (jsonPatchList != null) {
        patchList = parsePatchFromMap(fileName, jsonPatchList);
      } else {
        patchList = null;
      }

      comparisons.add(Comparison(fileName, commit['status'],
          commit['additions'], commit['deletions'], patchList));
    }
    return comparisons;
  }

  static List<CodePart> handleCodePart(List<dynamic> jsonCodePartList) {
    var codePartList = <CodePart>[];
    for (var codePartJson in jsonCodePartList) {
      var diffList = _handleDiff(codePartJson['coordinates']);
      var codePart = CodePart(codePartJson['nbrOfLines'],
          codePartJson['startsWith'], codePartJson['content'], diffList);
      codePartList.add(codePart);
    }
    return codePartList;
  }

  static List<Diff> _handleDiff(List<dynamic> jsonDiffList) {
    var diffList = <Diff>[];
    if (jsonDiffList == null) {
      return diffList;
    }
    for (var jsonDiff in jsonDiffList) {
      var diff = Diff(jsonDiff['startColumn'], jsonDiff['endColumn']);
      diffList.add(diff);
    }
    return diffList;
  }
}

class PullRequestPatch {
  String headRef;
  String headSha;
  String baseRef;
  String baseSha;
  List<Comparison> comparisons;
  PullRequestPatch(
      this.headRef, this.headSha, this.baseRef, this.baseSha, this.comparisons);
}

class Comparison {
  final String filename;
  final String status;
  final int additions;
  final int deletions;
  final List<Patch> patchList;
  String extension;
  bool isParsable;

  Comparison(this.filename, this.status, this.additions, this.deletions,
      this.patchList) {

    extension = p.extension(filename);

    try {
      isParsable = FileCollectionModel.isExtensionSupported(extension);
    } catch (e) {
      print('Error while trying to access supported extensions: $e');
      rethrow;
    }
  }
}

class Patch {
  int startSrcLine;
  int startTargetLine;
  int srcLinesLength;
  int targetLinesLength;

  List<CodePart> codeList;
  String fileName;
  Patch(this.fileName, this.startSrcLine, this.startTargetLine,
      this.srcLinesLength, this.targetLinesLength, this.codeList);
  @override
  String toString() {
    return '$fileName - $startSrcLine,$targetLinesLength $startTargetLine,$targetLinesLength $codeList';
  }
}

class CodePart {
  int nbrOfLines;
  String startsWith;
  String content;
  List<Diff> diffList;
  CodePart(this.nbrOfLines, this.startsWith, this.content, this.diffList);
}

class Diff {
  int startColumn;
  int endColumn;
  Diff(this.startColumn, this.endColumn);

  @override
  String toString() {
    return '$startColumn -> $endColumn';
  }
}
