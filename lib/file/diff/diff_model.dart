/// TODO DOCUMENTATION
import 'dart:convert';

import '../../gander_package.dart';
import '../../service_models/gander_service_model.dart';

class GanderDiff {
  CommitCollection commits;
  FileCollectionModel fileModel;

  static const int _PORT = 8001;
  static const String _HOST = 'http://localhost';
  static const String _GITCOMMITPATH = '/git';
  static const String _PURPATH = '/pur';
  static const String _COMMITPATH = '/commits';

  GanderDiff(this.fileModel);
  String getDiffs() {
    return '';
  }

  Future<Commit> getCommitChanges(Commit commit) async {
    try {
      var result = await GanderServiceModel
          .getRequest('$_HOST:$_PORT$_PURPATH$_COMMITPATH/${commit.sha}');
      return await _handleChangeRequestComplete(result, commit);
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_PURPATH$_COMMITPATH/${commit.sha}: " +
          e.toString());
    }
  }

  Future<List<String>> fetchCommitsFromSha(String sha) async {
    try {
      var result =
          await GanderServiceModel.getRequest('$_HOST:$_PORT$_PURPATH$_COMMITPATH/$sha');
      return await _handleCommitFromSha((result));
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_GITCOMMITPATH: " + e.toString());
      rethrow;
    }
  }

  Commit _handleChangeRequestComplete(String response, Commit commit) {
    var responseText = json.decode(response);
    var foundChanges = <FileChanges>[];
    for (var fileChange in responseText['files']) {
      var file = fileModel.getFile(fileChange['filename']);
      // Do I need to check this? It should be found
      var change =
          FileChanges(file, fileChange['additions'], fileChange['deletions']);
      foundChanges.add(change);
    }
    commit.changedFiles = foundChanges;
    return commit;
  }

  List<String> _handleCommitFromSha(String response) {
    Map<String, dynamic> responseText = json.decode(response);
    List<dynamic> fileList = responseText['files'];
    var nameList = <String>[];
    for (var file in fileList) {
      nameList.add(file['filename']);
    }
    return nameList;
  }

  Future<CommitCollection> getCommits() async {
    try {
      var result =
          await GanderServiceModel.getRequest('$_HOST:$_PORT$_PURPATH$_COMMITPATH');
      return await _handleCommitRequestComplete(result);
    } catch (e) {
      print(
          "Couldn't open $_HOST:$_PORT$_PURPATH$_COMMITPATH: " + e.toString());
      rethrow;
    }
  }

  CommitCollection _handleCommitRequestComplete(String response) {
    List<dynamic> responseText = json.decode(response);
    var foundCommits = <Commit>[];

    for (var commit in responseText) {
      foundCommits.add(Commit(
          commit['sha'], commit['name'], commit['message'], commit['date']));
    }
    commits = CommitCollection(foundCommits);
    return commits;
  }

  Future<FileCommitCollection> fetchCommitsForFile(String fileName) async {
    var encodedName = Uri.encodeComponent(fileName);
    try {
      var result = await GanderServiceModel
          .getRequest('$_HOST:$_PORT$_PURPATH$_COMMITPATH?name=$encodedName');

      return await handleCommitForFileRequestComplete(fileName, result);
    } catch (e) {
      print("Couldn't open $_HOST$_PORT$_PURPATH");
      rethrow;
    }
  }

  Future<FileCommitCollection> handleCommitForFileRequestComplete(
      String filename, String response) async {
    List<dynamic> responseText = json.decode(response);
    var shaList = <FileCommit>[];
    for (var commit in responseText) {
      shaList.add(FileCommit(commit['sha'], commit['date'], commit['message']));
    }
    return FileCommitCollection(shaList);
  }

  Future<String> fetchFileContentFromCommit(String name, String sha) async {
    var encodedName = Uri.encodeComponent(name);

    try {
      var result = await GanderServiceModel
          .getRequest('$_HOST:$_PORT$_PURPATH/$encodedName?sha=$sha');
      var encodedContent = fileContentFromCommitComplete(result);
      var content = GanderUtils.decodeContent(encodedContent);
      return content;
    } catch (e) {
      return null;
    }
  }

  String fileContentFromCommitComplete(String response) {
    Map<String, dynamic> responseText = json.decode(response);
    return responseText['content'];
  }
}

class Commit {
  String sha;
  List<FileChanges> changedFiles;
  String author;
  String message;
  String date;

  Commit(this.sha, this.author, this.message, this.date);

  PurFile getFile(String name) {
    for (var fileChange in changedFiles) {
      if (fileChange.file.name == name) {
        return fileChange.file;
      }
    }
    return null;
  }
}

class CommitCollection {
  List<Commit> commits;

  CommitCollection(this.commits);

  Commit getCommit(String sha) {
    for (var commit in commits) {
      if (commit.sha == sha) {
        return commit;
      }
    }
    return null;
  }
}

class FileChanges {
  PurFile file;
  int additions;
  int deletions;
  //Not sure if necessary, keeps track of the two commits that were compared
  String before;
  String after;

  FileChanges(this.file, this.additions, this.deletions);
}
