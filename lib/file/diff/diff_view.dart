/// TODO DOCUMENTATION
import 'dart:html';
import '../file_model.dart';
import 'diff_model.dart';

class DiffView {
  DivElement contentDiv;
  FileCollection fileList;
  FileCollectionModel model;
  Function visibleElementsCallback;
  Function sendInteractionCallback;

  GanderDiff diff;

  DiffView(this.fileList, this.model, this.diff, this.visibleElementsCallback,
      this.sendInteractionCallback);

  void generateDiffPage(PurFile file) async {
    DivElement contentDisplay = querySelector('.container-body')
      ..innerHtml = '';
    var cardDiv = DivElement()
      ..className = 'card'
      ..id = 'diff-page-card';

    file.fileVersions = await diff.fetchCommitsForFile(file.name);

    cardDiv.append(generateDropdown(file));
    contentDisplay.append(cardDiv);
    generateDiffView();
    displayProgressionView();
    visibleElementsCallback();
  }

  void generateDiffView({PurFile src, PurFile target}) {
    DivElement cardDiv = querySelector('.card');
    if ((src != null) && (target != null)) {
      cardDiv.append(generateNameDivs(src.name, target.name));
      cardDiv.append(generateContentAreas(src.content, target.content));
    } else {
      cardDiv.append(generateNameDivs('', ''));
      cardDiv.append(generateContentAreas('', ''));
    }
    cardDiv.append(generateButtons());
    querySelector('#saveUpdate').onClick.listen(updateContent);
  }

  DivElement generateNameDivs(String nameSrc, String nameTarget) {
    var cardRow = DivElement()
      ..className = 'row'
      ..id = 'name-div-row';
    // Adds the label of the source code to the row
    var srcDiv = DivElement()
      ..className = 'col'
      ..id = '$nameSrc-src-div-col';
    var nameDivSrc = DivElement()
      ..className = 'file-name-src'
      ..id = '$nameSrc-name-div-src';
    var labelSrc = LabelElement()
      ..id = 'label-src-diff-view-$nameSrc'
      ..htmlFor = 'code-line-src'
      ..text = 'Code content of $nameSrc';
    nameDivSrc.append(labelSrc);
    srcDiv.append(nameDivSrc);
    cardRow.append(srcDiv);
    // Adds the label of the target code to the row
    var targetDiv = DivElement()
      ..className = 'col'
      ..id = '$nameTarget-target-div-col';
    var nameTargetDiv = DivElement()
      ..className = 'file-name-target'
      ..id = '$nameTarget-name-div-target';
    var labelTarget = LabelElement()
      ..id = 'label-target-diff-view-$nameTarget'
      ..htmlFor = 'code-line-target'
      ..text = 'Code content of $nameTarget';
    nameTargetDiv.append(labelTarget);
    targetDiv.append(nameTargetDiv);
    cardRow.append(targetDiv);
    return cardRow;
  }

  DivElement generateContentAreas(String contentSrc, String contentTarget) {
    var cardRow = DivElement()
      ..className = 'row'
      ..id = 'content-area-row';

    var srcCol = DivElement()
      ..className = 'col'
      ..id = 'content-area-src-col';

    var srcArea = Element.pre()
      ..id = 'code-line-src'
      ..className = 'code-line-src card system-generated box'
      ..contentEditable = 'true'
      ..innerHtml = contentSrc;
    srcCol.append(srcArea);
    cardRow.append(srcCol);

    var targetCol = DivElement()
      ..className = 'col'
      ..id = 'content-area-target-col';

    var targetArea = Element.pre()
      ..id = 'code-line-target'
      ..className = 'code-line-target card system-generated box'
      ..contentEditable = 'true'
      ..innerHtml = contentTarget;
    targetCol.append(targetArea);
    cardRow.append(targetCol);

    return cardRow;
  }

  DivElement generateButtons() {
    var btnDiv = DivElement()
      ..className = 'file-buttons'
      ..id = 'file-button-div';
    var saveBtn = ButtonElement()
      ..id = 'saveUpdate'
      ..innerHtml = 'Save';
    var discardBtn = ButtonElement()
      ..id = 'removeUpdate'
      ..innerHtml = 'Discard';
    btnDiv.append(saveBtn);
    btnDiv.append(discardBtn);
    return btnDiv;
  }

  DivElement generateDropdown(PurFile file) {
    var dropdownDiv = DivElement()
      ..className = 'dropdown-div'
      ..id = 'dropdown-div';
    var labelDiv = DivElement()
      ..className = 'dropdown-label-div row'
      ..id = 'dropdown-label-div';
    var selectDiv = DivElement()
      ..className = 'dropdown-select-div row'
      ..id = 'dropdown-select-div';

    var labelSrc = LabelElement()
      ..htmlFor = 'dropdownSrc'
      ..id = 'dropdown-label-src'
      ..text = 'Select source branch';
    var selectSrc = SelectElement()
      ..className = 'dropdown col'
      ..name = 'dropdownSrc'
      ..id = 'dropdownSrc'
      ..onChange.listen((event) {
        changeSourceDiv(event, file);
      });

    var labelTarget = LabelElement()
      ..htmlFor = 'dropdownTarget'
      ..id = 'dropdown-label-target'
      ..text = 'Select target branch';
    var selectTarget = SelectElement()
      ..className = 'dropdown col'
      ..name = 'dropdownTarget'
      ..id = 'dropdownTarget'
      ..onChange.listen((event) {
        changeTargetDiv(event, file);
      });

    var placeholderSrc = OptionElement()
      ..value = ''
      ..id = 'dropdown-option-placeholder-src'
      ..text = 'Select source branch';
    var placeholderTarget = OptionElement()
      ..value = ''
      ..id = 'dropdown-option-placeholder-target'
      ..text = 'Select target branch';
    selectSrc.append(placeholderSrc);
    selectTarget.append(placeholderTarget);

    var shaList = file.fileVersions.fileCommits;

    for (var f in shaList) {
      var optSrc = OptionElement()
        ..value = '${f.sha}'
        ..id = 'src-${f.sha.substring(5)}'
        ..text = '${f.sha}';
      var optTarget = OptionElement()
        ..value = '${f.sha}'
        ..id = 'target-${f.sha.substring(5)}'
        ..text = '${f.sha}';
      selectSrc.append(optSrc);
      selectTarget.append(optTarget);
    }
    labelDiv.append(labelSrc);
    labelDiv.append(labelTarget);
    dropdownDiv.append(labelDiv);
    selectDiv.append(selectSrc);
    selectDiv.append(selectTarget);
    dropdownDiv.append(selectDiv);
    return dropdownDiv;
  }

  void changeSourceDiv(Event e, PurFile file) async {
    sendInteractionCallback(e);
    var srcArea = querySelector('#code-line-src');
    var label = querySelector('#label-src-diff-view');
    var content = await getViewContent(e, file);
    updateView(file.name, content, srcArea, label);
  }

  void changeTargetDiv(Event e, PurFile file) async {
    sendInteractionCallback(e);
    var srcArea = querySelector('#code-line-target');
    var label = querySelector('#label-target-diff-view');
    var content = await getViewContent(e, file);
    updateView(file.name, content, srcArea, label);
  }

  Future<String> getViewContent(Event e, PurFile file) async {
    sendInteractionCallback(e);
    SelectElement el = e.currentTarget;
    var sha = el.value;
    var fcc = file.fileVersions;
    var fileCommit = fcc.getFileCommit(sha);
    var content = fileCommit.content;
    if (content == null) {
      //If the FileCommit does not contain the content, fetch it from the server.
      content = await diff.fetchFileContentFromCommit(file.name, sha);
      fileCommit.fileContent = content;
    }
    return content;
  }

  void updateView(
      String name, String content, Element area, LabelElement label) {
    area.innerHtml = content;
    label.text = 'Code content of $name';
  }

  void updateContent(MouseEvent e) async {
    print('Wants to update ' + e.target.toString());
  }

  void displayProgressionView() async {
    var cardDiv = DivElement()
      ..className = 'card'
      ..id = 'progression-view-card';
    var commitCollection = await diff.getCommits();
    var commits = commitCollection.commits;
    var list = UListElement()..id = 'commit-list-progression';
    for (var current in commits) {
      var uElement = LIElement()
        ..className = 'list-item'
        ..id = 'list-commit-item-${current.sha.substring(5)}'
        ..value = 0
        ..text = current.sha;
      list.append(uElement);
    }
    cardDiv.append(list);
    // Creates a new card for the view where the progression is shown.
    // Should display each commit with the sha number
    //Each commit should display how many files were modified and how many lines
    querySelector('.container-body').append(cardDiv);
  }
}
