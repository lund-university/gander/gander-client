import 'dart:convert';
import 'dart:html';
import '../replay/replay_model.dart';

class GanderServiceModel {
  static const int _PORT = 8001;
  static const String _HOST = 'http://localhost';
  static const String _STOREINTERACTIONPATH = '/storeInteraction';
  static bool replay = false;
  static Map<String, List<ReqResLog>> mockService;

  /// Sends a request to the backend.
  /// if client is in replay, we ignore all POST that don't require a response
  static Future<void> sendRequest(String url,
      {String method,
      Map<String, String> requestHeaders,
      dynamic sendData}) async {
    if (!replay) {
      try {
        //print(url);
        //print('_________________');
        var timeReq = DateTime.now().microsecondsSinceEpoch;
        var result = await HttpRequest.request(url,
            method: method, sendData: sendData, requestHeaders: requestHeaders);
        var TimeRes = DateTime.now().microsecondsSinceEpoch;
        // print(url);
        //Make sure not to store excessive req/res information. the filtered functions already log the necessary data.
        if (!url.contains('/storeCoordinates') &&
            !url.contains('/storeElements') &&
            !url.contains('/storeInteraction') &&
            !url.contains('/storeBufferData')) {
          await postReqResToStorage(url, result.responseText, method,
              requestHeaders, sendData, timeReq, TimeRes);
        }
      } catch (e) {
        print("Couldn't open $url");
        rethrow;
      }
    }
  }

  ///Sends a request to the backend and returns the response.
  ///if client is in replay the response comes from the mockmap
  static Future<String> sendRequestwithRespons(String url,
      {String method,
      Map<String, String> requestHeaders,
      dynamic sendData, String fileName}) async {
    if (!replay) {
      try {
        var TimeReq = DateTime.now().microsecondsSinceEpoch;
        var result = await HttpRequest.request(url,
            method: method, sendData: sendData, requestHeaders: requestHeaders);
        // print(url);
        var TimeRes = DateTime.now().microsecondsSinceEpoch;

        await postReqResToStorage(url, result.responseText, method,
          requestHeaders, sendData, TimeReq, TimeRes, fileName);
        
        return result.responseText;
      } catch (e) {
        print("Couldn't open $url");
        rethrow;
      }
    } else {
      return MockResponses(url);
    }
  }

  ///Sends a GET request to the backend and returns the respons
  ///if client is in replay the response comes from the mockmap
  static Future<String> getRequest(String url) async {
    if (!replay) {
      try {
        var TimeReq = DateTime.now().microsecondsSinceEpoch;
        var result = await HttpRequest.getString(url);
        //print(url);
        var TimeRes = DateTime.now().microsecondsSinceEpoch;
        await postGETReqResToStorage(url, result, TimeReq, TimeRes);
        return result;
      } catch (e) {
        print("Couldn't open $url " + e.toString());
      }
    } else {
      return MockResponses(url);
    }
    return null;
  }

  ///posts the req/res pars to storage with their respective timestamps
  static Future<void> postReqResToStorage(
      String url,
      String Res,
      String method,
      Map<String, String> requestHeaders,
      dynamic sendData,
      int T1,
      int T2,
      [String fileName]) async {
    if (!replay) {
      var data = '{"msSinceEpoch": {"TimeReq": "$T1", "TimeRes": "$T2"}, "Request": "$url", "Method": "$method", "Data": $sendData, "Response": $Res}';
      
      if (fileName != null) {
        data = '{"msSinceEpoch": {"TimeReq": "$T1", "TimeRes": "$T2"}, "Request": "$url", "Method": "$method", "Data": $sendData, "FileName": "$fileName", "Response": $Res}';
      }

      try {
        var result = await HttpRequest.request(
            '$_HOST:$_PORT$_STOREINTERACTIONPATH',
            method: 'POST',
            sendData: data,
            requestHeaders: {
              'Content-Type': 'application/json; charset=UTF-8'
            });
      } catch (e) {
        print("Couldn't open $url " + e.toString());
      }
    }
  }

  ///posts the req/res pars to storage with their respective timestamps
  static Future<void> postGETReqResToStorage(
      String req, String res, int T1, int T2) async {
    if (!replay) {
      try {
        var result = await HttpRequest.request(
            '$_HOST:$_PORT$_STOREINTERACTIONPATH',
            method: 'POST',
            sendData:
                '{"msSinceEpoch": {"TimeReq": "$T1", "TimeRes": "$T2"}, "Request": "$req", "Method": "GET", "Response": $res}',
            requestHeaders: {
              'Content-Type': 'application/json; charset=UTF-8'
            });
      } catch (e) {
        print("Couldn't open $req " + e.toString());
      }
    }
  }

  static void getMockService(Map<String, List<ReqResLog>> map) {
    mockService = map;
  }

  ///Looks through the map to find the correct response for the request
  ///The delay is calculated from the timestamps and should reflect the time it took in the session
  ///if the specific response has already been seen/used, we iterate through the list to get the next one
  static Future<String> MockResponses(String req) async {
    var mockRes = mockService[req];
    for (var item in mockRes) {
      if (!item.used) {
        await Future.delayed(Duration(
            microseconds: int.parse(item.tRes) - int.parse(item.tReq)));
        item.used = true;
        return jsonEncode(item.response);
      }
    }
    return null;
/*
    if (mockService.containsKey(req)) {
      for (var i in mockService.entries) {
        if (i.key.compareTo(req) == 0) {
          for (var res in i.value) {
            if (!res.used) {
              await Future.delayed(Duration(
                  microseconds: int.parse(res.tRes) - int.parse(res.tReq)));
              res.used = true;
              return jsonEncode(res.response);
            }
          }
        }
      }
    }
    return null;*/
  }
}
