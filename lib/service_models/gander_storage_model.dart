import 'dart:convert';
import 'dart:html';

import 'gander_service_model.dart';

/// Responsible for storing and posting the eye tracker data to Storage Service.
class StorageModel {
  static const int _PORT = 8001;
  static const String _HOST = 'http://localhost';
  static const String _STORECOORDSPATH = '/storeCoordinates';
  static const String _STOREELEMENTSPATH = '/storeElements';
  static const String _STOREINTERACTIONPATH = '/storeInteraction';
  static const String _SYNCHPATH = '/initSynchronization';
  static const String _NEWFILEPATH = '/createNewFile';
  static const String _STORE_BUFFER_DATA_PATH = '/storeBufferData';
  // In what frequency we want to send the stored interactions to the storage.
  //static const int _FREQUENCY = 30;
  static const int _FREQUENCY = 90; //We are trying to samlpe 90 Hz via client
  int indexCoords = 0;
  int indexAST = 0;
  List<String> dataList = [];
  List<String> elementList = [];
  List<String> astList = [];

  StorageModel();

  /// Stores the coordinates [data].
  ///
  /// When we have the same amount of coordinates as our frequency rate - send them to the Storage Service.
  /// This is done to lower the amount of communication between the client and the server to save CPU.
  void storeCoordinates(String data) {
    indexCoords++;
    var json = jsonDecode(data);

    json['msSinceEpoch'] = DateTime.now().microsecondsSinceEpoch;

    dataList.add(jsonEncode(json));
    if (indexCoords == _FREQUENCY) {
      postEyeTrackingDataToStorage(dataList.toString());

      indexCoords = 0;
      dataList = [];
    }
  }

  /// Stores the element [data].
  ///
  /// When we have the same amount of coordinates as our frequency rate - send them to the Storage Service.
  /// This is done to lower the amount of communication between the client and the server to save CPU.
  void storeElements(String data) {
    elementList.add(data);
    if (elementList.length == _FREQUENCY) {
      postElementDataToStorage(elementList.toString());
      elementList = [];
    }
  }

/*
  /// Stores the interaction [action] with the element [el].
  Future<void> storeInteraction(String action, Element el) {
    var data =
        '{"action" : "$action", "element-id" : "${el.id}", "element" : "${el}", "msSinceEpoch" : "${DateTime.now().microsecondsSinceEpoch}"}';
    return postInteractionDataToStorage(data);
  }
*/
  ///Stores the information [action] with the element [el]
  ///meant to replace storeInformation when ready.
  Future<void> storeInteraction(String action, Element el, [String data = '']) {
    var content;
    if (data.startsWith('{')) {
      content =
          '{"action" : "$action", "element-id" : "${el.id}", "element" : "$el", "data" : $data, "msSinceEpoch" : "${DateTime.now().microsecondsSinceEpoch}"}';
    } else {
      content =
          '{"action" : "$action", "element-id" : "${el.id}", "element" : "$el", "data" : "$data", "msSinceEpoch" : "${DateTime.now().microsecondsSinceEpoch}"}';
    }

    return postInteractionDataToStorage(content);
  }

  /// Stores the AST Information [astString].
  ///
  /// When we have the same amount of coordinates as our frequency rate - send them to the Storage Service.
  /// This is done to lower the amount of communication between the client and the server to save CPU.
  void storeASTInformation(String astString) {
    indexAST++;
    astList.add(astString);
    if (indexAST == _FREQUENCY) {
      postEyeTrackingDataToStorage(astList.toString());

      indexAST = 0;
      astList = [];
    }
  }

  ///Sends buffer data to storage
  Future<void> postBufferDataToStorage(String content) async {
    try {
      var result = await GanderServiceModel.sendRequest(
          '$_HOST:$_PORT$_STORE_BUFFER_DATA_PATH',
          method: 'POST',
          sendData: content,
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_STORE_BUFFER_DATA_PATH");
      rethrow;
    }
  }

  /// Sends the list of coordinates [content] to the storage service.
  Future<void> postEyeTrackingDataToStorage(String content) async {
    try {
      var result = await GanderServiceModel.sendRequest(
          '$_HOST:$_PORT$_STORECOORDSPATH',
          method: 'POST',
          sendData: content,
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_STORECOORDSPATH");
      rethrow;
    }
  }

  /// Sends the list of elements [content] to the storage service.
  Future<void> postElementDataToStorage(String content) async {
    try {
      var result = await GanderServiceModel.sendRequest(
          '$_HOST:$_PORT$_STOREELEMENTSPATH',
          method: 'POST',
          sendData: content,
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_STOREELEMENTSPATH");
      print('kuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuk');
      rethrow;
    }
  }

  /// Sends the interaction [content] to the storage service.
  Future<void> postInteractionDataToStorage(String content) async {
    try {
      var result = await GanderServiceModel.sendRequest(
          '$_HOST:$_PORT$_STOREINTERACTIONPATH',
          method: 'POST',
          sendData: content,
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
    } catch (e) {
      print("Couldn't open $_HOST:$_PORT$_STOREINTERACTIONPATH");
      rethrow;
    }
  }

  /// Converts the timestamps to a JSON Object and sends them to the Storage Service.
  ///
  /// Since we have two different timestamps, [ts] and [millisecondsSinceEpoch], we need to streamline them.
  /// For more information, see /initSynchronization in Storage Service.
  void sendTimeSynch(String ts, String millisecondsSinceEpoch) {
    var data = '{"ts": "$ts", "msSinceEpoch": "$millisecondsSinceEpoch"}';
    postSynchDataToStorage(data);
  }

  /// Initializes the timestamps [data] in Storage Service.
  /// For more information, see /initSynchronization in Storage Service.
  Future<void> postSynchDataToStorage(String data) async {
    try {
      var result = await GanderServiceModel.sendRequest(
          '$_HOST:$_PORT$_SYNCHPATH',
          method: 'POST',
          sendData: data,
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
    } catch (e) {
      print("Couldn't post synch data to $_HOST:$_PORT$_SYNCHPATH");
      rethrow;
    }
  }

  /// When closing the connection, send all remaining data in the local storage to the Storage Service and reset the state.
  void sendAllDataOnEnd() {
    postEyeTrackingDataToStorage(dataList.toString());
    postElementDataToStorage(elementList.toString());
    postEyeTrackingDataToStorage(astList.toString());
    indexCoords = 0;
    indexAST = 0;
    dataList = [];
    astList = [];
    elementList = [];
  }

  /// Tells the Storage Service to create a new log file for the data.
  void createNewFileInStorage() {
    postCreateNewFileInstruction();
  }

  /// Tells the Storage Service to create a new log file for the data.
  Future<void> postCreateNewFileInstruction() async {
    var data = '""';
    try {
      var result = await GanderServiceModel.sendRequest(
          '$_HOST:$_PORT$_NEWFILEPATH',
          method: 'POST',
          sendData: data,
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'});
    } catch (e) {
      print(
          "Couldn't send instruction to create new file to $_HOST:$_PORT$_NEWFILEPATH");
      rethrow;
    }
  }
}
