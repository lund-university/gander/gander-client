# Client side README


## Installation
- Install Dart (must have Chocolatey installed on your machine):
choco install dart-sdk
- Make sure you have webdev activated:

```dart pub global activate webdev```

## Running the client
Start off by running:

```pub get```
And then:

```pub get update```

To retrieve all of the correct dependencies and config files.

Then, to run the Dart client write: 

```webdev serve```

## Test
- To test the Dart client, simply run:

```pub run test test/```

- To run the pacth test:
```pub run test -p chrome test/patch_test.dart```

## Line and column formatting for diff usage
The server sends information to the client about where there are changes, and the client uses the information to update the website. 
To do so, the server uses the following format on its responses:
```"start" : "0:1", "end" : "2:3"```
this means that the diff starts at line 0, col 1 and ends on line 2, col 3.



COMMENT {
    number: 375563595, 
    file: src/Bird.java, updated_at: 020-03-16T21:16:16Z, 
    diff_hunk: @@ -35,17 +35,23 @@, 
    created_at: 2020-03-16T20:36:28Z, 
    id: 393295073, 
    body: If we use my version, the bird will gradually fall to the ground.,
    is_a_reply: true, 
    user: ONeilN, 
    reply_to_id: 393283526}
