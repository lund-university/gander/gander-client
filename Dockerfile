FROM dart:2.19 AS build

#ENV PATH =  "$PATH:$Home/.pub-cache/bin"
#RUN echo $PATH

WORKDIR /app
COPY pubspec.yaml ./
COPY pubspec.lock ./
RUN dart pub get

COPY . .
ENV PATH="${PATH}:/usr/lib/dart/bin/"
ENV PATH="${PATH}:/root/.pub-cache/bin"

RUN dart pub get --offline
RUN dart pub global activate webdev
RUN webdev build
# #RUN dart pub add build_runner build_web_compilers --dev
# #RUN which dart
# #RUN dart pub global activate webdev
# #RUN webdev --version
# RUN dart pub get
# RUN dart pub update

# RUN mkdir -p /lib
# RUN mkdir -p /web
# # Copy app source code and AOT compile it.
# COPY lib ./lib

# COPY web ./web

# # Ensure packages are still up-to-date if anything has changed
# #RUN dart pub get --offline
# RUN dart webdev build

# Build minimal serving image from AOT-compiled `/server` and required system
# libraries and configuration files stored in `/runtime/` from the build stage.

FROM nginx

COPY --from=0 /app/build/ /usr/share/nginx/html


# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source

# Make this port accessible from outside the container
# Necessary for your browser to send HTTP requests to your Node app
EXPOSE 8080

# Command to run when the container is ready
# Separate arguments as separate values in the array
# CMD [ "npm", "start"]
